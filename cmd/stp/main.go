/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/stp-team/systemtestportal/config"
	"gitlab.com/stp-team/systemtestportal/store/user"
	"gitlab.com/stp-team/systemtestportal/web/context"
	"gitlab.com/stp-team/systemtestportal/web/handler"
	"gitlab.com/stp-team/systemtestportal/web/sessions"
)

func main() {
	var host = flag.String("host", "localhost", "IP of host to run webserver on")
	var port = flag.Int("port", 8080, "Port to run webserver on")
	var debug = flag.Bool("debug", false, "If this flag is present the server"+
		" outputs some additional debugging info.")
	sessions.InitSessionManagement(nil, nil)
	context.Init(sessions.GetSessionStore())
	flag.Parse()

	if *debug {
		log.SetFlags(log.Ldate | log.Ltime | log.Llongfile)
	}

	config.Load()

	fs := http.FileServer(http.Dir("static"))
	faviconfs := http.FileServer(http.Dir("static/img/favicons/"))
	http.Handle("/favicon.ico", faviconfs)
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.HandleFunc("/user/", handler.ProjectHandler)
	http.Handle("/register", handler.NewRegisterHandler(user.GetStore()))
	http.HandleFunc("/groups/new", handler.NewGroupHandler)
	http.HandleFunc("/groups/save", handler.SaveGroupHandler)
	http.Handle("/logout", handler.NewLogoutHandler(sessions.GetSessionStore()))
	http.Handle("/login", handler.NewLoginHandler(sessions.GetSessionStore(), user.GetStore()))
	http.HandleFunc("/", handler.RootHandler)
	http.HandleFunc("/explore/", handler.ExploreHandler)
	http.HandleFunc("/about/", handler.AboutHandler)

	addr := fmt.Sprintf("%s:%d", *host, *port)
	log.Printf("Listening on %s", addr)

	err := http.ListenAndServe(addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe error: ", err)
	}
}
