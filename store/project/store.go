/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

import (
	"log"
	"time"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/visibility"
)

// Store provides an interface for saving and retrieving projects
type Store interface {
	// List returns a list of projects in the given container
	List(string) []*project.Project

	// Add stores a new project. The first argument is used to identify the container for the project
	Add(string, *project.Project) error

	// Get looks up the project with the given id in the given container. It returns a pointer to the project and true
	// if a matching project was found or nil and false otherwise
	Get(container, project string) (*project.Project, bool, error)
}

// exampleProjects is a list of example projects
var exampleProjects = []*project.Project{dummyProject}

// dummyProject is a default project in the system for testing purposes
var dummyProject = &project.Project{
	ID:          "project-1",
	Name:        "Project 1",
	Description: "The first example project",
	Visibility:  visibility.Public,
	SUTVersions: []string{
		"1.0.1", "1.0.2", "2.0.1", "3.0.5",
	},
	CreationDate: time.Now().Round(time.Second),
}

var singleton Store = initRAMStore()

func init() {
	for _, p := range exampleProjects {
		err := singleton.Add("user", p)
		if err != nil {
			log.Println("Error while initializing project store:", err)
		}
	}
}

// GetStore returns a store, which is used for saving and retrieving projects
func GetStore() Store {
	return singleton
}
