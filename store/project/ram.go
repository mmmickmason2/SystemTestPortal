/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

import (
	"sync"

	"gitlab.com/stp-team/systemtestportal/domain/project"
)

type ramStore struct {
	sync.RWMutex
	projects map[string]map[string]*project.Project
}

func initRAMStore() *ramStore {
	return &ramStore{
		projects: make(map[string]map[string]*project.Project),
	}
}

func (s *ramStore) List(cID string) []*project.Project {
	if s == nil {
		panic("trying to list projects in nil store")
	}

	s.RLock()
	defer s.RUnlock()

	m, ok := s.projects[cID]
	if !ok {
		return nil
	}

	var l []*project.Project
	for _, p := range m {
		l = append(l, p)
	}

	return l
}

func (s *ramStore) Add(cID string, p *project.Project) error {
	if s == nil {
		panic("trying to add a project to a nil store")
	}

	if p == nil {
		panic("trying to add nil project to the store")
	}

	s.Lock()
	defer s.Unlock()

	m, ok := s.projects[cID]
	if !ok {
		m = make(map[string]*project.Project)
		s.projects[cID] = m
	}

	m[p.ID] = p

	return nil
}

func (s *ramStore) Get(cID, pID string) (*project.Project, bool, error) {
	if s == nil {
		panic("trying to get project from nil store")
	}

	s.RLock()
	defer s.RUnlock()

	m, ok := s.projects[cID]
	if !ok {
		return nil, false, nil
	}

	p, ok := m[pID]
	return p, ok, nil
}
