/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

import (
	"testing"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/visibility"
)

var u1Projects = map[string]*project.Project{
	"p0001":           {ID: "p0001", Name: "P0001"},
	"awesome-project": {ID: "awesome-project", Name: "Awesome Project"},
	"another-project": {ID: "another-project", Name: "Anöther Project"},
}

var pS = &ramStore{
	projects: map[string]map[string]*project.Project{
		"user-1": u1Projects,
	},
}

var _ Store = initRAMStore()

func expectPanic(t *testing.T) {
	if recover() == nil {
		t.Error("Expected panic did not occur")
	}
}

func TestCaseRAMStoreNilMethodCalls(t *testing.T) {
	var empty *ramStore

	t.Run("List", func(t *testing.T) {
		defer expectPanic(t)
		empty.List("user-1")
	})
	t.Run("Add (nil store)", func(t *testing.T) {
		defer expectPanic(t)
		empty.Add("user-1", &project.Project{ID: "awesome-project"})
	})
	t.Run("Add (nil project)", func(t *testing.T) {
		defer expectPanic(t)
		initRAMStore().Add("user-1", nil)
	})
	t.Run("Get", func(t *testing.T) {
		defer expectPanic(t)
		empty.Get("user-1", "awesome-project")
	})
}

func TestRAMStoreList(t *testing.T) {
	type args struct {
		store *ramStore
		cID   string
	}
	tests := []struct {
		name string
		args args
		want []*project.Project
	}{
		{
			name: "Unknown container",
			args: args{
				store: pS,
				cID:   "user-2",
			},
			want: nil,
		},
		{
			name: "Known container",
			args: args{store: pS, cID: "user-1"},
			want: []*project.Project{
				u1Projects["p0001"],
				u1Projects["awesome-project"],
				u1Projects["another-project"],
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {

			got := tc.args.store.List(tc.args.cID)

			if !containsUnordered(got, tc.want) {
				t.Errorf("List(%#v) = %v, want %v", tc.args.cID, got, tc.want)
			}
		})
	}
}

func TestRAMStoreAdd(t *testing.T) {
	type args struct {
		store   *ramStore
		cID     string
		project *project.Project
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "New container",
			args: args{
				store:   initRAMStore(),
				cID:     "user-1",
				project: &project.Project{ID: "cool-project", Name: "Cool Project"},
			},
			want: nil,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			got := tc.args.store.Add(tc.args.cID, tc.args.project)

			if got != tc.want {
				t.Errorf("Add(%v, %v) = %v, want %v", tc.args.cID, tc.args.project, got, tc.want)
			}
		})
	}
}

func TestRAMStoreGet(t *testing.T) {
	type args struct {
		store *ramStore
		cID   string
		pID   string
	}
	type want struct {
		p  *project.Project
		ok bool
	}
	tests := []struct {
		name string
		args args
		want want
	}{
		{
			name: "Unknown container",
			args: args{store: pS, cID: "user-2", pID: "p0001"},
			want: want{p: nil, ok: false},
		},
		{
			name: "Unknown project",
			args: args{store: pS, cID: "user-1", pID: "cool-project"},
			want: want{p: nil, ok: false},
		},
		{
			name: "Known project",
			args: args{store: pS, cID: "user-1", pID: "p0001"},
			want: want{p: u1Projects["p0001"], ok: true},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {

			gotP, gotOK, gotError := tc.args.store.Get(tc.args.cID, tc.args.pID)

			if gotP != tc.want.p || gotOK != tc.want.ok || gotError != nil {
				t.Errorf("Get(%v, %v) = (%v, %v), want (%v, %v)", tc.args.cID, tc.args.pID, gotP, gotOK, tc.want.p, tc.want.ok)
			}
		})
	}
}

func TestRAMStoreAddAndGet(t *testing.T) {
	type args struct {
		store *ramStore
		cID   string
		p     project.Project
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "Basic example",
			args: args{
				store: initRAMStore(),
				cID:   "user-1",
				p:     project.NewProject("Test Project", "This is a test project", visibility.Private),
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			tc.args.store.Add(tc.args.cID, &tc.args.p)
			gotP, gotOK, _ := tc.args.store.Get(tc.args.cID, tc.args.p.ID)

			if !gotOK {
				t.Errorf("Get after Add failed because the project was not found")
			}

			if gotOK && gotP != &tc.args.p {
				t.Errorf("Get after Add failed, because the project addresses were different: got %v, want %v", gotP, &tc.args.p)
			}
		})
	}
}

func containsUnordered(got, want []*project.Project) bool {
outer:
	for _, w := range want {
		for _, g := range got {
			if w == g {
				continue outer
			}
		}
		return false
	}
	return true
}
