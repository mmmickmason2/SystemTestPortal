/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package user

import (
	"testing"

	"gitlab.com/stp-team/systemtestportal/domain/user"
)

var _ Store = &storeRAM{}

func TestInitStoreRam(t *testing.T) {

	result := initStoreRAM()

	if result.emails == nil {
		t.Error("Email-List of server wasn't initialised. Expected map but was nil")
	}
	if result.list == nil {
		t.Error("User-list of server wasn't initialised. Expected map but was nil")
	}
}

func TestStoreRAM_Add(t *testing.T) {
	testUser := user.New("TestDisplay", "Username", "mail@test.de", "password")

	server := initStoreRAM()
	err := server.Add(&testUser)

	if err != nil {
		t.Error(err)
	}

	id, ok := server.emails[testUser.EMail]
	if !ok || id != testUser.ID {
		t.Errorf("EMail wasn't saved in database. Expected %s but was %s", testUser.ID, id)
	}
	id, ok = server.accountNames[testUser.AccountName]
	if !ok || id != testUser.ID {
		t.Errorf("AccountName wasn't saved in database. Expected %s but was %s", testUser.ID, id)
	}
	resultUser, ok := server.list[testUser.ID]
	if !ok || resultUser != &testUser {
		t.Errorf("User wasn't saved in database. Expected %+v but was %+v", testUser, resultUser)
	}
}

func TestServer_AddUser_NilTest(t *testing.T) {
	server := initStoreRAM()
	err := server.Add(nil)

	if err != nil {
		t.Error(err)
	}
}

func TestServer_GetUser(t *testing.T) {
	testUser := user.New("TestDisplay", "Username", "mail@test.de", "password")

	server := initStoreRAM()
	err := server.Add(&testUser)

	//get by ID
	result, ok := server.Get(testUser.ID)
	if err != nil {
		t.Error(err)
	}
	if !ok {
		t.Errorf("No User found by ID %s.", testUser.ID)
	}
	if *result != testUser {
		t.Errorf("Wrong user found by ID %s. Expected %+v but was %+v", testUser.ID, testUser, result)
	}
	//get by accountName
	result, ok = server.Get(testUser.AccountName)
	if err != nil {
		t.Error(err)
	}
	if !ok {
		t.Errorf("No User found by AccountName %s.", testUser.AccountName)
	}
	if *result != testUser {
		t.Errorf("Wrong user found by AccountName %s. Expected %+v but was %+v", testUser.AccountName, testUser, result)
	}
	//get by Email
	result, ok = server.Get(testUser.EMail)
	if err != nil {
		t.Error(err)
	}
	if !ok {
		t.Errorf("No User found by Email %s.", testUser.EMail)
	}
	if *result != testUser {
		t.Errorf("Wrong user found by Email %s. Expected %+v but was %+v", testUser.EMail, testUser, result)
	}
}

func TestServer_GetUser_negative(t *testing.T) {
	testUser := user.New("TestDisplay", "Username", "mail@test.de", "password")

	server := initStoreRAM()
	err := server.Add(&testUser)
	result, ok := server.Get("wrong")

	if err != nil {
		t.Error(err)
	}
	if ok {
		t.Errorf("Found user %+v but expected none", result)
	}
}

func TestServer_Validate_byMail(t *testing.T) {
	mail := "mail@test.de"
	password := "password"
	testUser := user.New("TestDisplay", "Username", mail, password)

	server := initStoreRAM()
	err1 := server.Add(&testUser)
	resultUser, ok, err2 := server.Validate(mail, password)

	if err1 != nil {
		t.Error(err1)
	}
	if *resultUser != testUser || !ok || err2 != nil {
		t.Errorf("Couldn't validate user %+v via %s and %s", testUser, mail, password)
	}
}

func TestServer_Validate_byUsername(t *testing.T) {
	name := "Username"
	password := "password"
	testUser := user.New("TestDisplay", name, "mail@test.de", password)

	server := initStoreRAM()
	err1 := server.Add(&testUser)
	resultUser, ok, err2 := server.Validate(name, password)

	if err1 != nil {
		t.Error(err1)
	}
	if *resultUser != testUser || !ok || err2 != nil {
		t.Errorf("Couldn't validate user %+v via %s and %s", testUser, name, password)
	}
}

func TestServer_Validate_negative_Password(t *testing.T) {
	password := "password"
	wrongPassword := "wrongPassword"
	testUser := user.New("TestDisplay", "Username", "mail@test.de", password)

	server := initStoreRAM()
	err1 := server.Add(&testUser)
	resultUser, ok, err2 := server.Validate("mail@test.de", wrongPassword)

	if err1 != nil {
		t.Error(err1)
	}
	if err2 != nil {
		t.Error(err2)
	}
	if ok {
		t.Errorf("Validated User, but password was wrong."+
			" Send %s but %s was correct so expected false but was true", wrongPassword, password)
	}
	if *resultUser != testUser {
		t.Errorf("Found wrong or no user. Expected %+v but was %+v.", testUser, resultUser)
	}
}

func TestServer_Validate_negative_Existence(t *testing.T) {
	testUser := user.New("TestDisplay", "Username", "mail@test.de", "password")

	server := initStoreRAM()
	err1 := server.Add(&testUser)
	resultUser, ok, err2 := server.Validate("wrong", "password")

	if err1 != nil {
		t.Error(err1)
	}
	if err2 != nil {
		t.Error(err2)
	}
	if ok {
		t.Errorf("Validated user %+v but expected none", resultUser)
	}
}

func TestServer_GetAllUser(t *testing.T) {
	server := initStoreRAM()
	result := server.List()

	if len(server.list) != len(result) {
		t.Errorf("Result contains wrong number of users. Expected %d but was %d.", len(server.list), len(result))
	}
}
