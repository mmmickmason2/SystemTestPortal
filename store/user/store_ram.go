/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package user

import (
	"log"
	"sync"

	"gitlab.com/stp-team/systemtestportal/domain/user"
	"gitlab.com/stp-team/systemtestportal/web/slugs"
)

//storeRAM provides functions for user-handling like add, get or validate
type storeRAM struct {
	sync.RWMutex
	// list is a map of all users in the system. It maps the ID of an user to the user itself.
	list map[string]*user.User
	// emails is a map of all user emails in the system. It maps emails to user IDs.
	emails map[string]string
	// accountNames is a map of all account-names in the system. It maps account-names to user IDs.
	accountNames map[string]string
}

// DefaultUsers is a set of example users
var DefaultUsers = []*user.User{
	{
		ID:          "admin",
		DisplayName: "admin",
		AccountName: "admin",
		EMail:       "admin@example.org",
		Password:    "admin",
	},
	{
		ID:          "default",
		DisplayName: "DeFauLt",
		AccountName: "default",
		EMail:       "default@example.org",
		Password:    "default",
	},
}

func initStoreRAM() *storeRAM {
	s := storeRAM{
		list:         make(map[string]*user.User),
		emails:       make(map[string]string),
		accountNames: make(map[string]string),
	}

	for _, u := range DefaultUsers {
		err := s.Add(u)
		if err != nil {
			log.Print("Default user couldn't be saved to database")
		}
	}

	return &s
}

//Add adds an user to the user-database
//If user already exists, function overrides the existing one
func (s *storeRAM) Add(u *user.User) error {
	if u == nil {
		return nil
	}

	s.Lock()
	defer s.Unlock()

	u.ID = slugs.Slugify(u.AccountName)
	s.emails[u.EMail] = u.ID
	s.accountNames[u.AccountName] = u.ID
	s.list[u.ID] = u
	return nil
}

//Get searches for an user with the given text as id, username or email
func (s *storeRAM) Get(identifier string) (*user.User, bool) {
	s.RLock()
	defer s.RUnlock()

	//by ID
	result, found := s.list[identifier]
	//by accountName
	if !found {
		id, ok := s.accountNames[identifier]
		if ok {
			result, found = s.list[id]
		}
	}
	//by email
	if !found {
		id, ok := s.emails[identifier]
		if ok {
			result, found = s.list[id]
		}
	}
	return result, found
}

//List returns a slice containing all user profiles
func (s *storeRAM) List() []*user.User {
	s.RLock()
	defer s.RUnlock()

	var l []*user.User
	for _, u := range s.list {
		l = append(l, u)
	}

	return l
}

// Validate authenticates a user
// returns identified user, a bool if validations was successful and error if any occurred
func (s *storeRAM) Validate(identifier string, password string) (*user.User, bool, error) {
	s.RLock()
	defer s.RUnlock()

	u, ok := s.Get(identifier)
	if !ok {
		return nil, false, nil
	}

	return u, (u.AccountName == identifier || u.ID == identifier || u.EMail == identifier) && u.Password == password, nil
}
