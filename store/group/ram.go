/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package group

import (
	"sync"

	"gitlab.com/stp-team/systemtestportal/domain/group"
)

type ramStore struct {
	sync.RWMutex
	groups map[string]*group.Group
}

func initRAMStore() *ramStore {
	return &ramStore{
		groups: make(map[string]*group.Group),
	}
}

func (s *ramStore) List() []*group.Group {
	if s == nil {
		panic("trying to list groups in a nil store")
	}

	s.RLock()
	defer s.RUnlock()

	var l []*group.Group
	for _, v := range s.groups {
		l = append(l, v)
	}

	return l
}

func (s *ramStore) Add(g *group.Group) error {
	if s == nil {
		panic("trying to add a group to a nil store")
	}

	if g == nil {
		panic("trying to add a nil group to the store")
	}

	s.Lock()
	defer s.Unlock()

	s.groups[g.ID] = g

	return nil
}

func (s *ramStore) Get(id string) (*group.Group, bool, error) {
	if s == nil {
		panic("trying to get a group from a nil store")
	}

	s.RLock()
	defer s.RUnlock()

	g, ok := s.groups[id]
	return g, ok, nil
}
