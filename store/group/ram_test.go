/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package group

import (
	"testing"

	"gitlab.com/stp-team/systemtestportal/domain/group"
	"gitlab.com/stp-team/systemtestportal/domain/visibility"
)

var _ Store = initRAMStore()

func expectPanic(t *testing.T) {
	if recover() == nil {
		t.Error("Expected panic did not occur")
	}
}

func TestCaseRAMStoreNilMethodCalls(t *testing.T) {
	var empty *ramStore
	t.Run("List", func(t *testing.T) {
		defer expectPanic(t)
		empty.List()
	})
	t.Run("Add (nil store)", func(t *testing.T) {
		defer expectPanic(t)
		empty.Add(&group.Group{ID: "new-group"})
	})
	t.Run("Add (nil group)", func(t *testing.T) {
		defer expectPanic(t)
		initRAMStore().Add(nil)
	})
	t.Run("Get", func(t *testing.T) {
		defer expectPanic(t)
		empty.Get("new-group")
	})
}

func TestRAMStoreList(t *testing.T) {
	store := &ramStore{
		groups: map[string]*group.Group{
			"test": {
				ID:         "test",
				Name:       "Test",
				Visibility: visibility.Private,
			},
			"totally-real-group": {
				ID:         "totally-real-group",
				Name:       "Totally real group",
				Visibility: visibility.Public,
			},
		},
	}
	tests := []struct {
		name string
		s    *ramStore
		want []*group.Group
	}{
		{
			name: "Empty store",
			s:    initRAMStore(),
			want: []*group.Group{},
		},
		{
			name: "Example store",
			s:    store,
			want: []*group.Group{
				store.groups["test"], store.groups["totally-real-group"],
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.List(); !containsUnordered(got, tt.want) {
				t.Errorf("ramStore.List() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRAMStoreAdd(t *testing.T) {
	type args struct {
		g *group.Group
	}

	testGroup := group.Group{ID: "test", Name: "Test", Visibility: visibility.Private}

	tests := []struct {
		name string
		s    *ramStore
		args args
		want error
	}{
		{
			name: "Correct case",
			s:    initRAMStore(),
			args: args{g: &testGroup},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.Add(tt.args.g); got != tt.want {
				t.Errorf("ramStore.Add(%v) = %v, want %v", tt.args.g, got, tt.want)
			}
		})
	}
}

func TestRAMStoreGet(t *testing.T) {
	store := &ramStore{
		groups: map[string]*group.Group{
			"test": {
				ID:         "test",
				Name:       "Test",
				Visibility: visibility.Private,
			},
			"totally-real-group": {
				ID:         "totally-real-group",
				Name:       "Totally real group",
				Visibility: visibility.Public,
			},
		},
	}
	type args struct {
		id string
	}
	type want struct {
		g  *group.Group
		ok bool
	}
	tests := []struct {
		name string
		s    *ramStore
		args args
		want want
	}{
		{
			name: "Group not found",
			s:    initRAMStore(),
			args: args{id: "test"},
			want: want{},
		},
		{
			name: "Group found",
			s:    store,
			args: args{id: "test"},
			want: want{g: store.groups["test"], ok: true},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotG, gotOK, gotError := tt.s.Get(tt.args.id)

			if gotG != tt.want.g || gotOK != tt.want.ok || gotError != nil {
				t.Errorf("ramStore.Get(%v) = (%v, %v, %v), want (%v, %v, nil)", tt.args.id, gotG, gotOK,
					gotError, tt.want.g, tt.want.ok)
			}
		})
	}
}

func TestRAMStoreAddAndGet(t *testing.T) {
	type args struct {
		store *ramStore
		g     group.Group
	}

	tcs := []struct {
		name string
		args args
	}{
		{
			name: "Basic example",
			args: args{
				store: initRAMStore(),
				g:     group.NewGroup("Test Group", "This is a test group", visibility.Private, nil),
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			tc.args.store.Add(&tc.args.g)
			gotG, gotOK, _ := tc.args.store.Get(tc.args.g.ID)

			if !gotOK {
				t.Errorf("Get after Add failed because the project was not found")
			}

			if gotOK && gotG != &tc.args.g {
				t.Errorf("Get after Add failed, because the project addresses were different: got %v, want %v", gotG, &tc.args.g)
			}
		})
	}
}

func containsUnordered(got, want []*group.Group) bool {
outer:
	for _, w := range want {
		for _, g := range got {
			if w == g {
				continue outer
			}
		}
		return false
	}
	return true
}
