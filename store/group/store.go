/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package group

import (
	"log"
	"time"

	"gitlab.com/stp-team/systemtestportal/domain/group"
	"gitlab.com/stp-team/systemtestportal/domain/visibility"
)

// Store provides an interface for storing and retrieving groups
type Store interface {
	// List returns a list of groups in the system
	List() []*group.Group

	// Add stores a group in the system
	Add(group *group.Group) error

	// Get returns the group with the given ID
	Get(groupID string) (*group.Group, bool, error)
}

// DummyGroups is a list of example groups
var dummyGroups = []*group.Group{
	{
		ID:           "group-1",
		Name:         "Group 1",
		Description:  "An example group",
		Visibility:   visibility.Public,
		CreationDate: time.Now().Round(time.Second),
	},
	{
		ID:           "group-2",
		Name:         "Group 2",
		Visibility:   visibility.Private,
		CreationDate: time.Now().Round(time.Second),
	},
}

var singleton = initRAMStore()

func init() {
	for _, g := range dummyGroups {
		err := singleton.Add(g)
		if err != nil {
			log.Println("Error while initializing group store:", err)
		}
	}
}

// GetStore returns a Store that can be used to store and retrieve groups
func GetStore() Store {
	return singleton
}
