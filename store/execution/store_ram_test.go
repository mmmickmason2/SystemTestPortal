/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"testing"
	"time"

	"gitlab.com/stp-team/systemtestportal/domain/test"
)

const CaseID = "C18e5d4z"
const SequenceID = "S4e8dg852"
const SUTVersion = "1.1"
const Environment = "Win10"
const OverAllComment = "All OK"
const ProjectID = "abcdef42"

var stepRec1 = test.StepExecutionRecord{
	NeededTime:       4 * time.Minute,
	ObservedBehavior: "",
	Result:           test.NotAssessed,
	Comment:          "",
}
var stepRec2 = test.StepExecutionRecord{
	NeededTime:       10 * time.Minute,
	ObservedBehavior: "",
	Result:           test.Pass,
	Comment:          "Hello?",
}
var stepRec3 = test.StepExecutionRecord{
	NeededTime:       22 * time.Minute,
	ObservedBehavior: "Something happened",
	Result:           test.PassWithComment,
	Comment:          "What's up?",
}
var stepRec4 = test.StepExecutionRecord{
	NeededTime:       0,
	ObservedBehavior: "",
	Result:           test.NotAssessed,
	Comment:          "",
}
var stepRec5 = test.StepExecutionRecord{
	NeededTime:       0,
	ObservedBehavior: "",
	Result:           test.NotAssessed,
	Comment:          "",
}
var stepRec6 = test.StepExecutionRecord{
	NeededTime:       1 * time.Minute,
	ObservedBehavior: "",
	Result:           test.NotAssessed,
	Comment:          "",
}
var caseRecordTemplate = test.CaseExecutionRecord{
	TestCaseID:        CaseID,
	TestCaseVersionNr: 1,
	ProjectID:         ProjectID,
	SUTVersion:        SUTVersion,
	Environment:       Environment,

	ExecutionDate: time.Date(2000, 12, 24, 12, 0, 0, 0, time.UTC),
	StepRecords:   []test.StepExecutionRecord{stepRec1, stepRec2, stepRec3, stepRec4, stepRec5, stepRec6},
	Result:        test.NotAssessed,
	Comment:       OverAllComment,
}

func caseRecordMapContainsCaseRecord(m caseRecordMap, item test.CaseExecutionRecord, index int) bool {

	slice := m[item.ProjectID][item.TestCaseID]
	if slice == nil || index >= len(slice) {
		return false
	}
	r := slice[index]
	return r.Equals(item)
}
func sequenceRecordMapContainsSequenceRecord(m sequenceRecordMap, item test.SequenceExecutionRecord, index int) bool {
	slice := m[item.ProjectID][item.TestSequenceID]
	if slice == nil || index >= len(slice) {
		return false
	}
	r := slice[index]
	return r.Equals(item)
}

func sliceContainsCaseRecord(slice []test.CaseExecutionRecord, item test.CaseExecutionRecord) bool {

	for _, r := range slice {
		if r.Equals(item) {
			return true
		}
	}
	return false
}
func sliceContainsSequenceRecord(slice []test.SequenceExecutionRecord, item test.SequenceExecutionRecord) bool {

	for _, r := range slice {
		if r.Equals(item) {
			return true
		}
	}
	return false
}

func TestStoreRAM_AddCaseRecord(t *testing.T) {

	caseRecord := caseRecordTemplate.DeepCopy()
	server := storeRAM{
		caseExecutionRecords:     make(caseRecordMap),
		sequenceExecutionRecords: make(sequenceRecordMap),
	}

	id, err := server.AddCaseRecord(&caseRecord)
	caseRecord.ID = id

	if err != nil {
		t.Error(err)
	}
	if !caseRecordMapContainsCaseRecord(server.caseExecutionRecords, caseRecord, id-1) {
		t.Error("Record wasn't saved correctly and couldn't be found in store.")
	}
}

func TestStoreRAM_AddSequenceRecord(t *testing.T) {
	caseRecord1 := caseRecordTemplate.DeepCopy()
	caseRecord2 := caseRecordTemplate.DeepCopy()
	caseRecord2.TestCaseVersionNr = 2
	sequenceRecord := test.SequenceExecutionRecord{
		TestSequenceID:        "54dfg8sd",
		TestSequenceVersionNr: 1,
		ProjectID:             "asf885sdf7",
		CaseExecutionRecords: []test.CaseExecutionRecordID{
			{caseRecord1.ID, caseRecord1.TestCaseID, caseRecord1.ProjectID},
			{caseRecord2.ID, caseRecord2.TestCaseID, caseRecord2.ProjectID},
		},
	}
	server := storeRAM{
		caseExecutionRecords:     make(caseRecordMap),
		sequenceExecutionRecords: make(sequenceRecordMap),
	}

	id, err := server.AddSequenceRecord(&sequenceRecord)
	sequenceRecord.ID = id

	if err != nil {
		t.Error(err)
	}

	//search for sequenceRecord
	if !sequenceRecordMapContainsSequenceRecord(server.sequenceExecutionRecords, sequenceRecord, id-1) {
		t.Error("Record wasn't saved correctly and couldn't be found in store.")
	}
}

func TestStoreRAM_AddSequenceRecord2(t *testing.T) {
	caseRecord1 := caseRecordTemplate.DeepCopy()
	caseRecord2 := caseRecordTemplate.DeepCopy()
	caseRecord2.TestCaseVersionNr = 2
	caseRecord3 := caseRecordTemplate.DeepCopy()
	caseRecord3.TestCaseVersionNr = 3
	sequenceRecord1 := test.SequenceExecutionRecord{
		TestSequenceID:        "54dfg8sd",
		TestSequenceVersionNr: 1,
		ProjectID:             "asf885sdf7",
		CaseExecutionRecords: []test.CaseExecutionRecordID{
			{caseRecord1.ID, caseRecord1.TestCaseID, caseRecord1.ProjectID},
		},
	}
	sequenceRecord2 := test.SequenceExecutionRecord{
		TestSequenceID:        "54dfg8sd",
		TestSequenceVersionNr: 1,
		ProjectID:             "asf885sdf7",
		CaseExecutionRecords: []test.CaseExecutionRecordID{
			{caseRecord2.ID, caseRecord2.TestCaseID, caseRecord2.ProjectID},
			{caseRecord3.ID, caseRecord3.TestCaseID, caseRecord3.ProjectID},
		},
	}
	server := storeRAM{
		caseExecutionRecords:     make(caseRecordMap),
		sequenceExecutionRecords: make(sequenceRecordMap),
	}

	id1, err1 := server.AddSequenceRecord(&sequenceRecord1)
	id2, err2 := server.AddSequenceRecord(&sequenceRecord2)
	sequenceRecord1.ID = id1
	sequenceRecord2.ID = id2

	if err1 != nil {
		t.Error(err1)
	}
	if err2 != nil {
		t.Error(err2)
	}

	//search for sequenceRecord
	if !sequenceRecordMapContainsSequenceRecord(server.sequenceExecutionRecords, sequenceRecord1, id1-1) {
		t.Error("Record 1 wasn't saved correctly and couldn't be found in store.")
	}
	if !sequenceRecordMapContainsSequenceRecord(server.sequenceExecutionRecords, sequenceRecord2, id2-1) {
		t.Error("Record 2 wasn't saved correctly and couldn't be found in store.")
	}
}

func TestStoreRAM_GetCaseExecutionRecords(t *testing.T) {
	caseRecord := caseRecordTemplate.DeepCopy()
	server := storeRAM{
		caseExecutionRecords:     make(caseRecordMap),
		sequenceExecutionRecords: make(sequenceRecordMap),
	}

	_, err1 := server.AddCaseRecord(&caseRecord)
	slice, err2 := server.GetCaseExecutionRecords(ProjectID, CaseID)

	if err1 != nil {
		t.Error(err1)
	}
	if err2 != nil {
		t.Errorf("No Records were found for ProjectID %s and TestCaseID %s.", ProjectID, CaseID)
	}
	if !sliceContainsCaseRecord(slice, caseRecord) {
		t.Error("Records couln't be read from store.")
	}
}

func TestStoreRAM_GetSequenceExecutionRecords(t *testing.T) {
	caseRecord1 := caseRecordTemplate.DeepCopy()
	caseRecord2 := caseRecordTemplate.DeepCopy()
	caseRecord2.TestCaseVersionNr = 2
	sequenceRecord := test.SequenceExecutionRecord{
		TestSequenceID:        SequenceID,
		TestSequenceVersionNr: 1,
		ProjectID:             ProjectID,
		CaseExecutionRecords: []test.CaseExecutionRecordID{
			{caseRecord1.ID, caseRecord1.TestCaseID, caseRecord1.ProjectID},
			{caseRecord2.ID, caseRecord2.TestCaseID, caseRecord2.ProjectID},
		},
	}
	server := storeRAM{
		caseExecutionRecords:     make(caseRecordMap),
		sequenceExecutionRecords: make(sequenceRecordMap),
	}

	_, err1 := server.AddSequenceRecord(&sequenceRecord)
	sequenceSlice, err2 := server.GetSequenceExecutionRecords(ProjectID, SequenceID)

	//search for sequenceRecord
	if err1 != nil {
		t.Error(err1)
	}
	if err2 != nil {
		t.Errorf("No Records were found for ProjectID %s and SequenceID %s.", ProjectID, SequenceID)
	}
	if !sliceContainsSequenceRecord(sequenceSlice, sequenceRecord) {
		t.Error("Record wasn't saved correctly and couldn't be found in store.")
	}
}
