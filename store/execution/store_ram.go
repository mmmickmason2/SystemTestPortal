/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"time"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/test"
	project2 "gitlab.com/stp-team/systemtestportal/store/project"
	test2 "gitlab.com/stp-team/systemtestportal/store/test"
)

// caseRecordMap stores records for testcases
type caseRecordMap map[string]map[string][]test.CaseExecutionRecord

// sequenceRecordMap stores records for testsequeneces
type sequenceRecordMap map[string]map[string][]test.SequenceExecutionRecord

//storeRAM provides functions for saving and loading execution-records
type storeRAM struct {
	// caseExecutionRecords is a map of all CaseExecutionRecords in the system.
	// It maps the TestCaseID to the records of this TestCase.
	caseExecutionRecords caseRecordMap
	// sequenceExecutionRecords is a map of all SequenceExecutionRecords in the system.
	// It maps the ID of a Sequence to its records.
	sequenceExecutionRecords sequenceRecordMap
}

// StoreRAM interface for storing test records.
type StoreRAM interface {
	// AddCaseRecord adds the given record to the store
	AddCaseRecord(r *test.CaseExecutionRecord) (caseRecordID int, err error)
	// AddSequenceRecord adds the given record to the store
	AddSequenceRecord(r *test.SequenceExecutionRecord) (sequenceRecordID int, err error)
	// GetCaseExecutionRecords gets the records for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionRecords(projectID, testCaseID string) ([]test.CaseExecutionRecord, error)
	// GetCaseExecutionRecord gets the record with the given id for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionRecord(projectID, testCaseID string, recordID int) (test.CaseExecutionRecord, error)
	// GetSequenceExecutionRecords gets the records for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionRecords(projectID, sequenceID string) ([]test.SequenceExecutionRecord, error)
	// GetSequenceExecutionRecord gets the record with the given id for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionRecord(projectID, sequenceID string, recordID int) (test.SequenceExecutionRecord, error)
}

var singleton storeRAM

func init() {
	singleton = storeRAM{
		caseExecutionRecords:     make(caseRecordMap),
		sequenceExecutionRecords: make(sequenceRecordMap),
	}
	addDummyData()
}

func addDummyData() {
	var err error
	Store := GetRecordStore()

	mustCase := func(t *test.Case, ok bool, err error) test.Case {
		return *t
	}
	mustProj := func(p *project.Project, ok bool, err error) project.Project {
		return *p
	}

	//Case 1 Record 1
	rec1 := test.NewCaseExecutionRecord(
		mustCase(test2.GetCaseStore().Get("user", "project-1", "test-case-1")),
		1,
		"project-1",
		mustProj(project2.GetStore().Get("user", "project-1")).SUTVersions[0],
		"Win 10")
	rec1.SaveStep(
		1,
		"Result 1",
		test.Pass,
		"Comment 1",
		5*time.Minute)
	rec1.SaveStep(
		2,
		"Result 2",
		test.PassWithComment,
		"Comment 2",
		5*time.Minute)
	rec1.SaveStep(
		3,
		"Result 3",
		test.Fail,
		"Comment 3",
		5*time.Minute)
	rec1.Finish(
		test.Pass,
		"Final Comment Rec 1",
		2*time.Minute)
	_, err = Store.AddCaseRecord(&rec1)
	if err != nil {
		print(err)
	}
	//Case 1 Record 2
	rec2 := test.NewCaseExecutionRecord(
		mustCase(test2.GetCaseStore().Get("user", "project-1", "test-case-1")),
		1,
		"project-1",
		mustProj(project2.GetStore().Get("user", "project-1")).SUTVersions[0],
		"Win 10")
	rec2.SaveStep(
		1,
		"Result 1",
		test.Pass,
		"Comment 1",
		5*time.Minute)
	rec2.SaveStep(
		2,
		"Result 2",
		test.PassWithComment,
		"Comment 2",
		5*time.Minute)
	rec2.SaveStep(
		3,
		"Result 3",
		test.Fail,
		"Comment 3",
		5*time.Minute)
	rec2.Finish(
		test.PassWithComment,
		"Final Comment Rec 2",
		2*time.Minute)
	_, err = Store.AddCaseRecord(&rec2)
	if err != nil {
		print(err)
	}

	//Case 1 Record 3
	rec3 := test.NewCaseExecutionRecord(
		mustCase(test2.GetCaseStore().Get("user", "project-1", "test-case-1")),
		1,
		"project-1",
		mustProj(project2.GetStore().Get("user", "project-1")).SUTVersions[0],
		"Win 10")
	rec3.SaveStep(
		1,
		"Result 1",
		test.Pass,
		"Comment 1",
		5*time.Minute)
	rec3.SaveStep(
		2,
		"Result 2",
		test.PassWithComment,
		"Comment 2",
		5*time.Minute)
	rec3.SaveStep(
		3,
		"Result 3",
		test.Fail,
		"Comment 3",
		5*time.Minute)
	rec3.Finish(
		test.Fail,
		"Final Comment Rec 3",
		2*time.Minute)
	_, err = Store.AddCaseRecord(&rec3)
	if err != nil {
		print(err)
	}

	//Case 1 Record 4
	rec4 := test.NewCaseExecutionRecord(
		mustCase(test2.GetCaseStore().Get("user", "project-1", "test-case-1")),
		1,
		"project-1",
		mustProj(project2.GetStore().Get("user", "project-1")).SUTVersions[0],
		"Win 10")
	rec4.SaveStep(
		1,
		"Result 1",
		test.NotAssessed,
		"Comment 1",
		5*time.Minute)
	rec4.SaveStep(
		2,
		"Result 2",
		test.NotAssessed,
		"Comment 2",
		5*time.Minute)
	rec4.SaveStep(
		3,
		"Result 3",
		test.NotAssessed,
		"Comment 3",
		5*time.Minute)
	rec4.Finish(
		test.NotAssessed,
		"Final Comment Rec 4",
		2*time.Minute)
	_, err = Store.AddCaseRecord(&rec4)
	if err != nil {
		print(err)
	}

	//Case 2 Record 1
	rec5 := test.NewCaseExecutionRecord(
		mustCase(test2.GetCaseStore().Get("user", "project-1", "test-case-2")),
		1,
		"project-1",
		mustProj(project2.GetStore().Get("user", "project-1")).SUTVersions[0],
		"Win 10")
	rec5.SaveStep(
		1,
		"Result 1",
		test.Pass,
		"Comment 1",
		5*time.Minute)
	rec5.SaveStep(
		2,
		"Result 2",
		test.PassWithComment,
		"Comment 2",
		5*time.Minute)
	rec5.SaveStep(
		3,
		"Result 3",
		test.Fail,
		"Comment 3",
		5*time.Minute)
	rec5.Finish(
		test.Pass,
		"Final Comment Rec 1",
		2*time.Minute)
	_, err = Store.AddCaseRecord(&rec5)
	if err != nil {
		print(err)
	}

	//Sequence 1
	seq, _, _ := test2.GetSequenceStore().Get("user", "project-1", "test-sequence-1")
	seqRec := test.NewSequenceExecutionRecord(
		*seq,
		len(seq.SequenceVersions),
		"project-1",
		mustProj(project2.GetStore().Get("user", "project-1")).SUTVersions[0],
		"Win 10")
	seqRec.AddCaseExecutionRecord(1, "test-case-1", "project-1")
	seqRec.AddCaseExecutionRecord(1, "test-case-2", "project-1")
	_, err = Store.AddSequenceRecord(&seqRec)
	if err != nil {
		print(err)
	}
}

//GetRecordStore returns the current store
func GetRecordStore() StoreRAM {
	return &singleton
}

//AddCaseRecord adds an CaseExecutionRecord to the store and saves it
func (s *storeRAM) AddCaseRecord(r *test.CaseExecutionRecord) (caseRecordID int, err error) {
	if r == nil {
		return -1, nil
	}

	m := s.caseExecutionRecords[r.ProjectID]
	if m == nil {
		s.caseExecutionRecords[r.ProjectID] = make(map[string][]test.CaseExecutionRecord)
	}
	records := s.caseExecutionRecords[r.ProjectID][r.TestCaseID]
	r.ID = len(records) + 1
	s.caseExecutionRecords[r.ProjectID][r.TestCaseID] = append(records, *r)
	return r.ID, nil
}

//AddSequenceRecord adds an SequenceExecutionRecord to the store and saves it
func (s *storeRAM) AddSequenceRecord(r *test.SequenceExecutionRecord) (sequenceRecordID int, err error) {
	if r == nil {
		return -1, nil
	}

	m := s.sequenceExecutionRecords[r.ProjectID]
	if m == nil {
		s.sequenceExecutionRecords[r.ProjectID] = make(map[string][]test.SequenceExecutionRecord)
	}
	records := s.sequenceExecutionRecords[r.ProjectID][r.TestSequenceID]
	r.ID = len(records) + 1
	s.sequenceExecutionRecords[r.ProjectID][r.TestSequenceID] = append(records, *r)
	return r.ID, nil
}

//GetCaseExecutionRecords returns a slice of all CaseExecutionRecords, that belongs to the given test case.
func (s *storeRAM) GetCaseExecutionRecords(projectID, testCaseID string) ([]test.CaseExecutionRecord, error) {
	return s.caseExecutionRecords[projectID][testCaseID], nil
}

//GetSequenceExecutionRecords returns a slice of all SequenceExecutionRecord, that belongs to the given test sequence.
func (s *storeRAM) GetSequenceExecutionRecords(projectID, sequenceID string) ([]test.SequenceExecutionRecord, error) {
	return s.sequenceExecutionRecords[projectID][sequenceID], nil
}

//GetCaseExecutionRecords returns a slice of all CaseExecutionRecords, that belongs to the given test case.
func (s *storeRAM) GetCaseExecutionRecord(projectID, tcID string, recordID int) (test.CaseExecutionRecord, error) {
	slice, err := s.GetCaseExecutionRecords(projectID, tcID)
	if err != nil {
		return test.CaseExecutionRecord{}, err
	}
	if slice != nil && recordID > 0 && recordID <= len(slice) {
		return slice[recordID-1], nil
	}
	return test.CaseExecutionRecord{}, nil
}

//GetSequenceExecutionRecord returns the SequenceExecutionRecord, that fits to the given id data.
func (s *storeRAM) GetSequenceExecutionRecord(projectID, sequenceID string,
	recordID int) (test.SequenceExecutionRecord, error) {
	slice, err := s.GetSequenceExecutionRecords(projectID, sequenceID)
	if err != nil {
		return test.SequenceExecutionRecord{}, err
	}
	if slice != nil && recordID > 0 && recordID <= len(slice) {
		return slice[recordID-1], nil
	}
	return test.SequenceExecutionRecord{}, nil
}
