/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"sync"

	"gitlab.com/stp-team/systemtestportal/domain/test"
)

type sequenceRAMStore struct {
	sync.RWMutex
	sequences map[string]map[string]map[string]*test.Sequence
}

func initSequenceRAMStore() *sequenceRAMStore {
	return &sequenceRAMStore{
		sequences: make(map[string]map[string]map[string]*test.Sequence),
	}
}

func (s *sequenceRAMStore) List(cID, pID string) []*test.Sequence {
	if s == nil {
		panic("trying to list test sequences in a nil store")
	}

	s.RLock()
	defer s.RUnlock()
	pm, ok := s.sequences[cID]
	if !ok {
		return nil
	}

	var l []*test.Sequence
	for _, ts := range pm[pID] {
		l = append(l, ts)
	}
	return l
}

func (s *sequenceRAMStore) Add(cID, pID string, ts *test.Sequence) error {
	if s == nil {
		panic("trying to add a test sequence to a nil store")
	}

	if ts == nil {
		panic("trying to add a nil test sequence to the store")
	}

	s.Lock()
	defer s.Unlock()

	pm, ok := s.sequences[cID]
	if !ok {
		pm = make(map[string]map[string]*test.Sequence)
		s.sequences[cID] = pm
	}

	tsm, ok := pm[pID]
	if !ok {
		tsm = make(map[string]*test.Sequence)
		pm[pID] = tsm
	}

	tsm[ts.ID] = ts
	return nil
}

func (s *sequenceRAMStore) Get(cID, pID, tsID string) (*test.Sequence, bool, error) {
	if s == nil {
		panic("trying to get a test sequence from a nil store")
	}

	s.RLock()
	defer s.RUnlock()

	pm, ok := s.sequences[cID]
	if !ok {
		return nil, false, nil
	}

	tsm, ok := pm[pID]
	if !ok {
		return nil, false, nil
	}

	ts, ok := tsm[tsID]
	return ts, ok, nil
}

func (s *sequenceRAMStore) Delete(cID, pID, tsID string) error {
	if s == nil {
		panic("trying to delete a test sequence from a nil store")
	}

	s.Lock()
	defer s.Unlock()

	pm, ok := s.sequences[cID]
	if !ok {
		return nil
	}

	tsm, ok := pm[pID]
	if !ok {
		return nil
	}

	delete(tsm, tsID)
	return nil
}
