/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"testing"

	"gitlab.com/stp-team/systemtestportal/domain/test"
)

var testSequenceStore = &sequenceRAMStore{
	sequences: map[string]map[string]map[string]*test.Sequence{
		"user-1": {
			"p0001": {
				"test-sequence-1": {
					ID:   "test-sequence-1",
					Name: "Test Sequence 1",
				},
				"test-sequence-2": {
					ID:   "test-sequence-2",
					Name: "Test Sequence 2",
				},
			},
			"awesome-project": {
				"create-tests": {
					ID:   "create-tests",
					Name: "Create tests",
				},
				"assign-tests": {
					ID:   "assign-tests",
					Name: "Assign tests",
				},
			},
			"another-project": {
				"Execute test sequence": {
					ID:   "another-project",
					Name: "Anöther Project",
				},
			},
		},
	},
}

var _ SequenceStore = initSequenceRAMStore()

func TestSequenceRAMStoreNilMethodCalls(t *testing.T) {
	var empty *sequenceRAMStore
	t.Run("List", func(t *testing.T) {
		defer expectPanic(t)
		empty.List("user-1", "awesome-project")
	})
	t.Run("Add (nil store)", func(t *testing.T) {
		defer expectPanic(t)
		empty.Add("user-1", "awesome-project", &test.Sequence{ID: "login-2"})
	})
	t.Run("Add (nil test sequence)", func(t *testing.T) {
		defer expectPanic(t)
		initSequenceRAMStore().Add("user-1", "awesome-project", nil)
	})
	t.Run("Get", func(t *testing.T) {
		defer expectPanic(t)
		empty.Get("user-1", "awesome-project", "login")
	})
	t.Run("Delete", func(t *testing.T) {
		defer expectPanic(t)
		empty.Delete("user-1", "awesome-project", "login")
	})
}

func TestSequenceRAMStoreList(t *testing.T) {
	type args struct {
		cID string
		pID string
	}
	tests := []struct {
		name string
		s    *sequenceRAMStore
		args args
		want []*test.Sequence
	}{
		{
			name: "Unknown container",
			s:    testSequenceStore,
			args: args{
				cID: "user-2",
				pID: "p0001",
			},
			want: nil,
		},
		{
			name: "Unknown project",
			s:    testSequenceStore,
			args: args{
				cID: "user-1",
				pID: "unknown",
			},
			want: nil,
		},
		{
			name: "Known container and project",
			s:    testSequenceStore,
			args: args{
				cID: "user-1",
				pID: "awesome-project",
			},
			want: []*test.Sequence{
				testSequenceStore.sequences["user-1"]["awesome-project"]["create-tests"],
				testSequenceStore.sequences["user-1"]["awesome-project"]["assign-tests"],
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.List(tt.args.cID, tt.args.pID); !containsUnorderedTestSequences(got, tt.want) {
				t.Errorf("caseRAMStore.List() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSequenceRAMStoreAdd(t *testing.T) {
	type args struct {
		cID string
		pID string
		ts  *test.Sequence
	}
	tests := []struct {
		name string
		s    *sequenceRAMStore
		args args
		want error
	}{
		{
			name: "New container",
			s:    initSequenceRAMStore(),
			args: args{
				cID: "user-1",
				pID: "cool-project",
				ts: &test.Sequence{
					ID:   "cool-test-case",
					Name: "Cool Test Case",
				},
			},
			want: nil,
		},
		{
			name: "New project",
			s: &sequenceRAMStore{
				sequences: map[string]map[string]map[string]*test.Sequence{
					"user-1": {},
				},
			},
			args: args{
				cID: "user-1",
				pID: "cool-project",
				ts: &test.Sequence{
					ID:   "cool-test-case",
					Name: "Cool Test Case",
				},
			},
			want: nil,
		},
		{
			name: "Existing container and project",
			s: &sequenceRAMStore{
				sequences: map[string]map[string]map[string]*test.Sequence{
					"user-1": {
						"cool-project": {},
					},
				},
			},
			args: args{
				cID: "user-1",
				pID: "cool-project",
				ts: &test.Sequence{
					ID:   "cool-test-case",
					Name: "Cool Test Case",
				},
			},
			want: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.Add(tt.args.cID, tt.args.pID, tt.args.ts); got != tt.want {
				t.Errorf("caseRAMStore.Add() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSequenceRAMStoreGet(t *testing.T) {
	type args struct {
		cID  string
		pID  string
		tsID string
	}
	type want struct {
		ts *test.Sequence
		ok bool
	}
	tests := []struct {
		name string
		s    *sequenceRAMStore
		args args
		want want
	}{
		{
			name: "Unknown container",
			s:    testSequenceStore,
			args: args{
				cID:  "user-2",
				pID:  "p0001",
				tsID: "test-case-2",
			},
			want: want{
				ts: nil,
				ok: false,
			},
		},
		{
			name: "Unknown project",
			s:    testSequenceStore,
			args: args{
				cID:  "user-1",
				pID:  "p0002",
				tsID: "test-case-2"},
			want: want{
				ts: nil,
				ok: false,
			},
		},
		{
			name: "Unknown test case",
			s:    testSequenceStore,
			args: args{
				cID:  "user-1",
				pID:  "p0001",
				tsID: "test-case-3"},
			want: want{
				ts: nil,
				ok: false,
			},
		},
		{
			name: "Known test sequence",
			s:    testSequenceStore,
			args: args{
				cID:  "user-1",
				pID:  "p0001",
				tsID: "test-sequence-2"},
			want: want{
				ts: testSequenceStore.sequences["user-1"]["p0001"]["test-sequence-2"],
				ok: true,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotTC, gotOK, gotError := tt.s.Get(tt.args.cID, tt.args.pID, tt.args.tsID)
			if gotTC != tt.want.ts || gotOK != tt.want.ok || gotError != nil {
				t.Errorf("Get(%v, %v, %v) = (%v, %v, %v), want (%v, %v, nil)", tt.args.cID, tt.args.pID, tt.args.tsID,
					gotTC, gotOK, gotError, tt.want.ts, tt.want.ok)
			}

		})
	}
}

func TestSequenceRAMStoreDelete(t *testing.T) {
	type args struct {
		cID  string
		pID  string
		tcID string
	}
	tests := []struct {
		name string
		s    *sequenceRAMStore
		args args
	}{
		{
			name: "Empty store",
			s:    initSequenceRAMStore(),
			args: args{
				cID:  "user-1",
				pID:  "p0001",
				tcID: "test-case-1",
			},
		},
		{
			name: "Unknown user",
			s:    testSequenceStore,
			args: args{
				cID:  "user-53",
				pID:  "p0001",
				tcID: "test-case-1",
			},
		},
		{
			name: "Unknown project",
			s:    testSequenceStore,
			args: args{
				cID:  "user-1",
				pID:  "p0002",
				tcID: "test-case-1",
			},
		},
		{
			name: "Unknown test case",
			s:    testSequenceStore,
			args: args{
				cID:  "user-1",
				pID:  "p0001",
				tcID: "test-case-2",
			},
		},
		{
			name: "Known test case",
			s:    testSequenceStore,
			args: args{
				cID:  "user-1",
				pID:  "p0001",
				tcID: "test-case-1",
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.s.Delete(tt.args.cID, tt.args.pID, tt.args.tcID)

			if _, found, _ := tt.s.Get(tt.args.cID, tt.args.pID, tt.args.tcID); found {
				t.Errorf("Test case was not deleted")
			}
		})
	}
}

func TestSequenceRAMStoreAddAndGet(t *testing.T) {
	type args struct {
		cID string
		pID string
		ts  *test.Sequence
	}
	tests := []struct {
		name string
		s    *sequenceRAMStore
		args args
	}{
		{
			name: "Basic example",
			s:    initSequenceRAMStore(),
			args: args{
				cID: "user-1",
				pID: "p0001",
				ts: &test.Sequence{
					ID:   "new-test-case",
					Name: "New Test Case",
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotError := tt.s.Add(tt.args.cID, tt.args.pID, tt.args.ts)

			if gotError != nil {
				t.Errorf("Add(%v, %v, %v) = %v, want nil", tt.args.cID, tt.args.pID, tt.args.ts, gotError)
			}

			gotTC, gotOK, gotError := tt.s.Get(tt.args.cID, tt.args.pID, tt.args.ts.ID)

			if !gotOK || gotTC != tt.args.ts || gotError != nil {
				t.Errorf("Get(%v, %v, %v) = (%v, %v, %v), want (%v, %v, nil)", tt.args.cID, tt.args.pID,
					tt.args.ts.ID, gotTC, gotOK, gotError, tt.args.ts, true)
			}
		})
	}
}

func containsUnorderedTestSequences(got, want []*test.Sequence) bool {
outer:
	for _, w := range want {
		for _, g := range got {
			if w == g {
				continue outer
			}
		}
		return false
	}
	return true
}
