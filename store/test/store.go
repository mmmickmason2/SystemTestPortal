/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"log"
	"time"

	"gitlab.com/stp-team/systemtestportal/domain/test"
)

// CaseStore provides an interface for storing and retrieving test cases
type CaseStore interface {
	// List returns a list of test cases for the given project under the given container
	List(container, project string) []*test.Case

	// Add stores a test case for the given project under the given container
	Add(container, project string, testCase *test.Case) error

	// Get returns the test case with the given ID for the given project under the given container
	Get(container, project, id string) (*test.Case, bool, error)

	// Delete removes the test case with the given ID for the given project under the given container
	Delete(container, project, id string) error
}

// SequenceStore provides an interface for storing and retrieving test sequences
type SequenceStore interface {
	// List returns a list of test sequences for the given project under the given container
	List(container, project string) []*test.Sequence

	// Add stores a test sequence for the given project under the given container
	Add(container, project string, testSequence *test.Sequence) error

	// Get returns the test sequence with the given ID for the given project under the given container
	Get(container, project, id string) (*test.Sequence, bool, error)

	// Delete removes the test case with the given ID for the given project under the given container
	Delete(container, project, id string) error
}

var (
	caseSingleton     CaseStore     = initCaseRAMStore()
	sequenceSingleton SequenceStore = initSequenceRAMStore()
)

var (
	exampleTestCases = map[string][]*test.Case{
		"project-1": {
			{
				ID:           "test-case-1",
				Name:         "Test Case 1",
				CreationDate: time.Now().Round(time.Second),
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:      4,
						Message:        "",
						IsMinor:        false,
						Description:    "Fourth version for the first test case",
						DurationHours:  4,
						DurationMin:    20,
						SUTVersionFrom: "2.0.1",
						SUTVersionTo:   test.MaxSUT,
						Steps:          test.ExampleSteps[0],
						CreationDate:   time.Now().Add(-time.Hour * 2),
					},
					1: {
						VersionNr: 3,
						Message: "The third version with a very very very very very long commit message to test how " +
							"very very very very very long commit messages are shown on the screen. Anyways, there is " +
							"no information here why this version was created. This is a minor change! Wow!",
						IsMinor:        true,
						Description:    "The third version which overwrote the second version",
						Preconditions:  "abcdefghijklmnopqrstuvwxyz",
						DurationHours:  0,
						DurationMin:    1,
						SUTVersionFrom: "1.0.2",
						SUTVersionTo:   test.MaxSUT,
						Steps:          test.ExampleSteps[1],
						CreationDate:   time.Now().AddDate(0, 0, -1),
					},
					2: {
						VersionNr:      2,
						Message:        "Create second version",
						IsMinor:        false,
						Description:    "Second version",
						Preconditions:  "Use the force, Luke",
						DurationHours:  25,
						DurationMin:    1,
						SUTVersionFrom: "1.0.1",
						SUTVersionTo:   test.MaxSUT,
						Steps:          test.ExampleSteps[1],
						CreationDate:   time.Now().AddDate(0, 0, -2),
					},
					3: {
						VersionNr:      1,
						Message:        "Initial test case created",
						IsMinor:        false,
						Description:    "Initial test case and first version",
						Preconditions:  "Precondition for the first version",
						DurationHours:  27,
						DurationMin:    15,
						SUTVersionFrom: test.MinSUT,
						SUTVersionTo:   test.MaxSUT,
						Steps:          test.ExampleSteps[0],
						CreationDate:   time.Now().AddDate(-1, 0, 0),
					},
				},
			},
			{
				ID:           "test-case-2",
				Name:         "Test Case 2",
				CreationDate: time.Now().Round(time.Second),
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:      1,
						Description:    "An example test case version for the second test case",
						DurationHours:  1,
						DurationMin:    0,
						SUTVersionFrom: test.MinSUT,
						SUTVersionTo:   test.MaxSUT,
						Steps:          test.ExampleSteps[0],
					},
				},
			},
		},
	}
	exampleTestSequences = map[string][]*test.Sequence{
		"project-1": {

			{
				ID:           "test-sequence-1",
				Name:         "Test Sequence 1",
				CreationDate: time.Now().Round(time.Second),
				SequenceVersions: []test.SequenceVersion{
					0: {
						VersionNr:     2,
						Message:       "Updated sequence",
						IsMinor:       true,
						Description:   "The second version of an example test sequence",
						Preconditions: "Jump up and down. Round and round.",
						CreationDate:  time.Now().Add(-time.Hour * 72),
						Cases:         []test.Case{*exampleTestCases["project-1"][0], *exampleTestCases["project-1"][1]},
					},
					1: {
						VersionNr:     1,
						Message:       "Initial test sequence created",
						IsMinor:       false,
						Description:   "The first version of an example test sequence",
						Preconditions: "Jump up and down",
						CreationDate:  time.Now().AddDate(-4, -2, -1),
						Cases:         []test.Case{*exampleTestCases["project-1"][1]},
					},
				},
			},
			{
				ID:           "test-sequence-2",
				Name:         "Test Sequence 2",
				CreationDate: time.Now().Round(time.Second),
				SequenceVersions: []test.SequenceVersion{
					0: {
						VersionNr:     1,
						Description:   "Another example test sequence",
						Preconditions: "Hands in the air",
						Cases:         []test.Case{*exampleTestCases["project-1"][0]},
					},
				},
			},
		},
	}
)

func init() {
	for pID, tcs := range exampleTestCases {
		for _, tc := range tcs {
			err := caseSingleton.Add("user", pID, tc)

			if err != nil {
				log.Println("Error while initializing test case store:", err)
			}
		}
	}

	for pID, tss := range exampleTestSequences {
		for _, ts := range tss {
			err := sequenceSingleton.Add("user", pID, ts)

			if err != nil {
				log.Println("Error while initializing test case store:", err)
			}
		}
	}
}

// GetCaseStore returns the store used for storing and retrieving test cases
func GetCaseStore() CaseStore {
	return caseSingleton
}

// GetSequenceStore returns the store used for storing and retrieving test sequences
func GetSequenceStore() SequenceStore {
	return sequenceSingleton
}
