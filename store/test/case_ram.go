/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"sync"

	"gitlab.com/stp-team/systemtestportal/domain/test"
)

type caseRAMStore struct {
	sync.RWMutex
	cases map[string]map[string]map[string]*test.Case
}

func initCaseRAMStore() *caseRAMStore {
	return &caseRAMStore{
		cases: make(map[string]map[string]map[string]*test.Case),
	}
}

func (s *caseRAMStore) List(cID, pID string) []*test.Case {
	if s == nil {
		panic("trying to list test cases in a nil store")
	}

	s.RLock()
	defer s.RUnlock()

	pm, ok := s.cases[cID]
	if !ok {
		return nil
	}

	var l []*test.Case
	for _, tc := range pm[pID] {
		l = append(l, tc)
	}
	return l
}

func (s *caseRAMStore) Add(cID, pID string, tc *test.Case) error {
	if s == nil {
		panic("trying to add a test case to a nil store")
	}

	if tc == nil {
		panic("can not add a nil test case to the store")
	}

	s.Lock()
	defer s.Unlock()

	pm, ok := s.cases[cID]
	if !ok {
		pm = make(map[string]map[string]*test.Case)
		s.cases[cID] = pm
	}

	tcm, ok := pm[pID]
	if !ok {
		tcm = make(map[string]*test.Case)
		pm[pID] = tcm
	}

	tcm[tc.ID] = tc
	return nil
}

func (s *caseRAMStore) Get(cID, pID, tcID string) (*test.Case, bool, error) {
	if s == nil {
		panic("trying to get a test case from a nil store")
	}

	s.RLock()
	defer s.RUnlock()

	pm, ok := s.cases[cID]
	if !ok {
		return nil, false, nil
	}

	tcm, ok := pm[pID]
	if !ok {
		return nil, false, nil
	}

	tc, ok := tcm[tcID]
	return tc, ok, nil
}

func (s *caseRAMStore) Delete(cID, pID, tcID string) error {
	if s == nil {
		panic("trying to delete a test case from a nil store")
	}

	s.Lock()
	defer s.Unlock()

	pm, ok := s.cases[cID]
	if !ok {
		return nil
	}

	tcm, ok := pm[pID]
	if !ok {
		return nil
	}

	delete(tcm, tcID)
	return nil
}
