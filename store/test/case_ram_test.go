/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"testing"

	"gitlab.com/stp-team/systemtestportal/domain/test"
)

var testCaseStore = &caseRAMStore{
	cases: map[string]map[string]map[string]*test.Case{
		"user-1": {
			"p0001": {
				"test-case-1": {
					ID:   "test-case-1",
					Name: "Test Case 1",
				},
				"test-case-2": {
					ID:   "test-case-2",
					Name: "Test Case 2",
				},
			},
			"awesome-project": {
				"login": {
					ID:   "login",
					Name: "Login",
				},
				"logout": {
					ID:   "logout",
					Name: "Login",
				},
			},
			"another-project": {
				"Execute test": {
					ID:   "another-project",
					Name: "Anöther Project",
				},
			},
		},
	},
}

var _ CaseStore = initCaseRAMStore()

func TestCaseRAMStoreNilMethodCalls(t *testing.T) {
	var empty *caseRAMStore
	t.Run("List", func(t *testing.T) {
		defer expectPanic(t)
		empty.List("user-1", "awesome-project")
	})
	t.Run("Add (nil store)", func(t *testing.T) {
		defer expectPanic(t)
		empty.Add("user-1", "awesome-project", &test.Case{ID: "login-2"})
	})
	t.Run("Add (nil test case)", func(t *testing.T) {
		defer expectPanic(t)
		initCaseRAMStore().Add("user-1", "awesome-project", nil)
	})
	t.Run("Get", func(t *testing.T) {
		defer expectPanic(t)
		empty.Get("user-1", "awesome-project", "login")
	})
	t.Run("Delete", func(t *testing.T) {
		defer expectPanic(t)
		empty.Delete("user-1", "awesome-project", "login")
	})
}

func TestCaseRAMStoreList(t *testing.T) {
	type args struct {
		cID string
		pID string
	}
	tests := []struct {
		name string
		s    *caseRAMStore
		args args
		want []*test.Case
	}{
		{
			name: "Known container and project",
			s:    testCaseStore,
			args: args{
				cID: "user-1",
				pID: "awesome-project",
			},
			want: []*test.Case{
				testCaseStore.cases["user-1"]["awesome-project"]["login"],
				testCaseStore.cases["user-1"]["awesome-project"]["logout"],
			},
		},
		{
			name: "Unknown container",
			s:    testCaseStore,
			args: args{
				cID: "user-2",
				pID: "p0001",
			},
			want: nil,
		},
		{
			name: "Unknown project",
			s:    testCaseStore,
			args: args{
				cID: "user-1",
				pID: "unknown",
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.List(tt.args.cID, tt.args.pID); !containsUnorderedTestCases(got, tt.want) {
				t.Errorf("caseRAMStore.List() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCaseRAMStoreAdd(t *testing.T) {
	type args struct {
		cID string
		pID string
		tc  *test.Case
	}
	tests := []struct {
		name string
		s    *caseRAMStore
		args args
		want error
	}{
		{
			name: "New container",
			s:    initCaseRAMStore(),
			args: args{
				cID: "user-1",
				pID: "cool-project",
				tc: &test.Case{
					ID:   "cool-test-case",
					Name: "Cool Test Case",
				},
			},
			want: nil,
		},
		{
			name: "New project",
			s: &caseRAMStore{
				cases: map[string]map[string]map[string]*test.Case{
					"user-1": {},
				},
			},
			args: args{
				cID: "user-1",
				pID: "cool-project",
				tc: &test.Case{
					ID:   "cool-test-case",
					Name: "Cool Test Case",
				},
			},
			want: nil,
		},
		{
			name: "Existing container and project",
			s: &caseRAMStore{
				cases: map[string]map[string]map[string]*test.Case{
					"user-1": {
						"cool-project": {},
					},
				},
			},
			args: args{
				cID: "user-1",
				pID: "cool-project",
				tc: &test.Case{
					ID:   "cool-test-case",
					Name: "Cool Test Case",
				},
			},
			want: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.Add(tt.args.cID, tt.args.pID, tt.args.tc); got != tt.want {
				t.Errorf("caseRAMStore.Add() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCaseRAMStoreGet(t *testing.T) {
	type args struct {
		cID  string
		pID  string
		tcID string
	}
	type want struct {
		tc *test.Case
		ok bool
	}
	tests := []struct {
		name string
		s    *caseRAMStore
		args args
		want want
	}{
		{
			name: "Unknown container",
			s:    testCaseStore,
			args: args{
				cID:  "user-2",
				pID:  "p0001",
				tcID: "test-case-2",
			},
			want: want{
				tc: nil,
				ok: false,
			},
		},
		{
			name: "Unknown project",
			s:    testCaseStore,
			args: args{
				cID:  "user-1",
				pID:  "p0002",
				tcID: "test-case-2"},
			want: want{
				tc: nil,
				ok: false,
			},
		},
		{
			name: "Unknown test case",
			s:    testCaseStore,
			args: args{
				cID:  "user-1",
				pID:  "p0001",
				tcID: "test-case-3"},
			want: want{
				tc: nil,
				ok: false,
			},
		},
		{
			name: "Known test case",
			s:    testCaseStore,
			args: args{
				cID:  "user-1",
				pID:  "p0001",
				tcID: "test-case-2"},
			want: want{
				tc: testCaseStore.cases["user-1"]["p0001"]["test-case-2"],
				ok: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotTC, gotOK, gotError := tt.s.Get(tt.args.cID, tt.args.pID, tt.args.tcID)
			if gotTC != tt.want.tc || gotOK != tt.want.ok || gotError != nil {
				t.Errorf("Get(%v, %v, %v) = (%v, %v, %v), want (%v, %v, nil)", tt.args.cID, tt.args.pID,
					tt.args.tcID, gotTC, gotOK, gotError, tt.want.tc, tt.want.ok)
			}

		})
	}
}

func TestCaseRAMStoreDelete(t *testing.T) {
	type args struct {
		cID  string
		pID  string
		tcID string
	}
	tests := []struct {
		name string
		s    *caseRAMStore
		args args
	}{
		{
			name: "Empty store",
			s:    initCaseRAMStore(),
			args: args{
				cID:  "user-1",
				pID:  "p0001",
				tcID: "test-case-1",
			},
		},
		{
			name: "Unknown user",
			s:    testCaseStore,
			args: args{
				cID:  "user-53",
				pID:  "p0001",
				tcID: "test-case-1",
			},
		},
		{
			name: "Unknown project",
			s:    testCaseStore,
			args: args{
				cID:  "user-1",
				pID:  "p0002",
				tcID: "test-case-1",
			},
		},
		{
			name: "Unknown test case",
			s:    testCaseStore,
			args: args{
				cID:  "user-1",
				pID:  "p0001",
				tcID: "test-case-2",
			},
		},
		{
			name: "Known test case",
			s:    testCaseStore,
			args: args{
				cID:  "user-1",
				pID:  "p0001",
				tcID: "test-case-1",
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.s.Delete(tt.args.cID, tt.args.pID, tt.args.tcID)

			if _, found, _ := tt.s.Get(tt.args.cID, tt.args.pID, tt.args.tcID); found {
				t.Errorf("Test case was not deleted")
			}
		})
	}
}

func TestCaseRAMStoreAddAndGet(t *testing.T) {
	type args struct {
		cID string
		pID string
		tc  *test.Case
	}
	tests := []struct {
		name string
		s    *caseRAMStore
		args args
	}{
		{
			name: "Basic example",
			s:    initCaseRAMStore(),
			args: args{
				cID: "user-1",
				pID: "p0001",
				tc: &test.Case{
					ID:   "new-test-case",
					Name: "New Test Case",
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotError := tt.s.Add(tt.args.cID, tt.args.pID, tt.args.tc)

			if gotError != nil {
				t.Errorf("Add(%v, %v, %v) = %v, want nil", tt.args.cID, tt.args.pID, tt.args.tc, gotError)
			}

			gotTC, gotOK, gotError := tt.s.Get(tt.args.cID, tt.args.pID, tt.args.tc.ID)

			if !gotOK || gotTC != tt.args.tc || gotError != nil {
				t.Errorf("Get(%v, %v, %v) = (%v, %v, %v), want (%v, %v, nil)", tt.args.cID, tt.args.pID,
					tt.args.tc.ID, gotTC, gotOK, gotError, tt.args.tc, true)
			}
		})
	}
}

func containsUnorderedTestCases(got, want []*test.Case) bool {
outer:
	for _, w := range want {
		for _, g := range got {
			if w == g {
				continue outer
			}
		}
		return false
	}
	return true
}
