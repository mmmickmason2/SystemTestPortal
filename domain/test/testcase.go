/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"strings"
	"time"

	"math"

	"gitlab.com/stp-team/systemtestportal/web/slugs"
)

// The Case struct contains the information needed for describing a test case
type Case struct {
	ID               string
	Name             string
	Labels           []string
	TestCaseVersions []CaseVersion
	CreationDate     time.Time
}

// CaseVersion contains the information needed for describing a version of a test case
type CaseVersion struct {
	VersionNr      int
	Message        string
	IsMinor        bool
	Description    string
	Preconditions  string
	SUTVersionFrom string
	SUTVersionTo   string
	DurationHours  int
	DurationMin    int
	Steps          []Step
	CreationDate   time.Time
}

// ItemName returns a test cases name
func (tc Case) ItemName() string {
	return tc.Name
}

// ItemID returns a test cases id
func (tc Case) ItemID() string {
	return tc.ID
}

// NewTestCase creates a new test case with the given information
func NewTestCase(name, description, preconditions, sutVerFrom, sutVerTo string, dur time.Duration) Case {
	name = strings.TrimSpace(name)
	creationDate := time.Now().Round(time.Second)

	tc := Case{
		Name:         name,
		CreationDate: creationDate,
	}
	tc.ID = slugs.Slugify(tc.Name)

	initVer := 1
	initMessage := "Initial test case created"

	tcv := NewTestCaseVersion(initVer, false, initMessage, description, preconditions, sutVerFrom, sutVerTo, dur)
	tc.TestCaseVersions = append(tc.TestCaseVersions, tcv)

	return tc
}

// NewTestCaseVersion creates a new version for a test case
// Returns the created test case version
func NewTestCaseVersion(version int, isMinor bool, message, description, preconditions, sutVerFrom, sutVerTo string,
	dur time.Duration) CaseVersion {

	description = strings.TrimSpace(description)
	preconditions = strings.TrimSpace(preconditions)
	message = strings.TrimSpace(message)

	dh := int(math.Floor(dur.Hours()))
	dur -= time.Hour * time.Duration(dh)
	dm := int(math.Floor(dur.Minutes()))

	tcv := CaseVersion{
		VersionNr:      version,
		Message:        message,
		IsMinor:        isMinor,
		Description:    description,
		Preconditions:  preconditions,
		SUTVersionFrom: sutVerFrom,
		SUTVersionTo:   sutVerTo,
		DurationHours:  dh,
		DurationMin:    dm,
		CreationDate:   time.Now().Round(time.Second),
	}

	return tcv
}
