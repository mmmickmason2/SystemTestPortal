/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"reflect"
	"testing"

	"time"
)

// TestNewTestCase contains test cases for the NewTestCase function
func TestNewTestCase(t *testing.T) {
	type tcArgs struct {
		name          string
		description   string
		preconditions string
	}
	tcs := []struct {
		in   tcArgs
		want Case
	}{
		{tcArgs{},
			Case{
				ID:               "",
				Name:             "",
				TestCaseVersions: defaultTCVersion(),
				CreationDate:     time.Now().Round(time.Second)}},
		{tcArgs{name: "  "},
			Case{
				TestCaseVersions: defaultTCVersion(),
				CreationDate:     time.Now().Round(time.Second)}},
		{tcArgs{name: "Test"},
			Case{
				ID:               "test",
				Name:             "Test",
				TestCaseVersions: defaultTCVersion(),
				CreationDate:     time.Now().Round(time.Second)}},
		{tcArgs{name: " Test 1"},
			Case{
				ID:               "test-1",
				Name:             "Test 1",
				TestCaseVersions: defaultTCVersion(),
				CreationDate:     time.Now().Round(time.Second)}},
	}

	for _, tc := range tcs {
		got := NewTestCase(tc.in.name, tc.in.description, tc.in.preconditions, "", "", 0)

		if !reflect.DeepEqual(got, tc.want) {
			t.Errorf("NewTestCase(%s, %s, %s) = %v, want %v", tc.in.name, tc.in.description, tc.in.preconditions, got, tc.want)
		}
	}
}

func defaultTCVersion() []CaseVersion {
	return []CaseVersion{
		{
			VersionNr:    1,
			Message:      "Initial test case created",
			IsMinor:      false,
			CreationDate: time.Now().Round(time.Second),
		},
	}
}
