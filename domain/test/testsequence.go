/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"strings"
	"time"

	"strconv"

	"fmt"

	"gitlab.com/stp-team/systemtestportal/web/slugs"
)

// The Sequence struct contains information for describing a test sequence
type Sequence struct {
	ID               string
	Name             string
	Labels           []string
	CreationDate     time.Time
	SequenceVersions []SequenceVersion
}

// SequenceVersion contains information for describing a version of a test sequence
type SequenceVersion struct {
	VersionNr     int
	Message       string
	IsMinor       bool
	Description   string
	Preconditions string
	CreationDate  time.Time
	Cases         []Case
	SequenceInfos SequenceInfo
}

// SequenceInfo contains meta information for a SequenceVersion
type SequenceInfo struct {
	SUTVersionFrom string
	SUTVersionTo   string
	DurationHours  int
	DurationMin    int
}

// ItemEstimatedDuration returns the estimated Duration for the sequence
func (tsv SequenceVersion) ItemEstimatedDuration() (minutes int, hours int) {
	minutes = 0
	hours = 0

	for _, c := range tsv.Cases {
		minutes += c.TestCaseVersions[0].DurationMin
		hours += c.TestCaseVersions[0].DurationHours
	}
	return
}

// ItemName returns a test sequences name
func (ts Sequence) ItemName() string {
	return ts.Name
}

// CountIncludedSteps returns the total number of steps summed over all included cases
func (tsv SequenceVersion) CountIncludedSteps() int {
	result := 0
	for _, tc := range tsv.Cases {
		result += len(tc.TestCaseVersions[0].Steps)
	}
	return result
}

// CountIncludedStepsUpTo returns the total number of steps summed over all included cases up to maxCaseNr
// the numeration starts with 0 and maxCaseNr itself will be included
func (tsv SequenceVersion) CountIncludedStepsUpTo(maxCaseNr int) int {
	result := 0
	for i := 0; i <= maxCaseNr && i < len(tsv.Cases); i++ {
		tcv := tsv.Cases[i].TestCaseVersions[0]
		result += len(tcv.Steps)
	}
	return result
}

// ItemID returns a test sequences id
func (ts Sequence) ItemID() string {
	return ts.ID
}

const (
	MinSUT = "Oldest Version"
	MaxSUT = "Current Version"
)

// NewTestSequence creates a new test sequence with the given information
func NewTestSequence(name, description, preconditions string, cases []Case) (Sequence, error) {
	name = strings.TrimSpace(name)
	creationDate := time.Now().Round(time.Second)

	ts := Sequence{Name: name, CreationDate: creationDate}
	ts.ID = slugs.Slugify(ts.Name)

	initVer := 1
	initMessage := "Initial test sequence created"

	tsv, err := NewTestSequenceVersion(initVer, false, initMessage, description, preconditions, cases)

	ts.SequenceVersions = append(ts.SequenceVersions, tsv)

	return ts, err
}

// NewTestSequenceVersion creates a new version for a test case
// Returns the test case the version belongs to
func NewTestSequenceVersion(version int, isMinor bool, message, description string, preconditions string,
	cases []Case) (SequenceVersion, error) {

	description = strings.TrimSpace(description)
	preconditions = strings.TrimSpace(preconditions)
	message = strings.TrimSpace(message)
	sutf, err := CalculateSUTVersion(cases, MaxSUT, MinSUT, "From")
	if err != nil {
		return SequenceVersion{}, err
	}
	sutt, err := CalculateSUTVersion(cases, MinSUT, MaxSUT, "To")
	if err != nil {
		return SequenceVersion{}, err
	}
	dh, dm := CalculateDuration(cases)

	si := SequenceInfo{
		SUTVersionFrom: sutf,
		SUTVersionTo:   sutt,
		DurationHours:  dh,
		DurationMin:    dm,
	}

	tsv := SequenceVersion{
		VersionNr:     version,
		Message:       message,
		IsMinor:       isMinor,
		Description:   description,
		Preconditions: preconditions,
		CreationDate:  time.Now().Round(time.Second),
		Cases:         cases,
		SequenceInfos: si,
	}

	return tsv, nil
}

func getVersionByType(v CaseVersion, sutType string) string {
	if sutType == "From" {
		return v.SUTVersionFrom
	}
	return v.SUTVersionTo
}

func compareByType(v, sut, lower, upper, sutType string) (int, error) {
	if sutType == "From" {
		return compareSUTVersions(v, sut, lower, upper)
	}
	return compareSUTVersions(sut, v, upper, lower)
}

//CalculateSUTVersion calculates the SUTVersionFrom or the SUTVersionTo depending on the given sutType parameter
func CalculateSUTVersion(cases []Case, upperBound, lowerBound, sutType string) (string, error) {
	var sut = lowerBound
	for _, c := range cases {
		v := getVersionByType(c.TestCaseVersions[0], sutType)
		r, err := compareByType(v, sut, lowerBound, upperBound, sutType)
		if err != nil {
			return sut, fmt.Errorf("couldn't get %s-sut-version because %s", sutType, err)
		}
		if r > 0 {
			sut = v
		}
	}
	return sut, nil
}

// compareSUTVersions compares two sut-versions and returns
// a value < 0 if a < b, > 0 if a > b and = 0 if a = b.
func compareSUTVersions(a, b, lower, upper string) (int, error) {
	if a == b {
		return 0, nil
	} else if a == upper || b == lower {
		return 1, nil
	} else if a == lower || b == upper {
		return -1, nil
	}
	partsA := strings.Split(a, ".")
	partsB := strings.Split(b, ".")
	for i := 0; i < min(len(partsA), len(partsB)); i++ {
		numA, err := strconv.Atoi(partsA[i])
		if err != nil {
			return 0, parsePartError(a, b, err, i)
		}
		numB, err := strconv.Atoi(partsB[i])
		if err != nil {
			return 0, parsePartError(a, b, err, i)
		}
		if numA != numB {
			return numA - numB, nil
		}
	}
	return 0, nil
}

func parsePartError(a, b string, err error, i int) error {
	return fmt.Errorf("failed to compare sut versions %s and %s,"+
		" because %s at part %d", a, b, err, i)
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// CalculateDuration calculates the durationHours and durationMin for testsequences
func CalculateDuration(cases []Case) (int, int) {
	var dh = 0
	var dm = 0
	var tcv = CaseVersion{}
	for _, c := range cases {
		tcv = c.TestCaseVersions[0]
		dh += tcv.DurationHours
		dm += tcv.DurationMin
	}

	dh += dm / 60
	dm = dm % 60

	return dh, dm
}
