/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/stp-team/systemtestportal/web/slugs"
)

// TestNewTestSequence contains tests for the creation function of test sequences
func TestNewTestSequence(t *testing.T) {
	type tsArgs struct {
		name          string
		description   string
		preconditions string
		cases         []Case
	}
	tss := []struct {
		in   tsArgs
		want Sequence
	}{
		{tsArgs{
			cases: []Case{},
		},
			Sequence{
				ID:               "",
				Name:             "",
				CreationDate:     time.Now().Round(time.Second),
				SequenceVersions: defaultTSVersion(),
			},
		},
		{tsArgs{
			name:  "    ",
			cases: []Case{},
		},
			Sequence{
				CreationDate:     time.Now().Round(time.Second),
				SequenceVersions: defaultTSVersion(),
			},
		},
		{tsArgs{name: "Test", cases: []Case{}},
			wantSequence("Test"),
		},
		{tsArgs{name: "Test 1", cases: []Case{}},
			wantSequence("Test 1"),
		},
	}

	for _, ts := range tss {
		got, _ := NewTestSequence(ts.in.name, ts.in.description, ts.in.preconditions, ts.in.cases)

		if !reflect.DeepEqual(got, ts.want) {
			t.Errorf("NewTestSequence(%s, %s, %s, %v) = \n%v \nwant \n%v", ts.in.name, ts.in.description,
				ts.in.preconditions, ts.in.cases, got, ts.want)
		}
	}
}

func wantSequence(name string) Sequence {
	return Sequence{
		ID:               slugs.Slugify(name),
		Name:             name,
		CreationDate:     time.Now().Round(time.Second),
		SequenceVersions: defaultTSVersion(),
	}
}

// defaultTSVersion returns the default version of a test sequence that is created when the test sequence is created
func defaultTSVersion() []SequenceVersion {
	return []SequenceVersion{
		{
			VersionNr:    1,
			Message:      "Initial test sequence created",
			IsMinor:      false,
			CreationDate: time.Now().Round(time.Second),
			Cases:        []Case{},
			SequenceInfos: SequenceInfo{
				SUTVersionFrom: MinSUT,
				SUTVersionTo:   MaxSUT,
				DurationHours:  0,
				DurationMin:    0,
			},
		},
	}
}

// TestCalculateSUTVersionFrom tests the calculation of the SUTVersionFrom of a testsequence
func TestCalculateSUTVersionFrom(t *testing.T) {
	sutf, _ := CalculateSUTVersion(getTestCases(), MaxSUT, MinSUT, "From")
	if sutf != "2.0.1" {
		t.Errorf("CalculateSUTVersionFrom: got: %s want: %s", sutf, "2.0.1")
	}
	sutf, _ = CalculateSUTVersion(getTestCases2(), MaxSUT, MinSUT, "From")
	if sutf != MaxSUT {
		t.Errorf("CalculateSUTVersionFrom: got: %s want: %s", sutf, MaxSUT)
	}
	sutf, _ = CalculateSUTVersion(getTestCases3(), MaxSUT, MinSUT, "From")
	if sutf != "1.13.111" {
		t.Errorf("CalculateSUTVersionFrom: got: %s want: %s", sutf, "1.13.111")
	}
	_, err := CalculateSUTVersion(getTestCasesErrors(), MaxSUT, MinSUT, "From")
	if err == nil {
		t.Errorf("Expected error but got %v for invalid version number!", err)
	}
}

// TestCalculateSUTVersionFrom tests the calculation of the SUTVersionTo of a testsequence
func TestCalculateSUTVersionTo(t *testing.T) {
	sutt, _ := CalculateSUTVersion(getTestCases(), MinSUT, MaxSUT, "To")
	if sutt != "2.0.1" {
		t.Errorf("CalculateSUTVersionTo: got: %s want: %s", sutt, "2.0.1")
	}
	sutt, _ = CalculateSUTVersion(getTestCases2(), MinSUT, MaxSUT, "To")
	if sutt != MinSUT {
		t.Errorf("CalculateSUTVersionTo: got: %s want: %s", sutt, MinSUT)
	}
	sutt, _ = CalculateSUTVersion(getTestCases3(), MinSUT, MaxSUT, "To")
	if sutt != "1.15.06" {
		t.Errorf("CalculateSUTVersionTo: got: %s want: %s", sutt, "1.15.06")
	}
	_, err := CalculateSUTVersion(getTestCasesErrors(), MinSUT, MaxSUT, "To")
	if err == nil {
		t.Errorf("Expected error but got %v for invalid version number!", err)
	}
}

// TestCalculateSUTVersionFrom tests the calculation of the Duration of a testsequence
func TestCalculateDuration(t *testing.T) {
	var cases = getTestCases()
	dh, dm := CalculateDuration(cases)
	if dh != 4 && dm != 0 {
		t.Errorf("CalculateSUTVersionFrom: got: %d %d want: %d %d", dh, dm, 4, 0)
	}
}

//getTestCases returns some example testcases
func getTestCases() []Case {
	var cases = []Case{
		{
			ID:           "test-case-1",
			Name:         "Test Case 1",
			CreationDate: time.Now().Round(time.Second),
			TestCaseVersions: []CaseVersion{
				0: {
					VersionNr:      1,
					Description:    "An example test case version for the first test case",
					DurationHours:  1,
					DurationMin:    1,
					SUTVersionFrom: "2.0.1",
					SUTVersionTo:   "2.0.1",
					Steps:          ExampleSteps[0],
				},
			},
		},
		{
			ID:           "test-case-2",
			Name:         "Test Case 2",
			CreationDate: time.Now().Round(time.Second),
			TestCaseVersions: []CaseVersion{
				0: {
					VersionNr:      1,
					Description:    "An example test case version for the second test case",
					DurationHours:  2,
					DurationMin:    59,
					SUTVersionFrom: "1.0.5",
					SUTVersionTo:   "3.0.5",
					Steps:          ExampleSteps[0],
				},
			},
		},
	}
	return cases
}

//getTestCases returns some example testcases
func getTestCases2() []Case {
	var cases = []Case{
		{
			ID:           "test-case-1",
			Name:         "Test Case 1",
			CreationDate: time.Now().Round(time.Second),
			TestCaseVersions: []CaseVersion{
				0: {
					VersionNr:      1,
					Description:    "An example test case version for the first test case",
					DurationHours:  1,
					DurationMin:    1,
					SUTVersionFrom: MaxSUT,
					SUTVersionTo:   MaxSUT,
					Steps:          ExampleSteps[0],
				},
			},
		},
		{
			ID:           "test-case-2",
			Name:         "Test Case 2",
			CreationDate: time.Now().Round(time.Second),
			TestCaseVersions: []CaseVersion{
				0: {
					VersionNr:      1,
					Description:    "An example test case version for the second test case",
					DurationHours:  2,
					DurationMin:    59,
					SUTVersionFrom: MinSUT,
					SUTVersionTo:   MinSUT,
					Steps:          ExampleSteps[0],
				},
			},
		},
	}
	return cases
}

//getTestCases3 returns some example testcases
func getTestCases3() []Case {
	var cases = []Case{
		NewTestCase("tc1", "", "",
			"1.13.110", "1.16.0", time.Hour*5),
		NewTestCase("tc2", "", "",
			"1.13.111", "1.15.06", time.Hour*3),
		NewTestCase("tc3", "", "",
			"1.0.0.0", "2.0.0.0", time.Hour*1),
	}
	return cases
}

//getTestCases3 returns some example testcases
func getTestCasesErrors() []Case {
	var cases = []Case{
		NewTestCase("tc1", "", "",
			"abc", "lk.ö", time.Hour*5),
		NewTestCase("tc2", "", "",
			"okay", "Version k", time.Hour*5),
	}
	return cases
}
