/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

// The Step struct contains information for describing a single step of a test case
type Step struct {
	Action         string
	ActualResult   Result
	ExpectedResult string
	Assessment     string
	ID             int
}

// NewTestStep creates a new Step with action and expected result from parameters
func NewTestStep(action, expect string) Step {
	s := Step{action, NotAssessed, expect, "", 0}
	return s
}

// ExampleSteps is slice of exemplary Step-slices for use in tests and initialization
var ExampleSteps = []([]Step){{
	Step{"Open the application", NotAssessed,
		"The application should display the start screen", "", 1},
	Step{"Click on login", NotAssessed,
		"The application should display the login modal", "", 2},
	Step{"Enter user name and password", NotAssessed,
		"The user can see that he entered symbols into input fields", "", 3},
}, {
	Step{"The user uses the application", NotAssessed,
		"The application appropriately responses to the user input", "", 1},
}}
