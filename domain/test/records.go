/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"reflect"
	"time"
)

//CaseExecutionRecord contains all information relating to an execution of a test case
type CaseExecutionRecord struct {
	ID                int
	TestCaseID        string
	TestCaseVersionNr int
	ProjectID         string

	SUTVersion    string
	Environment   string
	ExecutionDate time.Time

	StepRecords     []StepExecutionRecord
	Result          Result
	Comment         string
	OtherNeededTime time.Duration
}

//StepExecutionRecord contains all information relating to an execution of a test step
type StepExecutionRecord struct {
	NeededTime       time.Duration
	ObservedBehavior string
	Result           Result
	Comment          string
	Visited          bool
}

//DeepCopy returns a copy, that is completely independent of its original, but equal.
func (s StepExecutionRecord) DeepCopy() StepExecutionRecord {
	result := StepExecutionRecord{
		NeededTime:       s.NeededTime,
		ObservedBehavior: s.ObservedBehavior,
		Result:           s.Result,
		Comment:          s.Comment,
		Visited:          s.Visited,
	}
	return result
}

//CaseExecutionRecordID embraces all values needed to identify a case record
type CaseExecutionRecordID struct {
	ID         int
	TestCaseID string
	ProjectID  string
}

/*SequenceExecutionRecord contains all information relating to an execution of a test sequence itself.
Data referring to the single test cases are saved in the corresponding CaseExecutionRecord
*/
type SequenceExecutionRecord struct {
	ID                    int
	TestSequenceID        string
	TestSequenceVersionNr int
	ProjectID             string
	SUTVersion            string
	Environment           string
	CaseExecutionRecords  []CaseExecutionRecordID
}

// NewCaseExecutionRecord creates a new execution record of a test case execution with the given information
func NewCaseExecutionRecord(TestCase Case, TestCaseVersionNr int, projectID string, SUTVersion string,
	Environment string) CaseExecutionRecord {

	countSteps := len(TestCase.TestCaseVersions[len(TestCase.TestCaseVersions)-TestCaseVersionNr].Steps)

	record := CaseExecutionRecord{
		ID:                0,
		TestCaseID:        TestCase.ID,
		TestCaseVersionNr: TestCaseVersionNr,
		ProjectID:         projectID,
		SUTVersion:        SUTVersion,
		Environment:       Environment,

		ExecutionDate:   time.Now().Round(time.Second),
		StepRecords:     make([]StepExecutionRecord, countSteps),
		Result:          NotAssessed,
		Comment:         "",
		OtherNeededTime: 0,
	}
	return record
}

// NewSequenceExecutionRecord creates a new execution record of a test sequence execution with the given information
func NewSequenceExecutionRecord(testSequence Sequence, testSequenceVersionNr int,
	projectID string, sutVersion string, environment string) SequenceExecutionRecord {

	record := SequenceExecutionRecord{
		ID:                    0,
		TestSequenceID:        testSequence.ID,
		TestSequenceVersionNr: testSequenceVersionNr,
		ProjectID:             projectID,
		CaseExecutionRecords:  nil,
		SUTVersion:            sutVersion,
		Environment:           environment,
	}
	return record
}

//AddCaseExecutionRecord adds an case record to the sequence record
func (s *SequenceExecutionRecord) AddCaseExecutionRecord(id int, testCaseID string, projectID string) {
	s.CaseExecutionRecords = append(s.CaseExecutionRecords,
		CaseExecutionRecordID{id, testCaseID, projectID})
}

/*Equals compares two CaseExecutionRecords and returns true, if these two are equal.
If in is nil, Equals will return false.
*/
func (rec CaseExecutionRecord) Equals(in interface{}) bool {
	return reflect.DeepEqual(rec, in)
}

//DeepCopy returns a copy, that is completely independent of its original.
func (rec CaseExecutionRecord) DeepCopy() CaseExecutionRecord {
	result := CaseExecutionRecord{
		ID:                rec.ID,
		TestCaseID:        rec.TestCaseID,
		TestCaseVersionNr: rec.TestCaseVersionNr,
		ProjectID:         rec.ProjectID,
		SUTVersion:        rec.SUTVersion,
		Environment:       rec.Environment,

		ExecutionDate:   rec.ExecutionDate,
		StepRecords:     make([]StepExecutionRecord, len(rec.StepRecords)),
		Result:          rec.Result,
		Comment:         rec.Comment,
		OtherNeededTime: rec.OtherNeededTime,
	}
	copy(result.StepRecords, rec.StepRecords)
	return result
}

//IsFinished checks, if the TestCase was executed till end or was interrupted and isn't completed yet.
func (rec *CaseExecutionRecord) IsFinished() bool {
	result := true
	for _, step := range rec.StepRecords {
		result = result && step.Visited
	}
	return result
}

//SaveStep will be called, if the user executed one test step. The results of the execution will be saved to the record.
//stepNr starts with step 1
func (rec *CaseExecutionRecord) SaveStep(stepNr int, observedBehavior string, result Result, comment string,
	neededTime time.Duration) {
	stepRec := StepExecutionRecord{
		NeededTime:       neededTime,
		ObservedBehavior: observedBehavior,
		Result:           result,
		Comment:          comment,
		Visited:          true,
	}

	rec.StepRecords[stepNr-1] = stepRec
}

//Finish will finish the Record and saves over all Data.
func (rec *CaseExecutionRecord) Finish(result Result, comment string, neededTime time.Duration) {
	rec.Result = result
	rec.Comment = comment
	rec.OtherNeededTime = neededTime
}

//GetNeededTime returns the summed up time, needed for this execution so far.
func (rec *CaseExecutionRecord) GetNeededTime() time.Duration {
	var result = rec.OtherNeededTime
	for _, v := range rec.StepRecords {
		result += v.NeededTime
	}
	return result
}

/*Equals compares two SequenceExecutionRecord and returns true, if these two are equal.
The contained CaseExecutionRecords will be compared too.
If in is nil, Equals will return false.
*/
func (s SequenceExecutionRecord) Equals(in interface{}) bool {
	return reflect.DeepEqual(s, in)
}
