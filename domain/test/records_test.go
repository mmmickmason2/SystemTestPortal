/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"testing"
	"time"
)

var stepRec1 = StepExecutionRecord{
	NeededTime:       4 * time.Minute,
	ObservedBehavior: "",
	Result:           NotAssessed,
	Comment:          "",
	Visited:          true,
}
var stepRec2 = StepExecutionRecord{
	NeededTime:       10 * time.Minute,
	ObservedBehavior: "",
	Result:           Pass,
	Comment:          "Hello?",
	Visited:          true,
}
var stepRec3 = StepExecutionRecord{
	NeededTime:       22 * time.Minute,
	ObservedBehavior: "Something happened",
	Result:           PassWithComment,
	Comment:          "What's up?",
	Visited:          true,
}
var stepRec4 = StepExecutionRecord{
	NeededTime:       0,
	ObservedBehavior: "",
	Result:           NotAssessed,
	Comment:          "",
	Visited:          true,
}
var stepRec5 = StepExecutionRecord{
	NeededTime:       0,
	ObservedBehavior: "",
	Result:           NotAssessed,
	Comment:          "",
	Visited:          true,
}
var stepRec6 = StepExecutionRecord{
	NeededTime:       1 * time.Minute,
	ObservedBehavior: "",
	Result:           NotAssessed,
	Comment:          "",
	Visited:          true,
}
var defaultCaseRecord = CaseExecutionRecord{
	TestCaseID:        "123456G",
	TestCaseVersionNr: 2,
	ProjectID:         "d8ed5f42",
	SUTVersion:        "Version 1.02",
	Environment:       "Win 10",

	ExecutionDate:   time.Date(2000, 12, 24, 12, 0, 0, 0, time.UTC),
	StepRecords:     []StepExecutionRecord{stepRec1, stepRec2, stepRec3, stepRec4, stepRec5, stepRec6},
	Result:          NotAssessed,
	Comment:         "All OK",
	OtherNeededTime: 55 * time.Second,
}

func assertEqual(actual, target interface{}, message string, t *testing.T) {
	if actual != target {
		t.Errorf(message, actual, target)
	}
}

func TestNewSequenceExecutionRecord(t *testing.T) {
	const SeqIDErrorMessage = "TestSequenceID wasn't transferred correctly to new Record. " +
		"Value in new Record is %q but expected %q"
	const ProjIDErrorMessage = "ProjectID wasn't transferred correctly to new Record. " +
		"Value in new Record is %q but expected %q"
	const SeqVersErrorMessage = "TestSequenceVersionNr wasn't transferred correctly to new Record. " +
		"Value in new Record is %d but expected %d"
	const EnvErrorMessage = "Environment wasn't transferred correctly to new Record. " +
		"Value in new Record is %q but expected %q"
	const SUTVersErrorMessage = "SUTVersion wasn't transferred correctly to new Record. " +
		"Value in new Record is %q but expected %q"

	TestSequenceVersionNr := 0

	ProjectID := "d8ed5f42"

	TestCase1 := NewTestCase("Case 1", "Do things", "booted", "1.0", "3.0", 5*time.Minute)
	TestCase2 := NewTestCase("Case 2", "Do other things", "Case 1", "1.0", "3.0", 3*time.Minute)
	TestSeq, _ := NewTestSequence("Seq", "Do things", "boot", []Case{TestCase1, TestCase2})
	TestSeqID := TestSeq.ID
	SUTVersion := "1.0.1"
	Environment := "Win 10"

	TestSeqRec := NewSequenceExecutionRecord(TestSeq, TestSequenceVersionNr, ProjectID, SUTVersion, Environment)

	if TestSeqRec.TestSequenceID != TestSeqID {
		t.Errorf(SeqIDErrorMessage, TestSeqRec.TestSequenceID, TestSeqID)
	}
	if TestSeqRec.ProjectID != ProjectID {
		t.Errorf(ProjIDErrorMessage, TestSeqRec.ProjectID, ProjectID)
	}
	if TestSeqRec.TestSequenceVersionNr != TestSequenceVersionNr {
		t.Errorf(SeqVersErrorMessage, TestSeqRec.TestSequenceVersionNr, TestSequenceVersionNr)
	}
	if TestSeqRec.SUTVersion != SUTVersion {
		t.Errorf(SUTVersErrorMessage, TestSeqRec.SUTVersion, SUTVersion)
	}
	if TestSeqRec.Environment != Environment {
		t.Errorf(EnvErrorMessage, TestSeqRec.Environment, Environment)
	}
}

func TestSequenceExecutionRecord_Equals(t *testing.T) {
	a := defaultCaseRecord.DeepCopy()
	stepRec1_1 := stepRec1.DeepCopy()
	stepRec1_1.NeededTime = 0

	b := CaseExecutionRecord{
		TestCaseID:        "987654C",
		TestCaseVersionNr: 1,
		ProjectID:         "d8ed5f42",
		SUTVersion:        "Version 1.87",
		Environment:       "Unix",

		ExecutionDate: time.Date(2005, 12, 24, 12, 0, 0, 0, time.UTC),
		StepRecords:   []StepExecutionRecord{stepRec1_1, stepRec2, stepRec3, stepRec4, stepRec5, stepRec6},
		Result:        Pass,
		Comment:       "Just minor fails",
	}
	c := a.DeepCopy()
	d := b.DeepCopy()

	seq1 := SequenceExecutionRecord{
		TestSequenceID:        "54dfg8sd",
		TestSequenceVersionNr: 1,
		ProjectID:             "asf885sdf7",
		SUTVersion:            "1.0.1",
		Environment:           "Win 10",
		CaseExecutionRecords: []CaseExecutionRecordID{
			{a.ID, a.TestCaseID, a.ProjectID},
			{b.ID, b.TestCaseID, b.ProjectID},
		},
	}
	seq2 := SequenceExecutionRecord{
		TestSequenceID:        "54dfg8sd",
		TestSequenceVersionNr: 1,
		ProjectID:             "asf885sdf7",
		SUTVersion:            "1.0.1",
		Environment:           "Win 10",
		CaseExecutionRecords: []CaseExecutionRecordID{
			{c.ID, c.TestCaseID, c.ProjectID},
			{d.ID, d.TestCaseID, d.ProjectID},
		},
	}
	if !seq1.Equals(seq2) {
		t.Errorf("seq1 is equal to seq2 but equals-function returned false")
	}

	seq2.CaseExecutionRecords[0].ID = 42
	if seq1.Equals(seq2) {
		t.Errorf("seq1 isn't equal to seq2 but equals-function returned true. " +
			"(TestCaseVersionNr of first CaseRecord in seq2 was changed)")
	}
	seq2.CaseExecutionRecords = []CaseExecutionRecordID{{c.ID, c.TestCaseID, c.ProjectID}}
	if seq1.Equals(seq2) {
		t.Errorf("seq1 isn't equal to seq2 but equals-function returned true. " +
			"(seq2 has only one entry but seq1 has two)")
	}

	seq2.CaseExecutionRecords = nil
	if seq1.Equals(seq2) {
		t.Errorf("seq2 is nil and seq1 isn't so false was expected but equals-function returned true")
	}
}

func TestCaseExecutionRecord_GetNeededTime(t *testing.T) {
	a := defaultCaseRecord.DeepCopy()
	a.StepRecords[0].NeededTime = 4 * time.Minute
	a.StepRecords[1].NeededTime = 10 * time.Minute
	a.StepRecords[2].NeededTime = 22 * time.Minute
	a.StepRecords[3].NeededTime = 0
	a.StepRecords[4].NeededTime = 0
	a.StepRecords[5].NeededTime = 1 * time.Minute
	a.OtherNeededTime = 1 * time.Minute

	expectedResult := 38 * time.Minute

	if a.GetNeededTime() != expectedResult {
		t.Errorf("GetNeededTime should return %v but returned %v", expectedResult, a.GetNeededTime())
	}
}

func TestCaseExecutionRecord_Finish(t *testing.T) {
	const OverAllResultErrorMessage = "Result wasn't saved correctly to Record. " +
		"Result is %s but expected %s"
	const OverAllCommentErrorMessage = "Comment wasn't saved correctly to Record. " +
		"Comment is %s but expected %s"
	const NeededTimeErrorMessage = "NeededTime wasn't saved correctly to Record."

	TestCase := NewTestCase("Test-TestCase", "Desc", "None", "Version 1", "Version 2", 20*time.Minute)
	TestCaseVersionNr := 1
	ProjectID := "d8ed5f42"
	SUTVersion := "Version 1.02"
	Environment := "Win 10"

	OverAllResult := PassWithComment
	OverAllComment := "All OK"
	neededTime := 1 * time.Minute

	a := NewCaseExecutionRecord(TestCase, TestCaseVersionNr, ProjectID, SUTVersion, Environment)
	neededTimeOld := a.GetNeededTime()
	a.Finish(OverAllResult, OverAllComment, neededTime)

	if a.Result != OverAllResult {
		t.Errorf(OverAllResultErrorMessage, a.Result, OverAllResult)
	}
	if a.Comment != OverAllComment {
		t.Errorf(OverAllCommentErrorMessage, a.Comment, OverAllComment)
	}
	if a.OtherNeededTime != neededTime || neededTimeOld+neededTime != a.GetNeededTime() {
		t.Error(NeededTimeErrorMessage)
	}
}

func TestCaseExecutionRecord_SaveStep(t *testing.T) {
	const ObservedBehaviorsErrorMessage = "ObservedBehavior wasn't saved correctly to Record. " +
		"ObservedBehavior is %q but expected %q"
	const ResultsErrorMessage = "Result wasn't saved correctly to Record. Result is %q but expected %q"
	const CommentsErrorMessage = "Comment wasn't saved correctly to Record. Comment is %q but expected %q"
	const NeededTimeErrorMessage = "NeededTime wasn't saved correctly to Record. NeededTime is %q but expected %q"
	const VisitedErrorMessage = "Saved test step was marked as not visited, but was visited"

	TestCaseVersionNr := 1
	ProjectID := "d8ed5f42"
	SUTVersion := "Version 1.02"
	Environment := "Win 10"

	TestStep := Step{"Do something!", 0, "All Ok", "Think it's ok", 5}
	TestCase := NewTestCase("Test-TestCase", "Desc", "None", "Version 1", "Version 2", 20*time.Minute)
	tcvIndex := len(TestCase.TestCaseVersions) - TestCaseVersionNr
	TestCase.TestCaseVersions[tcvIndex].Steps = []Step{TestStep, TestStep, TestStep}

	StepNr := 2
	ObservedBehavior := "Nothing Happened"
	result := Fail
	Comment := ""
	NeededTime := 2 * time.Minute

	a := NewCaseExecutionRecord(TestCase, TestCaseVersionNr, ProjectID, SUTVersion, Environment)
	a.SaveStep(StepNr, ObservedBehavior, result, Comment, NeededTime)

	if a.StepRecords[StepNr-1].ObservedBehavior != ObservedBehavior {
		t.Errorf(ObservedBehaviorsErrorMessage, a.StepRecords[StepNr-1].ObservedBehavior, ObservedBehavior)
	}
	if a.StepRecords[StepNr-1].Result != result {
		t.Errorf(ResultsErrorMessage, a.StepRecords[StepNr-1].Result, result)
	}
	if a.StepRecords[StepNr-1].Comment != Comment {
		t.Errorf(CommentsErrorMessage, a.StepRecords[StepNr-1].Comment, Comment)
	}
	if a.StepRecords[StepNr-1].NeededTime != NeededTime {
		t.Errorf(NeededTimeErrorMessage, a.StepRecords[StepNr-1].NeededTime, NeededTime)
	}
	if !a.StepRecords[StepNr-1].Visited {
		t.Error(VisitedErrorMessage)
	}
}

func TestNewCaseExecutionRecord(t *testing.T) {
	const IDErrorMessage = "TestCaseID wasn't transferred correctly to new Record. " +
		"Value in new Record is %q but expected %q"
	const VersionErrorMessage = "TestCaseVersionNr wasn't transferred correctly to new Record. " +
		"Value in new Record is %d but expected %d"
	const SUTErrorMessage = "SUTVersion wasn't transferred correctly to new Record. " +
		"Value in new Record is %q but expected %q"
	const EnvironmentErrorMessage = "Environment wasn't transferred correctly to new Record. " +
		"Value in new Record is %q but expected %q"
	const DateErrorMessage = "ExecutionDate wasn't saved correctly to new Record. " +
		"Value in new Record is %v but expected current time: %v"
	const OAResultErrorMessage = "Result should be %s but was %s"
	const ProjectIDErrorMessage = "ProjectID wasn't transferred correctly to new Record. " +
		"Value in new Record is %q but expected %q"

	TestCase := NewTestCase("Test-TestCase", "Desc", "None", "Version 1", "Version 2", 20*time.Minute)
	TestCaseID := TestCase.ID

	TestCaseVersionNr := 1
	ProjectID := "d8ed5f42"
	SUTVersion := "Version 1.02"
	Environment := "Win 10"

	a := NewCaseExecutionRecord(TestCase, TestCaseVersionNr, ProjectID, SUTVersion, Environment)

	if a.TestCaseID != TestCaseID {
		t.Errorf(IDErrorMessage, a.TestCaseID, TestCaseID)
	}
	if a.TestCaseVersionNr != TestCaseVersionNr {
		t.Errorf(VersionErrorMessage, a.TestCaseVersionNr, TestCaseVersionNr)
	}
	if a.ProjectID != ProjectID {
		t.Errorf(ProjectIDErrorMessage, a.ProjectID, ProjectID)
	}
	if a.SUTVersion != SUTVersion {
		t.Errorf(SUTErrorMessage, a.SUTVersion, SUTVersion)
	}
	if a.Environment != Environment {
		t.Errorf(EnvironmentErrorMessage, a.Environment, Environment)
	}

	if a.ExecutionDate.Round(time.Minute) != time.Now().Round(time.Minute) {
		t.Errorf(DateErrorMessage, a.ExecutionDate.Round(time.Minute), time.Now().Round(time.Minute))
	}
	if a.Result != NotAssessed {
		t.Errorf(OAResultErrorMessage, a.Result, NotAssessed)
	}
}

func TestCaseExecutionRecord_DeepCopy(t *testing.T) {

	const IDErrorMessage = "Original TestCaseID was changed by changing the DeepCopy. " +
		"Original is %q but expected %q"
	const VersionErrorMessage = "Original TestCaseVersionNr was changed by changing the DeepCopy. " +
		"Original is %d but expected %d"
	const SUTErrorMessage = "Original SUTVersion was changed by changing the DeepCopy. " +
		"Original is %q but expected %q"
	const EnvironmentErrorMessage = "Original Environment was changed by changing the DeepCopy. " +
		"Original is %q but expected %q"
	const DateErrorMessage = "Original ExecutionDate was changed by changing the DeepCopy. " +
		"Original is %v but expected %v"
	const NeededTimesErrorMessage = "Original NeededTimes[0] was changed by changing the DeepCopy. " +
		"Original is %v but expected %v"
	const ObsBehaviorsErrorMessage = "Original ObservedBehaviors[2] was changed by changing the DeepCopy. " +
		"Original is %q but expected %q"
	const ResultsErrorMessage = "Original Results[0] was changed by changing the DeepCopy. " +
		"Original is %s but expected %s"
	const CommentsErrorMessage = "Original Comments[1] was changed by changing the DeepCopy. " +
		"Original is %q but expected %q"
	const OAResult = "Original Result was changed by changing the DeepCopy. " +
		"Original is %s but expected %s"
	const OAComment = "Original Comment was changed by changing the DeepCopy. " +
		"Original is %s but expected %s"
	const ProjectIDErrorMessage = "Original ProjectID was changed by changing the DeepCopy. " +
		"Original is %q but expected %q"

	TestCaseID := "123456G"
	TestCaseVersionNr := 1
	ProjectID := "d8ed5f42"
	SUTVersion := "Version 1.02"
	Environment := "Win 10"

	ExecutionDate := time.Date(2000, 12, 24, 12, 0, 0, 0, time.UTC)
	OverAllResult := NotAssessed
	OverAllComment := "All OK"

	a := CaseExecutionRecord{
		TestCaseID:        TestCaseID,
		TestCaseVersionNr: TestCaseVersionNr,
		ProjectID:         ProjectID,
		SUTVersion:        SUTVersion,
		Environment:       Environment,

		ExecutionDate: ExecutionDate,
		StepRecords:   []StepExecutionRecord{stepRec1.DeepCopy(), stepRec2.DeepCopy(), stepRec3.DeepCopy()},
		Result:        OverAllResult,
		Comment:       OverAllComment,
	}
	b := a.DeepCopy()

	if !a.Equals(b) {
		t.Errorf("DeepCopy is not equal to original. \n Original: %+v \n DeepCopy: %+v", a, b)
	}

	b.TestCaseID = "48d584s"
	b.TestCaseVersionNr = 5
	b.ProjectID = "84sd8f"
	b.SUTVersion = "Version 0.8"
	b.Environment = "Linux"
	b.ExecutionDate = time.Now()
	b.StepRecords[0].NeededTime = time.Duration(78 * time.Minute)
	b.StepRecords[2].ObservedBehavior = "Hello"
	b.StepRecords[0].Result = Pass
	b.StepRecords[1].Comment = "Hello!"
	b.Result = Pass
	b.Comment = "Alarm! Major errors!"

	assertEqual(a.TestCaseID, TestCaseID, IDErrorMessage, t)
	assertEqual(a.TestCaseVersionNr, TestCaseVersionNr, VersionErrorMessage, t)
	assertEqual(a.ProjectID, ProjectID, ProjectIDErrorMessage, t)
	assertEqual(a.SUTVersion, SUTVersion, SUTErrorMessage, t)
	assertEqual(a.Environment, Environment, EnvironmentErrorMessage, t)
	assertEqual(a.ExecutionDate, ExecutionDate, DateErrorMessage, t)
	assertEqual(a.StepRecords[0].NeededTime, stepRec1.NeededTime, NeededTimesErrorMessage, t)
	assertEqual(a.StepRecords[2].ObservedBehavior, stepRec3.ObservedBehavior, ObsBehaviorsErrorMessage, t)
	assertEqual(a.StepRecords[0].Result, stepRec1.Result, ResultsErrorMessage, t)
	assertEqual(a.StepRecords[1].Comment, stepRec2.Comment, CommentsErrorMessage, t)
	assertEqual(a.Result, OverAllResult, OAResult, t)
	assertEqual(a.Comment, OverAllComment, OAComment, t)
}

func TestCaseExecutionRecord_Equals(t *testing.T) {
	a := defaultCaseRecord.DeepCopy()
	b := a

	evaluateEqualityTest(a, b, true, t, "A and B are exactly the same.")
	evaluateEqualityTest(a, nil, false, t, "B was nil, but A wasn't.")
	var empty *CaseExecutionRecord
	evaluateEqualityTest(a, empty, false, t, "B was an pointer with value nil, but A wasn't.")
	evaluateEqualityTest(a, "I'm not the expected Type", false, t, "B was of wrong type.")
	b = a.DeepCopy()
	b.TestCaseID = "G654321"
	evaluateEqualityTest(a, b, false, t, "TestCaseID of B was different from A.")
	b = a.DeepCopy()
	b.TestCaseVersionNr = -5
	evaluateEqualityTest(a, b, false, t, "TestCaseVersionNr of B was different from A.")
	b = a.DeepCopy()
	b.SUTVersion = "Version 0.84"
	evaluateEqualityTest(a, b, false, t, "SUTVersion of B was different from A.")
	b = a.DeepCopy()
	b.Environment = "Unix"
	evaluateEqualityTest(a, b, false, t, "Environment of B was different from A.")
	b = a.DeepCopy()
	b.ExecutionDate = time.Now()
	evaluateEqualityTest(a, b, false, t, "ExecutionDate of B was different from A.")
	b = a.DeepCopy()
	b.StepRecords[0].NeededTime = 0
	evaluateEqualityTest(a, b, false, t, "NeededTimes[0] of B was different from A.")
	b = a.DeepCopy()
	b.StepRecords = append(b.StepRecords, stepRec5)
	evaluateEqualityTest(a, b, false, t, "StepRecords of B is one entry longer than StepRecords of A.")
	b = a.DeepCopy()
	b.StepRecords[4].NeededTime = 4 * time.Second
	evaluateEqualityTest(a, b, false, t, "NeededTimes[4] of B was different from A.")
	b = a.DeepCopy()
	b.StepRecords[1].ObservedBehavior = "Ring-Ding-Dong!!!"
	evaluateEqualityTest(a, b, false, t, "ObservedBehaviors[1] of B was different from A.")
	b = a.DeepCopy()
	b.StepRecords[2].ObservedBehavior = ""
	evaluateEqualityTest(a, b, false, t, "ObservedBehaviors[2] of B was different from A.")
	b = a.DeepCopy()
	b.StepRecords[1].Result = 0
	evaluateEqualityTest(a, b, false, t, "Results[1] of B was different from A.")
	b = a.DeepCopy()
	b.StepRecords[0].Result = 1
	evaluateEqualityTest(a, b, false, t, "Results[0] of B was different from A.")
	b = a.DeepCopy()
	b.StepRecords[2].Comment = "World?"
	evaluateEqualityTest(a, b, false, t, "Comments[2] of B was different from A.")
	b = a.DeepCopy()
	b.StepRecords[1].Comment = ""
	evaluateEqualityTest(a, b, false, t, "Comments[1] of B was different from A.")
	b = a.DeepCopy()
	b.Result = Pass
	evaluateEqualityTest(a, b, false, t, "Result of B was different from A.")
	b = a.DeepCopy()
	b.Comment = "No, not ok. We have to do it again"
	evaluateEqualityTest(a, b, false, t, "Comment of B was different from A.")
	b = a.DeepCopy()
	b.ProjectID = "47sdf85"
	evaluateEqualityTest(a, b, false, t, "ProjectID of B was different from A.")
}

func evaluateEqualityTest(a CaseExecutionRecord, b interface{}, expected bool, t *testing.T, ErrorSuffix string) {
	const ErrorMessage = "Equals does not work symmetric! 'a.Equals(b)' returned %t, but 'b.Equals(a)' returned %t."

	resultA := a.Equals(b)

	if v, ok := b.(CaseExecutionRecord); ok {
		resultB := v.Equals(a)

		if resultA != resultB {
			t.Errorf(ErrorMessage, resultA, resultB)
		}
	}
	if resultA != expected {
		t.Errorf("Equals-Function returend %t, but expected %t. "+ErrorSuffix, resultA, expected)
	}
}
