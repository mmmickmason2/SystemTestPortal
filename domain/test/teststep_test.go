/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"testing"
)

func TestNewTestStep(t *testing.T) {
	action := "Test the test step"
	actual := NotAssessed
	expect := "The test step test tests the functionality correctly"
	assess := ""

	s := NewTestStep(action, expect)

	if s.Action != action {
		t.Errorf("Test step action is %s and not %s", s.Action, action)
	}
	if s.ActualResult != actual {
		t.Errorf("Test step actual outcome is %v and not %v", s.ActualResult, actual)
	}
	if s.ExpectedResult != expect {
		t.Errorf("Test step expected outcome is %s and not %s", s.ExpectedResult, expect)
	}
	if s.Action != action {
		t.Errorf("Test step assessment is %s and not %s", s.Assessment, assess)
	}
}
