/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package group

import (
	"testing"

	"gitlab.com/stp-team/systemtestportal/domain/user"
	"gitlab.com/stp-team/systemtestportal/domain/visibility"
)

func TestNewGroup(t *testing.T) {
	name := "BlankGroup   "
	nameWant := "BlankGroup"
	desc := "   Blank Description"
	descWant := "Blank Description"
	vis := visibility.Public

	g := NewGroup(name, desc, vis, make(map[string]user.User))
	if g.Name != nameWant {
		t.Errorf("Groupname = %s, want %s", g.Name, nameWant)
	}
	if g.Description != descWant {
		t.Errorf("Group description = %s, want %s", g.Description, descWant)
	}
	if g.Visibility != vis {
		t.Errorf("Groupvisibility = %d, want %d", g.Visibility, vis)
	}
}
