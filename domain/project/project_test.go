/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

import (
	"testing"

	"gitlab.com/stp-team/systemtestportal/domain/visibility"
)

// TestNewProject tests the NewProject function
func TestNewProject(t *testing.T) {
	name := "TestProject"
	desc := "TestDescription"
	vis := visibility.Public

	p := NewProject(name, desc, vis)
	if p.Name != name {
		t.Errorf("Projectname = %s, want %s", p.Name, name)
	}
	if p.Description != desc {
		t.Errorf("Projectdescription = %s, want %s", p.Description, desc)
	}
	if p.Visibility != vis {
		t.Errorf("Projectvisibility = %d, want %d", p.Visibility, vis)
	}

	name = "BlankProject   "
	nameWant := "BlankProject"
	desc = "   BlankDescription"
	descWant := "BlankDescription"
	vis = visibility.Private

	p = NewProject(name, desc, vis)
	if p.Name != nameWant {
		t.Errorf("Projectname = %s, want %s", p.Name, nameWant)
	}
	if p.Description != descWant {
		t.Errorf("Projectdescription = %s, want %s", p.Description, descWant)
	}
	if p.Visibility != vis {
		t.Errorf("Projectvisibility = %d, want %d", p.Visibility, vis)
	}
}
