/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

import (
	"strings"
	"time"

	"gitlab.com/stp-team/systemtestportal/domain/visibility"
	"gitlab.com/stp-team/systemtestportal/web/slugs"
)

// The Project struct contains information used to describe a project in the system
type Project struct {
	ID           string
	Name         string
	Description  string
	Visibility   visibility.Visibility
	CreationDate time.Time
	Owner        string
	SUTVersions  []string
}

// ItemName returns a projects name
func (p Project) ItemName() string {
	return p.Name
}

// ItemID returns a projects id
func (p Project) ItemID() string {
	return p.ID
}

// ItemVisibility returns a projects visibility
func (p Project) ItemVisibility() visibility.Visibility {
	return p.Visibility
}

// NewProject creates a new project
func NewProject(name, description string, visibility visibility.Visibility) Project {
	name = strings.TrimSpace(name)
	description = strings.TrimSpace(description)
	creationDate := time.Now().Round(time.Second)

	p := Project{
		Name:         name,
		Description:  description,
		Visibility:   visibility,
		CreationDate: creationDate,
		SUTVersions:  []string{"default version 1", "default version 2"},
	}

	p.ID = slugs.Slugify(p.Name)

	return p
}
