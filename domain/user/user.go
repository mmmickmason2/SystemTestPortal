/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package user

import (
	"strings"
	"time"

	"gitlab.com/stp-team/systemtestportal/web/slugs"
)

// The User struct contains information to represent a user in the system
type User struct {
	ID               string
	DisplayName      string
	AccountName      string
	EMail            string
	Password         string
	RegistrationDate time.Time
}

// New creates a new user
func New(display, account, email, password string) User {
	display = strings.TrimSpace(display)
	account = strings.TrimSpace(account)
	email = strings.TrimSpace(email)
	u := User{DisplayName: display, AccountName: account, EMail: email, Password: password}
	u.ID = slugs.Slugify(u.AccountName)
	return u
}
