/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package user

import (
	"testing"
)

func TestNewUser(t *testing.T) {
	displayName := "Erik"
	accountName := "ErikOlafson"
	email := "Erik@olaf.com"
	password := "very-secure Sequence!"
	expdAccountName := "ErikOlafson"

	u := New(displayName, accountName, email, password)

	if u.DisplayName != displayName {
		t.Error("Displayname is incorrect", u.DisplayName, displayName)
	}
	if u.AccountName != expdAccountName {
		t.Error("Accountname is incorrect", u.AccountName, expdAccountName)
	}
	if u.EMail != email {
		t.Error("Mailadress is incorrect", u.EMail, email)
	}
	if u.Password != password {
		t.Error("Password is incorrect", u.Password, password)
	}

	displayName = "E r i k"
	accountName = "Erik-Olafson"
	expdAccountName = "Erik-Olafson"
	password = "Specialcharacter ©#µ"

	u = New(displayName, accountName, email, password)

	if u.DisplayName != displayName {
		t.Error("Displayname is incorrect", u.DisplayName, displayName)
	}
	if u.AccountName != expdAccountName {
		t.Error("Accountname is incorrect", u.AccountName, expdAccountName)
	}
	if u.Password != password {
		t.Error("Password is incorrect", u.Password, password)
	}
}
