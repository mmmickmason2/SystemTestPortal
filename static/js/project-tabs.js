/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

$.getScript("/static/js/util/common.js");

/* attach a handler to the tab buttons */
//$("#tabButtonDashboard").click(function(event) {requestTab(event,"dashboard")});
//$("#tabButtonActivity").click(function(event) {requestTab(event,"activity")});
$("#tabButtonTestCases").click(function (event) { requestTab(event, "testcases") });
$("#menuButtonTestCases").click(function (event) { requestTab(event, "testcases") });
$("#tabButtonTestSequences").click(function (event) { requestTab(event, "testsequences") });
$("#menuButtonTestSequences").click(function (event) { requestTab(event, "testsequences") });
$("#tabButtonProtocols").click(function(event) {requestTab(event,"protocols")});
//$("#tabButtonMembers").click(function(event) {requestTab(event,"members")});
//$("#tabButtonSettings").click(function(event) {requestTab(event,"settings")});

function requestTab(event, tab) {
    /* stop form from submitting normally */
    event.preventDefault();

    var url = urlify(window.location.pathname);
    var requestURL = url.takeFirstSegments(3).appendSegment(tab).toString() + "/";
    var posting = $.get(requestURL + "?fragment=true");

    /* Alerts the results */
    posting.done(function (response) {
        if (url.segments[3] !== tab || url.segments[4] !== "") {
            history.pushState('data', '', requestURL);
        }
        $('#tabarea').empty().append(response);
        updateTabMenu(tab);
    }).fail(function (response) {
        $("#modalPlaceholder").empty().append(response.responseText);
        $('#errorModal').modal('show');
    });

}

function updateTabMenu(tab) {
    $(".tab-collapse-menu").children().removeClass("active");
    if(tab === "testcases") {
        $("#menuButtonTestCases").addClass("active");
    } else if (tab === "testsequences") {
        $("#menuButtonTestSequences").addClass("active");
    } else if (tab === "protocols") {
        $("#menuButtonProtocols").addClass("active");
    }
}

$(document).ready(function () {
    var url = window.location.href;
    if (url.match(/testcases\//)) {
        $("#tabButtonTestCases").addClass("active");
        $("#menuButtonTestCases").addClass("active");
    } else if (url.match(/testsequences\//)) {
        $("#tabButtonTestSequences").addClass("active");
        $("#menuButtonTestSequences").addClass("active");
    } else if (url.match(/protocols\//)) {
        $("#tabButtonProtocols").addClass("active");
        $("#menuButtonProtocols").addClass("active");
    }

    var timeString = $(".timeago").text();
    timeString = timeString.replace(" ", 'T');
    timeString = timeString.replace(" ", '');
    timeString = timeString.replace(" CET", '');
    $(".timeago").text(timeString);
    $(".timeago").attr("datetime", timeString);
    jQuery("time.timeago").timeago();

});