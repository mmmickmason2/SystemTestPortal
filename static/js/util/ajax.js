/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Ajax requests */

function ajaxRequestFragment (event, target, params, requestType) {
    ajaxRequestFragmentWithHistory (event, target, params, requestType, target);
}

function ajaxRequestFragmentWithHistory (event, target, params, requestType, historyText) {
    event.preventDefault();
    target = updateQueryStringParameter(target, "fragment", true);

    var posting = $.ajax({
        url: target,
        type: requestType,
        data: params
    });
    posting.done(function(response) {
        history.pushState('data', '', historyText);
        $('#tabarea').empty().append(response);
        loadTooltips();
        return true;
    }).fail(function (response) {
        $( "#modalPlaceholder" ).empty().append(response.responseText);
        $('#errorModal').modal('show');
    });
}

function updateQueryStringParameter(uri, key, value) {
    var regex = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
    if (uri.match(regex)) {
        return uri.replace(regex, '$1' + key + "=" + value + '$2');
    } else {
        var hash =  '';
        var index = uri.lastIndexOf('#');
        if( index !== -1 ){
            hash = uri.slice(index, uri.length);
            uri = uri.slice(0, index);
        }
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        return uri + separator + key + "=" + value + hash;
    }
}