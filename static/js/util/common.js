/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

function getProjectURL() {
    return urlify(window.location.pathname).takeFirstSegments(3).toString();
}

function urlify(string) {
    return new URL(string.split("/"));
}

function URL(seg) {
    this.segments = seg;
    this.toString = function () {
        return this.segments.join("/")
    };
    this.removeLastSegments = function(n) {
        var copy = new URL(this.segments);
        copy.segments = copy.segments.slice(0, this.segments.length - n);
        return copy;
    };
    this.takeFirstSegments = function(n) {
        var copy = new URL(this.segments);
        copy.segments = copy.segments.slice(0, n);
        return copy;
    };
    this.appendSegment = function(s) {
        var copy = new URL(this.segments);
        copy.segments.push(s);
        return copy;
    };
}