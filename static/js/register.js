/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

$( document ).ready( function () {
    $( "#newAccountForm" ).validate( {
        rules: {
            inputAccountName: {
                required: true,
                minlength: 2
            },
            inputDisplayName: {
                required: true,
                minlength: 2
            },
            inputEmail: {
                required: true,
                email: true
            },
            inputPassword: {
                required: true,
                minlength: 8
            },
            inputPasswordRepeat: {
                equalTo: "#inputPassword1"
            }
        },
        messages: {
            inputAccountName: {
                required: "Please enter a username.",
                minlength: "Your username must consist of at least 2 characters."
            },
            inputDisplayName: {
                required: "Please enter a name.",
                minlength: "Your display name must consist of at least 2 characters."
            },
            inputEmail: "Please enter a valid email.",
            inputPassword1: {
                required: "Please provide a password.",
                minlength: "Your password must be at least 8 characters long."
            },
            inputPassword1Repeat: {
                required: "Please repeat your password.",
                minlength: "Your password must be at least 8 characters long.",
                equalTo: "Please enter the same password as above."
            }
        },
        errorElement: "div",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            error.addClass( "form-control-feedback" );

            // Add `has-feedback` class to the parent div.form-group
            // in order to add icons to inputs
            element.parents( ".col-sm-5" ).addClass( "has-feedback" );

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }

            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if ( !element.next( "span" )[ 0 ] ) {
                $( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
            }
        },
        success: function ( label, element ) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            $( element ).parents( ".col-sm-10" ).addClass( " text-success" ).removeClass( "text-danger" );
            $( element ).addClass( "form-control-success" ).removeClass( "text-danger" );

        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).parents( ".col-sm-10" ).addClass( " text-danger" ).removeClass( "text-success" );
            $( element ).addClass( "form-control-danger" ).removeClass( "text-danger" );
        },
        unhighlight: function ( element, errorClass, validClass ) {
            $( element ).parents( ".col-sm-10" ).addClass( "text-success" ).removeClass( "text-error" );
            $( element ).addClass( "form-control-success" ).removeClass( "text-danger" );
        },
        submitHandler: function(form, event) {
            /* stop form from submitting normally */
            event.preventDefault();

            /* set the action attribute */
            var url = "/register";

            /* Send the data using post with element ids*/

            var newAccountForm = $('#newAccountForm');
            var posting = $.post( url, {
                inputAccountName: newAccountForm.find('#inputAccountName').val(),
                inputDisplayName: newAccountForm.find('#inputDisplayName').val(),
                inputEmail: newAccountForm.find('#inputEmail').val(),
                inputPassword: newAccountForm.find('#inputPassword1').val()
            });

            /* Alerts the results */
            posting.done(function(request, textStatus, data) {
                window.location.href = "/" + data.getResponseHeader('newAccount') + "/";
            }).fail(function (request, textStatus, errorThrown) {
                $( "#modalPlaceholder" ).empty().append(request.responseText);
                $('#errorModal').modal('show');
            });
        }

    } );
} );