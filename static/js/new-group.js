/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

/* attach a handler to the new testcase button */

// Caret Toggle
$('#nameSelection').on('shown.bs.collapse', function(){
    $("#select-name-collapse-toggler").removeClass("fa-caret-down").addClass("fa-caret-up");
}).on('hidden.bs.collapse', function(){
    $("#select-name-collapse-toggler").removeClass("fa-caret-up").addClass("fa-caret-down");
});

// Requests the server to create a new group
function createGroup(form, event) {
    /* stop form from submitting normally */
    event.preventDefault();

    /* get the action attribute from the <form action=""> element */
    var url = $(form).attr('action');

    /* Send the data using post with element ids*/
    var posting = $.post(url, {
        inputGroupName: $('#inputGroupName').val(),
        inputGroupDesc: $('#inputGroupDesc').val(),
        inputGroupVisibility: document.querySelector('input[name="optionsGroupVisibility"]:checked').value
    });

    /* Alerts the results */
    posting.done(function () {
        window.location.href = "/explore/groups";
    }).fail(function (request) {
        $("#modalPlaceholder").empty().append(request.responseText);
        $('#errorModal').modal('show');
    });
}

//Validation
$( document ).ready( function () {
    $("#newGroupForm").validate( {
        rules: {
            inputGroupName: {
                required: true,
                minlength: 2
            }
        },
        messages: {
            inputGroupName: {
                required: "<span>&times;</span> Please enter a group name.",
                minlength: "<span>&times;</span> Your group name must consist of at least 2 characters."
            }
        },
        errorElement: "div",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            error.addClass("invalid-feedback");
            error.insertAfter(element);
        },
        success: function (label, element) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            $( element ).parents(".form-group").addClass("text-success").removeClass("text-danger");
            $( element ).addClass("is-invalid").removeClass("is-invalid");

        },
        highlight: function (element) {
            $( element ).parents(".form-group").addClass("text-danger").removeClass("text-success");
            $( element ).addClass("is-invalid").removeClass("is-invalid");
        },
        unhighlight: function (element) {
            $( element ).parents(".form-group").addClass("text-success").removeClass("text-danger");
            $( element ).removeClass("is-valid").removeClass("is-invalid");
        },
        submitHandler: createGroup

    } );
} );