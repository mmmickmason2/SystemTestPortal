/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

/* attach a handler to the create button */
$("#newProjectForm").submit(function(event) {

    /* stop form from submitting normally */
    event.preventDefault();

    /* get the action attribute from the <form action=""> element */
    var $form = $( this ),
        url = $form.attr( 'action' );

    /* Send the data using post with element ids*/
    var posting = $.post( url, {
        inputProjectName: $('#inputProjectName').val(),
        inputProjectDesc: $('#inputProjectDesc').val(),
        inputProjectLogo: $('#inputProjectLogo').val(),
        optionsProjectVisibility: document.querySelector('input[name="optionsProjectVisibility"]:checked').value
    });

    /* Alerts the results */
    posting.done(function(request, textStatus, data) {
        window.location.href = "../" + data.getResponseHeader('newName') + "/";
    }).fail(function (request, textStatus, errorThrown) {
        $( "#modalPlaceholder" ).empty().append(request.responseText);
        $('#errorModal').modal('show');
    });
});