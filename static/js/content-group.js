/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

$.getScript("/static/js/util/common.js");

/* attach a handler to the new testsequence button */
$("#tabButtonActivity").click(function(event) {requestTab(event,"activity")});
//$("#tabButtonProjects").click(function(event) {requestTab(event,"projects")});
//$("#tabButtonMembers").click(function(event) {requestTab(event,"members")});
//$("#tabButtonSettings").click(function(event) {requestTab(event,"settings")});

function requestTab(event, tab) {
    /* stop form from submitting normally */
    event.preventDefault();

    var url = urlify(window.location.pathname);
    var requestURL = url.takeFirstSegments(2).appendSegment(tab).toString() + "?fragment=true";
    var posting = $.get(requestURL);

    /* Alerts the results */
    posting.done(function (response) {
        if (url.segments[2] !== tab || url.segments[3] !== "") {
            history.pushState('data', '', requestURL);
        }
        $('#tabarea').empty().append(response);
    }).fail(function (response) {
        $("#modalPlaceholder").empty().append(response.responseText);
        $('#errorModal').modal('show');
    });
}

$(document).ready(function () {
    var url = window.location.pathname;
    var urlSeg = url.split("/");
    // noinspection FallThroughInSwitchStatementJS
    switch (urlSeg[2]) {
        case "activity":
            $("#tabButtonActivity").addClass("active");
        case "projects":
            // Not available disabled
        case "members":
            // Not available disabled
    }
});