/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

$.getScript("/static/js/util/ajax.js");

/* Button Assignment */
$("#buttonExecuteFirstTestStep") .click(executeFirstTestStep);
$("#buttonExecuteNextTestStep")  .click(executeNextTestStep);
$("#buttonSummaryFinish")        .click(finishSummary);
$("#buttonAbort")                .click(backToTestObject);


/* steps back to the test object  */
function backToTestObject(event) {
    var requestURL = urlify(window.location.pathname).takeFirstSegments(5).toString();
    ajaxRequestFragment(event, requestURL, "", "GET");
}

/* saves inputs and requests for first step execution page */
function executeFirstTestStep(event) {
    ajaxRequestFragment(event, window.location.pathname, getExecutionStartPageData(), "POST");
}

/* saves inputs and requests for next step execution page */
function executeNextTestStep(event) {
    ajaxRequestFragment(event, window.location.pathname, getExecutionStepPageData(), "POST");
}
/* saves inputs and redirect */
function finishSummary(event) {
    var path = urlify(window.location.pathname).removeLastSegments(1).toString();
    console.log(path);
    var data = getSummaryPageData();
    if (data.case === "0"){
        ajaxRequestFragmentWithHistory(event, window.location.pathname, data, "POST",path);
    }else{
        ajaxRequestFragment(event, window.location.pathname, data, "POST");
    }
}

/* helper */

// getExecutionStartPageData returns an array with the information given on the execution start page
function getExecutionStartPageData() {
    return {
        fragment : true,
        step: 0,
        case: $('#inputTestCaseNumber').val(),
        inputEnvironment: $('#inputEnvironment').val(),
        inputSUTVersion: $('#inputTestCaseSUTVersion').find("option:selected").val()
    }
}

// getExecutionStartPageData returns an array with the information given on the execution step page
function getExecutionStepPageData() {
    return {
        fragment : true,
        step: $('#inputTestStepNumber').val(),
        case: $('#inputTestCaseNumber').val(),
        result: $("input:radio[name ='testResults']:checked").val(),
        notes: $('#inputTestStepNotes').val(),
        inputTestStepActualResult: $('#inputTestStepActualResult').val(),
        seconds: $('#timeSeconds').val(),
        minutes: $('#timeMinutes').val()
    }
}
// getSummaryPageData returns an array with the information given on the summary page
function getSummaryPageData() {
    return {
        fragment : true,
        step: $('#inputTestStepNumber').val(),
        case: $('#inputTestCaseNumber').val(),
        notes: $('#inputTestComment').val(),
        result: $("input:radio[name ='testResults']:checked").val(),
        seconds: $('#timeSeconds').val(),
        minutes: $('#timeMinutes').val()
    }
}