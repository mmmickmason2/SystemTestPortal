All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

# [v0.6.0] - 24/11/2017

This is the first public release of the SystemTestPortal. Basic features such as the creation of projects, test cases and test sequences and the execution are implemented.
Additionally, there is a basic concept of users and permission management. Some features are restricted to logged-in users.

## Enhancements

#### Users
- Simple registration process
- Features limited to logged-in users (e.g. creating tests)

#### Tests
- Add creation of single tests and compound test sequences
- Add editing of tests and test sequences
- Add history for tests. Older versions of a test can be shown but not edited
- Add execution for tests and test sequences
- Add protocols to show results of executed tests and test sequences
