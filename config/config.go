/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package config

import (
	"flag"
	"fmt"
	"go/build"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

// basePath contains the path where this application searches for resources.
var basePath = getWorkingDir()

// basePathFlag contains the path passed by the flag for the base path.
var basePathFlag = flag.String("basepath", "", "Defines the folder where STP searches its resources")

// fallBackPath is the path the system will try if the given base path didn't
var fallBackPath = filepath.Join(getGoPath(), "src", "gitlab.com", "stp-team", "systemtestportal")

// neededFolders determines which folders need to be contained in th base path.
var neededFolders = []string{"templates", "static"}

// version contains the current version of the system
var version = "development"

func init() {
	err := checkBasePath(basePath)
	if err != nil {
		if basePath == "" {
			log.Printf("Falling back to development default base path '%s' since current working "+
				"directory couldn't be determined.\n", fallBackPath)
		} else {
			log.Printf("Falling back to development default base path '%s' since current working "+
				"directory ('%s') can't be used as base path because: %s\n", fallBackPath, basePath, err)
		}
		basePath = fallBackPath
	}
	log.Printf("Default base path is '%s'.\n", basePath)
	// Alias for base path flag
	flag.StringVar(basePathFlag, "p", "", "Defines the folder where STP searches its resources")
}

// Load loads the config.
func Load() {
	initBasePath(*basePathFlag)
}

// BasePath returns the configured base path.
func BasePath() string {
	return basePath
}

// Version returns the current version of the system.
func Version() string {
	return version
}

// getGoPath gets the GOPATH environment variable. If it isn't set
// it gets the default value for it (<userhome>/go).
func getGoPath() string {
	gopath := os.Getenv("GOPATH")
	if gopath == "" {
		return build.Default.GOPATH
	}
	return gopath
}

// initBasePath initializes the base path variable with given value if its not empty.
func initBasePath(bpath string) {
	if bpath != "" {
		basePath = bpath
	}
	err := checkBasePath(basePath)
	if err != nil {
		printExplanationForError(err, basePath)
	}
	err = os.Chdir(basePath)
	if err != nil {
		log.Panicf("Unable to change working directory to basepath '%s': %s", basePath, err)
	}
	log.Printf("Using '%s' as base path.\n", basePath)
}

func printExplanationForError(err error, basepath string) {
	if _, ok := err.(requiredFolderError); ok {
		log.Printf("The given base path(%s) doesn't contain the required folders (%s).\n",
			basepath, neededFolders)
		log.Println("Please make sure to place the required folders you got when downloading stp" +
			" into the base path or change " +
			"it to a different folder using the --basepath=<basepath> arguement.")
	} else {
		log.Printf("Unable to access given basepath '%s': %s\n", basepath, err)
	}
}

// getWorkingDir gets the current working dir or an empty string
// if it couldn't be retrieved.
func getWorkingDir() string {
	dir, err := os.Getwd()
	if err != nil {
		return ""
	}
	return dir
}

// checkBasePath checks whether the given base path can be used
// if not an error is returned.
func checkBasePath(path string) error {
	fi, err := os.Stat(path)
	if os.IsNotExist(err) {
		// Doesn't exist
		return err
	}
	if !fi.IsDir() {
		// Not a folder
		return fmt.Errorf("basepath '%s' isn't a folder", path)
	}
	files, err := ioutil.ReadDir(path)
	if err != nil {
		// Error reading dir
		return err
	}
	if !containRequiredFolders(files) {
		return requiredFolderError{
			files,
			fmt.Errorf("basepath doesn't contain required folders %s", neededFolders),
		}
	}
	return nil
}

type requiredFolderError struct {
	RequiredFolders []os.FileInfo
	error
}

// containRequiredFolders checks whether the required folders are contained in
// given files
func containRequiredFolders(files []os.FileInfo) bool {
	for _, name := range neededFolders {
		found := false
		for _, file := range files {
			if file.IsDir() && (file.Name() == name) {
				found = true
			}
		}
		if !found {
			return false
		}
	}
	return true
}
