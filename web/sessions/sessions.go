/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package sessions

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"encoding/gob"

	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"gitlab.com/stp-team/systemtestportal/domain/test"
	"gitlab.com/stp-team/systemtestportal/domain/user"
	userServer "gitlab.com/stp-team/systemtestportal/store/user"
)

const userSessionName = "User-Session"
const recSessionName = "Record-Session"
const idKey = "ID"
const caseRecKey = "Case Record"
const seqRecKey = "Sequence Record"
const cookieAge = 1209600 //2 weeks

type userDAO interface {
	Get(string) (*user.User, bool)
}

func init() {
	gob.Register(&test.CaseExecutionRecord{})
	gob.Register(&test.SequenceExecutionRecord{})
}

//Store provides functions to store and get data to the session of a request
//Don't create your own Store! Better get the global Store via the corresponding function
type Store struct {
	store    sessions.Store
	users    userDAO
	bootTime time.Time
}

//store is the global Store
var store Store

//GetSessionStore returns a pointer to the session-store
func GetSessionStore() *Store {
	return &store
}

//InitSessionManagement initialize the global Store.
//Default parameters are nil, anything else is just for testing
func InitSessionManagement(s sessions.Store, us userDAO) {
	//bootTime
	store = Store{nil, nil, time.Now()}
	//store
	if s == nil {
		store.store = sessions.NewCookieStore(securecookie.GenerateRandomKey(64))
	} else {
		store.store = s
	}
	//userDAO
	if us == nil {
		store.users = userServer.GetStore()
	} else {
		store.users = us
	}
}

func (s *Store) getUserSessionName() string {
	return fmt.Sprintf(
		"%s%d%d%d%d%d",
		userSessionName,
		s.bootTime.Year(),
		s.bootTime.YearDay(),
		s.bootTime.Hour(),
		s.bootTime.Minute(),
		s.bootTime.Second(),
	)
}

func (s *Store) getRecSessionName() string {
	return fmt.Sprintf(
		"%s%d%d%d%d%d",
		recSessionName,
		s.bootTime.Year(),
		s.bootTime.YearDay(),
		s.bootTime.Hour(),
		s.bootTime.Minute(),
		s.bootTime.Second(),
	)
}

//GetCurrent returns the current logged-in user to the given request.
//If the current user is not logged-in, the function will return nil, nil
//If an error occurs nil and the error will be returned
func (s *Store) GetCurrent(r *http.Request) (*user.User, error) {
	session, err := s.store.Get(r, s.getUserSessionName())
	if err != nil {
		return nil, err
	}
	id, ok := session.Values[idKey]
	if !ok {
		return nil, nil
	}
	currentUser, ok := s.users.Get(id.(string))
	if !ok {
		return nil, errors.New("No User with this ID found")
	}
	return currentUser, nil
}

//StartFor connects the session of the request with an user.
//After this call, you can get the user via the GetCurrent-function
func (s *Store) StartFor(w http.ResponseWriter, r *http.Request, user *user.User) error {
	session, err := s.store.Get(r, s.getUserSessionName())
	if err != nil {
		return err
	}
	session.Options = &sessions.Options{MaxAge: cookieAge}
	session.Values[idKey] = user.ID
	err = session.Save(r, w)
	return err
}

//EndFor removes the user from the session
//User parameter is not used by now, just for future
func (s *Store) EndFor(w http.ResponseWriter, r *http.Request, u *user.User) error {
	session, err := s.store.Get(r, s.getUserSessionName())
	if err != nil {
		return err
	}
	session.Options = &sessions.Options{MaxAge: -1}
	err = session.Save(r, w)
	return err
}

//SetCurrentCaseRecord saves the given record to the session.
//After this call, you can get the current case record via the GetCurrentCaseRecord-function
func (s *Store) SetCurrentCaseRecord(w http.ResponseWriter, r *http.Request, record *test.CaseExecutionRecord) error {
	session, err := s.store.Get(r, s.getRecSessionName())
	if err != nil {
		return err
	}
	session.Options = &sessions.Options{MaxAge: cookieAge}
	session.Values[caseRecKey] = record
	err = session.Save(r, w)
	return err
}

//RemoveCurrentCaseRecord removes the current case record from the session.
func (s *Store) RemoveCurrentCaseRecord(w http.ResponseWriter, r *http.Request) error {
	session, err := s.store.Get(r, s.getRecSessionName())
	if err != nil {
		return err
	}
	session.Options = &sessions.Options{MaxAge: cookieAge}
	delete(session.Values, caseRecKey)
	err = session.Save(r, w)
	return err
}

//GetCurrentCaseRecord returns the record to the currently running case execution to the given request.
//If there is no case execution running, the function will return nil, nil
//If an error occurs nil and the error will be returned
func (s *Store) GetCurrentCaseRecord(r *http.Request) (*test.CaseExecutionRecord, error) {
	session, err := s.store.Get(r, s.getRecSessionName())
	if err != nil {
		return nil, err
	}
	rec, ok := session.Values[caseRecKey]
	if !ok {
		return nil, nil
	}
	result, ok := rec.(*test.CaseExecutionRecord)
	if !ok {
		return nil, errors.New("Cast-Error. Saved data is not expected type")
	}
	return result, nil
}

//SetCurrentSequenceRecord saves the given record to the session.
//After this call, you can get the current sequence record via the GetCurrentSequenceRecord-function
func (s *Store) SetCurrentSequenceRecord(w http.ResponseWriter, r *http.Request, record *test.SequenceExecutionRecord) error {
	session, err := s.store.Get(r, s.getRecSessionName())
	if err != nil {
		return err
	}
	session.Options = &sessions.Options{MaxAge: cookieAge}
	session.Values[seqRecKey] = record
	err = session.Save(r, w)
	return err
}

//RemoveCurrentSequenceRecord removes the current sequence record from the session.
func (s *Store) RemoveCurrentSequenceRecord(w http.ResponseWriter, r *http.Request) error {
	session, err := s.store.Get(r, s.getRecSessionName())
	if err != nil {
		return err
	}
	session.Options = &sessions.Options{MaxAge: cookieAge}
	delete(session.Values, seqRecKey)
	err = session.Save(r, w)
	return err
}

//GetCurrentSequenceRecord returns the record to the currently running sequence execution to the given request.
//If there is no sequence execution running, the function will return nil, nil
//If an error occurs nil and the error will be returned
func (s *Store) GetCurrentSequenceRecord(r *http.Request) (*test.SequenceExecutionRecord, error) {
	session, err := s.store.Get(r, s.getRecSessionName())
	if err != nil {
		return nil, err
	}
	rec, ok := session.Values[seqRecKey]
	if !ok {
		return nil, nil
	}
	result, ok := rec.(*test.SequenceExecutionRecord)
	if !ok {
		return nil, errors.New("Cast-Error. Saved data is not expected type")
	}
	return result, nil
}

//TestStore is a Test-Stub of Session-Store
type TestStore struct {
	session *sessions.Session
}

func (s *TestStore) Get(r *http.Request, name string) (*sessions.Session, error) {
	if s.session == nil {
		return s.New(r, name)
	}
	return s.session, nil
}
func (s *TestStore) New(r *http.Request, name string) (*sessions.Session, error) {
	return sessions.NewSession(s, name), nil
}
func (s *TestStore) Save(r *http.Request, w http.ResponseWriter,
	session *sessions.Session) error {
	if session.Options != nil && session.Options.MaxAge < 0 {
		s.session = nil
	} else {
		s.session = session
	}
	return nil
}

//testUserServer is a Test-Stub of UserServer
type testUserServer struct {
	savedUser user.User
}

func (tus *testUserServer) Get(id string) (*user.User, bool) {
	return &tus.savedUser, true
}
