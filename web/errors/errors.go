/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Package errors contains helpful types and structures simplifying
error handling.

Handler Errors

Handler errors consist of a status code a log message and a body.
The status code and body can be send to the client and the log message can
obviously be logged.

Errors for handlers can easily be constructed using chaining.
You can start the construction of an error with the:
		Construct(status int)
or
		ConstructWithContext(status int, logMsg string)
method.
The latter is just a shortcut for:
		Construct(status).WithLog(logMsg).WithStackTrace(0)
This constructs an error with given status code and a log
message that contains the logMsg and a little stack trace showing
where the error was constructed which allows to find the root of an error
more easily.

The parameter for the WithStackTrace method determines how far up the stack
the trace should be taken from. A 0 means the trace should be taken from the
caller of the WithStackTrace method.
If you create your own construction function
for commonly constructed errors you can set this to 1 to take the trace of the
code calling your construction function instead of taking the trace from your
construction function itself.

Similarly to the  WithStackTrace(skip int) there is the WithCause(cause error)
method which appends the message of given cause to the log of the error.

Both of these method require that the WithLog(logMsg string) has been called before
them or else they will panic.

To determine the content of the errors body the following methods can be used:
		WithBody(body string)
		WithBodyModal(title, msg string)
		WriteToBody(write func(io.Writer))

The first one simply writes the given string to the body.
The second parses given title and msg into a html-template containing an error modal.
The third allows to directly write to the body without interrupting the chaining of calls.
It accepts a function taking a writer to which in can write the content of the body.

To finish the construction and retrieve the created error simply use the
Finish() method which will return the constructed error.
*/
package errors

import (
	"log"
	"net/http"

	"bytes"
	"fmt"
	"runtime"

	"io"

	"html/template"

	"gitlab.com/stp-team/systemtestportal/web/templates"
)

// HandlerErrorConstructor is used to construct a HandlerError.
// It allows chaining methods that consequently define the
// errors attributes.
type HandlerErrorConstructor interface {
	// WithBodyModal adds a compiled error modal template
	// to the errors body using given title and message.
	WithBodyModal(title, msg string) HandlerErrorConstructor
	// WithBody adds the given string to the body of
	// the error.
	WithBody(body string) HandlerErrorConstructor
	// WriteToBody allows to directly writing to the body of the error without
	// interrupting chaining.
	WriteToBody(write func(io.Writer)) HandlerErrorConstructor
	// WithLog adds the given message to the log entry
	// of the error.
	WithLog(logMsg string) HandlerErrorConstructor
	// WithStackTrace adds the line and file from the call stack
	// to the errors log message.
	//
	// The argument skip is the number of stack frames
	// to ascend, with 0 identifying the caller of this method.
	//
	// Please call WithLog before calling this method or else it
	// will panic.
	//
	// The new message looks like this:
	//
	// <current message>
	//     Created at: line <caller line> in file '<caller file>'
	WithStackTrace(skip int) HandlerErrorConstructor
	// WithCause adds the error message of given cause
	// to this errors log.
	//
	// Please call WithLog before calling this method or else it
	// will panic.
	//
	// The new message looks like this:
	//
	// <current message>
	//     Caused by: <cause.Error()>
	WithCause(cause error) HandlerErrorConstructor
	// Copy copies this constructor. Calls to the copy have no influence
	// on the constructor it was copied from. This can be used to create
	// template for error which can be copied an further modified without changing
	// the original.
	Copy() HandlerErrorConstructor
	// Finish finishes the construction and returns the constructed error.
	Finish() HandlerError
}

// HandlerError is an error that occurs in handlers
// the defined function will be used to write the
// error to a ResponseWriter and to the log.
type HandlerError interface {
	// Error returns the error message that
	// should be logged once the error occurs.
	Error() string
	// StatusCode returns the http status code that should
	// be sent to the client.
	StatusCode() int
	// Body returns the body this error contains and
	// should be written to the client response.
	Body() string
}

// handlerError is a simple implementation of a
// HandlerError.
//
// It uses a buffer so writing and reading the body can
// be easily achieved.
// The string that can be logged also uses a buffer so
// it can be written dynamically.
type handlerError struct {
	*bytes.Buffer
	statusCode int
	log        *bytes.Buffer
}

// StatusCode returns the http status code that should
// be sent to the client.
func (hErr handlerError) StatusCode() int {
	return hErr.statusCode
}

// Error returns the error message that
// should be logged once the error occurs.
func (hErr handlerError) Error() string {
	return hErr.log.String()
}

// Body returns the body this error contains and
// should be written to the client response.
func (hErr handlerError) Body() string {
	return hErr.String()
}

// WithBodyModal adds a compiled error modal template
// to the errors body using given title and message.
func (hErr handlerError) WithBodyModal(title, msg string) HandlerErrorConstructor {
	err := getErrorTemplate().Execute(hErr, struct {
		Title   string
		Message string
	}{title, msg})
	if err != nil {
		log.Panicf("Unable to execute error modal template: %s", hErr)
	}
	return hErr
}

// getErrorTemplate returns the template for an error modal
// that can be parsed into the body of an error.
func getErrorTemplate() *template.Template {
	return templates.Load("modal-error").Get()
}

// WriteToBody allows to directly writing to the body of the error without
// interrupting chaining.
func (hErr handlerError) WriteToBody(write func(io.Writer)) HandlerErrorConstructor {
	write(hErr)
	return hErr
}

// WithBody adds the given string to the body of
// the error.
func (hErr handlerError) WithBody(body string) HandlerErrorConstructor {
	hErr.WriteString(body)
	return hErr
}

// WithLog adds the given message to the log entry
// of the error.
func (hErr handlerError) WithLog(logMsg string) HandlerErrorConstructor {
	hErr.log.WriteString(logMsg)
	return hErr
}

// WithStackTrace adds the line and file from the call stack
// to the errors log message.
//
// The argument skip is the number of stack frames
// to ascend, with 0 identifying the caller of this method.
//
// Please call WithLog before calling this method or else it
// will panic.
//
// The new message looks like this:
//
// <current message>
//     Created at: line <caller line> in file '<caller file>'
func (hErr handlerError) WithStackTrace(skip int) HandlerErrorConstructor {
	if hErr.log.Len() <= 0 {
		panic("WithStackTrace can't be called before calling WithLog!")
	}
	_, file, line, ok := runtime.Caller(skip + 1)
	if ok {
		hErr.log.WriteString(fmt.Sprintf("\n    Created at: %s:%d",
			file, line))
	}
	return hErr
}

// WithCause adds the error message of given cause
// to this errors log.
//
// Please call WithLog before calling this method or else it
// will panic.
//
// The new message looks like this:
//
// <current message>
//     Caused by: <cause.Error()>
func (hErr handlerError) WithCause(cause error) HandlerErrorConstructor {
	if hErr.log.Len() <= 0 {
		panic("WithCause can't be called before calling WithLog!")
	}
	if cause != nil {
		hErr.log.WriteString(fmt.Sprintf("\n    Caused by: %s", cause))
	}
	return hErr
}

// Copy copies this constructor. Calls to the copy have no influence
// on the constructor it was copied from. This can be used to create
// template for error which can be copied an further modified without changing
// the original.
func (hErr handlerError) Copy() HandlerErrorConstructor {
	return handlerError{
		bytes.NewBufferString(hErr.Body()),
		hErr.StatusCode(),
		bytes.NewBufferString(hErr.Error()),
	}
}

// Finish finishes the construction and returns the constructed error.
func (hErr handlerError) Finish() HandlerError {
	return hErr
}

// Construct lets you construct a new handler error using the
// constructor.
func Construct(status int) HandlerErrorConstructor {
	return handlerError{
		bytes.NewBufferString(""),
		status,
		bytes.NewBufferString(""),
	}
}

// ConstructWithStackTrace this method is a convenient shortcut
// for:
// Construct(status).WithLog(logMsg).WithStackTrace(0)
func ConstructWithStackTrace(status int, logMsg string) HandlerErrorConstructor {
	return Construct(status).WithLog(logMsg).WithStackTrace(2)
}

// Respond writes the given error to the given response writer and logs
// the incidence.
//
// The errors body will be written to the response writer.
// The errors status code will be written to the response writer.
//
// If the log message contained in the error isn't empty
// the following message will be written to the log:
//
// Handler error. Status: <Text representation of status code>
// <error.Error()>
func Respond(error HandlerError, w http.ResponseWriter) {
	Log(error)
	w.WriteHeader(error.StatusCode())
	fmt.Fprint(w, error.Body())
}

// Log logs the given error.
//
// If the log message contained in the error isn't empty
// the following message will be written to the log:
//
// Handler error. Status: <Text representation of status code>
// <error.Error()>
func Log(error HandlerError) {
	if len(error.Error()) > 0 {
		log.Printf("Handler error. Status: %s\n%s",
			http.StatusText(error.StatusCode()),
			error.Error(),
		)
	}
}

// BadRequest shows an error displaying, that a request has been made
// that shouldn't have happen or was caused by an client error (Status Code 400)
func BadRequest(w http.ResponseWriter) {
	Respond(
		Construct(http.StatusBadRequest).
			WithLog("Client send an invalid request.").
			WithStackTrace(2).
			Finish(),
		w,
	)
	w.Header().Set("showError", "bad request")
}

// WrongRequestMethod shows an error displaying, that a request has been made
// using an unsupported method (Status Code 405)
func WrongRequestMethod(w http.ResponseWriter, r *http.Request) {
	Respond(
		Construct(http.StatusMethodNotAllowed).
			WithLog(
				fmt.Sprintf("The method '%s' is not supported by the handler.",
					r.Method,
				),
			).
			WithLog(
				fmt.Sprintf("\nRequested URL was %s with header %s.",
					r.URL, r.Header,
				),
			).
			WithStackTrace(2).
			Finish(),
		w,
	)
	w.Header().Set("showError", "wrong request method")
}

// WrongPath shows that a path does not exist (Status Code 404)
func WrongPath(w http.ResponseWriter, r *http.Request) {
	url := r.URL.EscapedPath()
	Respond(
		Construct(http.StatusNotFound).
			WithLog(
				fmt.Sprintf("%s is not a valid path",
					url,
				),
			).
			WithLog(
				fmt.Sprintf("\nRequested URL was %s with header %s.",
					r.URL, r.Header,
				),
			).
			WithStackTrace(2).
			Finish(),
		w,
	)

	w.Header().Set("showError", "wrong path")
}
