/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package errors

import (
	"bytes"
	"errors"
	"fmt"
	"net/http"
	"runtime"
	"testing"

	"log"
	"net/http/httptest"
	"os"

	"io"
)

func TestConstructWithStackTrace(t *testing.T) {
	err := ConstructWithStackTrace(http.StatusInternalServerError, "log").Finish()
	_, file, line, _ := runtime.Caller(0)
	expectedLog := fmt.Sprintf("log\n    Created at: %s:%d", file, line-1)
	if err.StatusCode() != http.StatusInternalServerError {
		t.Errorf("Status codes don't match: expected %d but got %d.",
			http.StatusInternalServerError,
			err.StatusCode())
	}
	if err.Body() != "" {
		t.Errorf("Expected body to be empty but got %q.", err.Body())
	}
	if err.Error() != expectedLog {
		t.Errorf("Expected log to be: %q but got %q.", expectedLog, err.Error())
	}
}

func TestHandlerError_WithBodyModal(t *testing.T) {
	title := "Title"
	msg := "Message"
	expected := bytes.NewBufferString("")
	err := getErrorTemplate().Execute(expected, struct {
		Title   string
		Message string
	}{title, msg})
	if err != nil {
		t.Errorf("Unexpected error: %s", err)
	}

	result := Construct(http.StatusOK).WithBodyModal(title, msg).Finish().Body()
	if result != expected.String() {
		t.Errorf("Bodies don't match: expected %q but got %q.", expected, result)
	}
}

func ExampleHandlerErrorConstructor_WriteToBody() {
	err := Construct(http.StatusGone).
		WriteToBody(func(w io.Writer) {
			fmt.Fprintln(w, "Write to body")
			fmt.Fprintln(w, "Write again")
		}).Finish()

	fmt.Println(err.Body())
	//Output:
	//Write to body
	//Write again
}

func ExampleHandlerErrorConstructor_Copy() {
	original := Construct(http.StatusGone)
	original.WithBody("This is the original body.")
	copied := original.Copy()
	copied.WithBody("\nThis was added to the copy.")

	oErr := original.Finish()
	cErr := copied.Finish()

	fmt.Println(oErr.Body())
	fmt.Println("Copied body:")
	fmt.Println(cErr.Body())
	//Output:
	//This is the original body.
	//Copied body:
	//This is the original body.
	//This was added to the copy.
}

func ExampleRespond() {
	recorder := httptest.NewRecorder()
	err := Construct(http.StatusInternalServerError).
		WithLog("This is the log message.").
		WithCause(errors.New("this is the causing error")).
		WithBody("This will be the body.").
		Finish()
	// Remove time stamp form log in order to
	// allow the output to be reliable.
	log.SetFlags(0)
	// Let log write to Stdout to show logged
	// data as output.
	log.SetOutput(os.Stdout)
	Respond(err, recorder)
	fmt.Println("-Status-")
	fmt.Println(recorder.Result().StatusCode)
	fmt.Println("-Body-")
	fmt.Println(recorder.Body.String())
	//Output:
	//Handler error. Status: Internal Server Error
	//This is the log message.
	//     Caused by: this is the causing error
	//-Status-
	//500
	//-Body-
	//This will be the body.
}

func TestHandlerError_WithCause(t *testing.T) {
	defer testForPanic(t)
	Construct(http.StatusGone).WithCause(nil)
}

func TestHandlerError_WithStackTrace(t *testing.T) {
	defer testForPanic(t)
	Construct(http.StatusGone).WithStackTrace(9)
}

func TestHandlerError_WithStackTrace2(t *testing.T) {
	expected := "log"
	err := Construct(http.StatusGone).
		WithLog(expected).
		WithStackTrace(6000).
		Finish()
	if err.Error() != expected {
		t.Errorf("Invalid stack trace number expected log not change. "+
			"But it did change from %q to %q.", expected, err.Error())
	}
}

func testForPanic(t *testing.T) {
	if r := recover(); r == nil {
		t.Errorf("Expected code to panic but it didn't.")
	}
}

func ExampleConstruct() {
	err := Construct(http.StatusInternalServerError).
		WithLog("This is the log message.").
		WithCause(errors.New("this is the causing error")).
		WithBody("This will be the body.").
		Finish()
	fmt.Println(err.StatusCode())
	fmt.Println(err.Body())
	fmt.Println(err.Error())
	//Output:
	//500
	//This will be the body.
	//This is the log message.
	//     Caused by: this is the causing error
}
