/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package templates

import (
	"html/template"
	"path/filepath"

	"log"

	"gitlab.com/stp-team/systemtestportal/config"
)

// cache caches loaded templates to avoid reloading them.
var cache = make(map[string]*template.Template)

// loadedTemplateImpl is an implementation of the LoadedTemplate interfaces.
// It simply wraps the html/template.Template from the standard library.
type loadedTemplateImpl struct {
	tmpl  *template.Template
	funcs template.FuncMap
}

// LoadedTemplate is a convenience type used to improve the usage of templates
// in this project.
type LoadedTemplate interface {
	// Append appends the templates with given names to the tree of this template.
	//
	// If this call doesn't get any arguments a new template with an empty
	// string as name is added to the tree.
	//
	// This method does directly modify the template it is called on and
	// returns the said template for chaining.
	Append(names ...string) LoadedTemplate

	// Funcs adds functions to the templates tree that can be used by the
	// templates accordingly.
	//
	// This method does directly modify the template it is called on and
	// returns the said template for chaining.
	Funcs(funcMap template.FuncMap) LoadedTemplate

	// Lookup searches the template tree for a template with given name.
	//
	// This call doesn't affect the template its called on and returns a new
	// template representing the found template.
	// If no template with given name is found nil is returned.
	Lookup(name string) LoadedTemplate

	// Clone clones a loaded templates so it can be reused.
	//
	// Changes to the returned template don't affect this template.
	Clone() LoadedTemplate

	// Get gets the loaded standard library template.
	Get() *template.Template
}

// Append appends the templates with given names to the tree of this template.
//
// If this call doesn't get any arguments a new template with an empty
// string as name is added to the tree.
//
// This method does directly modify the template it is called on and
// returns the said template for chaining.
func (et loadedTemplateImpl) Append(names ...string) LoadedTemplate {
	var base *template.Template
	if len(et.funcs) > 0 {
		base = template.New("").Funcs(et.funcs)
	}
	et.tmpl = combine(et.tmpl, Append(base, names...))
	return et
}

// Funcs adds functions to the templates tree that can be used by the
// templates accordingly.
//
// This method does directly modify the template it is called on and
// returns the said template for chaining.
func (et loadedTemplateImpl) Funcs(funcMap template.FuncMap) LoadedTemplate {
	merge(et.funcs, funcMap)
	et.tmpl.Funcs(funcMap)
	return et
}

// Lookup searches the template tree for a template with given name.
//
// This call doesn't affect the template its called on and returns a new
// template representing the found template.
// If no template with given name is found nil is returned.
func (et loadedTemplateImpl) Lookup(name string) LoadedTemplate {
	tmpl := et.tmpl.Lookup(name)
	if tmpl == nil {
		return nil
	}
	return &loadedTemplateImpl{tmpl, et.funcs}
}

// Clone clones a loaded templates so it can be reused.
//
// Changes to the returned template don't affect this template.
func (et loadedTemplateImpl) Clone() LoadedTemplate {
	return &loadedTemplateImpl{template.Must(et.tmpl.Clone()), et.funcs}
}

// Get gets the loaded standard library template.
func (et loadedTemplateImpl) Get() *template.Template {
	return template.Must(et.tmpl.Clone())
}

// Load loads a template tree containing templates with the given names.
//
// If a template can't be found this method panics.
// If no names are passed the loaded template tree will only contain
// a template with an empty string as name.
func Load(names ...string) LoadedTemplate {
	return &loadedTemplateImpl{Get(names...), make(map[string]interface{})}
}

// Append parses the templates with the given names together onto
// the given base template. Every loaded template is cached together with the
// base so be careful to pass only a minimal base that contains
// everything needed to load the templates with given names (normally functions).
//
// The given base should've never been executed yet. If it has been this method
// will panic.
//
// If the base is nil it is omitted.
// If no names are passed for the templates either the base or if its nil
// a new template with an empty string as name will be returned.
//
// The name only consists of the filename without extension.
// So for example instead of 'templates/header.tmpl' only 'header' needs to be passed.
// If the template file is nested into a directory you can simply type:
// 'my/special/dir/header'
func Append(base *template.Template, names ...string) *template.Template {
	if len(names) == 0 {
		if base == nil {
			return template.New("")
		}
		return base
	}
	var result *template.Template
	for _, name := range names {
		tmpl := parseSingleTemplate(base, name)
		result = combine(result, tmpl)
	}
	return result
}

// Get parses the templates with the given names together as a tree.
// If no names are passed the loaded template tree will only contain
// a template with an empty string as name.
//
// The name only consists of the filename without extension.
// So for example instead of 'templates/header.tmpl' only 'header' needs to be passed.
// If the template file is nested into a directory you can simply type:
// 'my/special/dir/header'
func Get(names ...string) *template.Template {
	return Append(nil, names...)
}

// combine combines the trees of the given templates.
// All templates contained in appendix are added to the tree of base.
func combine(base *template.Template, appendix *template.Template) *template.Template {
	if base == nil {
		return appendix
	}
	for _, t := range appendix.Templates() {
		if t.Tree == nil {
			continue
		}
		_, err := base.AddParseTree(t.Name(), t.Tree)
		if err != nil {
			log.Panicf("Failed to combine "+
				"templates %+v and %+v because %s", base, appendix, err)
		}
	}
	return base
}

// merge merges two function maps into each other.
// The values of the first one get overridden by the values of the second one.
func merge(funcMap template.FuncMap, funcMap2 template.FuncMap) {
	for k, v := range funcMap2 {
		funcMap[k] = v
	}
}

// ClearCache clears the cache of templates forcing a reload of every previously
// cached template.
func ClearCache() {
	for k := range cache {
		delete(cache, k)
	}
}

// Uncache removes the templates with given name form the cache forcing a reload
// of these templates when they are loaded the next time.
func Uncache(names ...string) {
	for _, k := range names {
		delete(cache, k)
	}
}

// parseSingleTemplate loads the templates with given name and appends it to the
// given base. If the given base is nil, the templates with given names are
// loaded without a base.
//
// The loaded templates are cached with the given base so be careful to only pass
// a minimal base that is required to load the given templates (normally functions).
func parseSingleTemplate(base *template.Template, name string) *template.Template {
	tmpl, ok := cache[name]
	if ok {
		return tmpl
	}
	path := getTemplateFile(name)

	if base == nil {
		cache[name] = template.Must(template.ParseFiles(path))
	} else {
		cache[name] = template.Must(template.Must(base.Clone()).ParseFiles(path))
	}
	return cache[name]
}

// getTemplateFile generates the absolute file path for a template with given name.
func getTemplateFile(name string) string {
	return filepath.Join(config.BasePath(), "templates", name+".tmpl")
}
