/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"testing"

	"net/http"

	"net/url"

	"net/http/httptest"

	"strconv"

	"time"

	"gitlab.com/stp-team/systemtestportal/domain/test"
	"gitlab.com/stp-team/systemtestportal/store/execution"
	project2 "gitlab.com/stp-team/systemtestportal/store/project"
	test2 "gitlab.com/stp-team/systemtestportal/store/test"
	"gitlab.com/stp-team/systemtestportal/web/context"
	"gitlab.com/stp-team/systemtestportal/web/sessions"
)

const emptyBodyErrorMessage = "Answer body is empty, but shouldn't."
const getRecordErrorMessage = "Couldn't get current record from session store."
const noRecordInSessionErrorMessage = "No record was saved to session store."
const RecordInSessionErrorMessage = "Expected no Record in session store, but found %v+."
const environmentErrorMessage = "Environment wasn't transferred correctly to new Record. " +
	"Value in Record is %q but expected %q"
const sutErrorMessage = "SUT-Version wasn't transferred correctly to new Record. " +
	"Value in Record is %q but expected %q"
const resultsErrorMessage = "Result wasn't saved correctly to Record. Result in Record is %q but expected %q"
const commentErrorMessage = "Comment wasn't saved correctly to Record. Comment in Record is %q but expected %q"
const observedBehaviourErrorMessage = "Observed behaviour wasn't saved correctly to Record. " +
	"Observed behaviour in Record is %q but expected %q"
const neededTimeErrorMessage = "NeededTime wasn't saved correctly to Record. NeededTime in Record is %q but expected %q"
const visitedErrorMessage = "Saved test step was marked as not visited, but was visited"

const environment = "Win 10"
const sutVersion = "2.0.1"

var p, _, _ = project2.GetStore().Get("user", "project-1")
var tc, _, _ = test2.GetCaseStore().Get("user", "project-1", "test-case-1")
var tcv = tc.TestCaseVersions[0]
var tcr = test.NewCaseExecutionRecord(*tc, len(tc.TestCaseVersions), p.ID, "1.5 Alpha", "Win 10")
var ts, _, _ = test2.GetSequenceStore().Get("user", "project-1", "test-sequence-1")
var tsv = ts.SequenceVersions[0]
var tsr = test.NewSequenceExecutionRecord(*ts, len(ts.SequenceVersions), p.ID, sutVersion, environment)
var caseNrLast = len(tsv.Cases)
var tcLast = tsv.Cases[caseNrLast-1]
var tcvLast = tcLast.TestCaseVersions[0]
var tcrLast = test.NewCaseExecutionRecord(tcLast, len(tcLast.TestCaseVersions), p.ID, sutVersion, environment)
var tcPath = "/user/" + p.ID + "/testcases/" + tc.ID + "/execute"
var tsPath = "/user/" + p.ID + "/testsequences/" + ts.ID + "/execute"

func setup(tcr *test.CaseExecutionRecord, tsr *test.SequenceExecutionRecord) {
	sessions.InitSessionManagement(&sessions.TestStore{}, nil)
	if tcr != nil {
		sessions.GetSessionStore().SetCurrentCaseRecord(nil, nil, tcr)
	}
	if tsr != nil {
		sessions.GetSessionStore().SetCurrentSequenceRecord(nil, nil, tsr)
	}
	context.Init(sessions.GetSessionStore())
}

func TestGetCase(t *testing.T) {
	setup(nil, nil)
	getRequest(t, tcPath, http.StatusOK, true, ProjectHandler)
}
func TestPostCaseStart(t *testing.T) {
	setup(nil, nil)

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, "0")
	values.Add(keyCaseNr, "0")
	values.Add(keyEnvironment, environment)
	values.Add(keySUTVersion, sutVersion)

	req, _ := testPostRequest(t, tcPath, values, ProjectHandler, http.StatusOK, false)
	rec := getCurrentCaseRecord(t, req, false)
	if rec.Environment != environment {
		t.Errorf(environmentErrorMessage, rec.Environment, environment)
	}
	if rec.SUTVersion != sutVersion {
		t.Errorf(sutErrorMessage, rec.SUTVersion, sutVersion)
	}
}

func testPostStep(t *testing.T, stepNr int, caseNr int, path string) {
	const result = test.PassWithComment
	const comment = "What's that? I think should write a comment."
	const observed = "Was all ok, but very slow."
	const min = 4
	const sec = 30

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, strconv.Itoa(stepNr))
	values.Add(keyCaseNr, strconv.Itoa(caseNr))
	values.Add(keyResult, strconv.Itoa(result.Integer()))
	values.Add(keyComment, comment)
	values.Add(keyObservedBehavior, observed)
	values.Add(keyMinutes, strconv.Itoa(min))
	values.Add(keySeconds, strconv.Itoa(sec))

	req, _ := testPostRequest(t, path, values, ProjectHandler, http.StatusOK, false)

	rec := getCurrentCaseRecord(t, req, false)
	stepRec := rec.StepRecords[stepNr-1]

	if !stepRec.Visited {
		t.Error(visitedErrorMessage)
	}
	if stepRec.Result != result {
		t.Errorf(resultsErrorMessage, stepRec.Result, result)
	}
	if stepRec.Comment != comment {
		t.Errorf(commentErrorMessage, stepRec.Comment, comment)
	}
	if stepRec.ObservedBehavior != observed {
		t.Errorf(observedBehaviourErrorMessage, stepRec.ObservedBehavior, observed)
	}
	if stepRec.NeededTime != min*time.Minute+sec*time.Second {
		t.Errorf(neededTimeErrorMessage, stepRec.NeededTime, min*time.Minute+sec*time.Second)
	}
}

func TestPostCaseStep(t *testing.T) {
	setup(&tcr, nil)

	testPostStep(t, 1, 0, tcPath)
	testPostStep(t, len(tcv.Steps), 0, tcPath)
}

func TestPostCaseSummary(t *testing.T) {
	setup(&tcr, nil)

	stepNr := len(tcv.Steps) + 1
	const comment = "I'm not motivated anymore for further testing!"
	const result = test.Pass
	const min = 4
	const sec = 30

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, strconv.Itoa(stepNr))
	values.Add(keyCaseNr, "0")
	values.Add(keyComment, comment)
	values.Add(keyResult, result.String())
	values.Add(keyMinutes, strconv.Itoa(min))
	values.Add(keySeconds, strconv.Itoa(sec))

	req, _ := testPostRequest(t, tcPath, values, ProjectHandler, http.StatusSeeOther, true)
	getCurrentCaseRecord(t, req, true)
	recSlice, err := execution.GetRecordStore().GetCaseExecutionRecords(p.ID, tc.ID)
	if err != nil {
		t.Error(err)
	}
	rec := recSlice[len(recSlice)-1]
	if rec.Comment != comment {
		t.Errorf(commentErrorMessage, rec.Comment, comment)
	}
	if rec.Result != result {
		t.Errorf(resultsErrorMessage, rec.Result, result)
	}
	if rec.OtherNeededTime != min*time.Minute+sec*time.Second {
		t.Errorf(neededTimeErrorMessage, rec.OtherNeededTime, min*time.Minute+sec*time.Second)
	}
}

func TestGetSequence(t *testing.T) {
	setup(nil, nil)
	getRequest(t, tsPath, http.StatusOK, true, ProjectHandler)
}
func TestPostSequenceStart(t *testing.T) {
	setup(nil, nil)

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, "0")
	values.Add(keyCaseNr, "0")
	values.Add(keyEnvironment, environment)
	values.Add(keySUTVersion, sutVersion)

	req, _ := testPostRequest(t, tsPath, values, ProjectHandler, http.StatusOK, false)
	rec := getCurrentSequenceRecord(t, req, false)
	if rec.Environment != environment {
		t.Errorf(environmentErrorMessage, rec.Environment, environment)
	}
	if rec.SUTVersion != sutVersion {
		t.Errorf(sutErrorMessage, rec.SUTVersion, sutVersion)
	}
}
func TestPostSequenceCaseStart(t *testing.T) {
	setup(nil, &tsr)

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, "0")
	values.Add(keyCaseNr, "1")
	values.Add(keyEnvironment, environment)
	values.Add(keySUTVersion, sutVersion)

	req, _ := testPostRequest(t, tsPath, values, ProjectHandler, http.StatusOK, false)
	rec := getCurrentCaseRecord(t, req, false)
	if rec.Environment != environment {
		t.Errorf(environmentErrorMessage, rec.Environment, environment)
	}
	if rec.SUTVersion != sutVersion {
		t.Errorf(sutErrorMessage, rec.SUTVersion, sutVersion)
	}
}

func TestPostSequenceStep(t *testing.T) {
	setup(&tcr, &tsr)
	testPostStep(t, 1, 1, tsPath)
	testPostStep(t, 2, 1, tsPath)
	setup(&tcrLast, &tsr)
	testPostStep(t, len(tcv.Steps), 1, tsPath)
	testPostStep(t, len(tcv.Steps), len(tsv.Cases), tsPath)
}

func getCurrentCaseRecord(t *testing.T, r http.Request, expectedEmpty bool) *test.CaseExecutionRecord {
	rec, err := sessions.GetSessionStore().GetCurrentCaseRecord(&r)
	if err != nil {
		t.Error(getRecordErrorMessage)
	}
	if !expectedEmpty && rec == nil {
		t.Error(noRecordInSessionErrorMessage)
	}
	if expectedEmpty && rec != nil {
		t.Errorf(RecordInSessionErrorMessage, rec)
	}
	return rec
}
func TestPostSequenceCaseSummary(t *testing.T) {
	setup(&tcr, &tsr)

	const comment = "I'm not motivated anymore for further testing!"
	const result = test.Pass
	const min = 4
	const sec = 30

	var stepNr = len(tcvLast.Steps) + 1

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, strconv.Itoa(stepNr))
	values.Add(keyCaseNr, "1")
	values.Add(keyComment, comment)
	values.Add(keyResult, result.String())
	values.Add(keyMinutes, strconv.Itoa(min))
	values.Add(keySeconds, strconv.Itoa(sec))

	req, _ := testPostRequest(t, tsPath, values, ProjectHandler, http.StatusOK, false)
	getCurrentCaseRecord(t, req, true)
	getCurrentSequenceRecord(t, req, false)
	recSlice, err := execution.GetRecordStore().GetCaseExecutionRecords(p.ID, tc.ID)
	if err != nil {
		t.Error(err)
	}
	rec := recSlice[len(recSlice)-1]
	if rec.Comment != comment {
		t.Errorf(commentErrorMessage, rec.Comment, comment)
	}
	if rec.Result != result {
		t.Errorf(resultsErrorMessage, rec.Result, result)
	}
	if rec.OtherNeededTime != min*time.Minute+sec*time.Second {
		t.Errorf(neededTimeErrorMessage, rec.OtherNeededTime, min*time.Minute+sec*time.Second)
	}
}
func TestPostSequenceLastCaseSummary(t *testing.T) {
	tsRecord, _ := execution.GetRecordStore().GetSequenceExecutionRecord(p.ID, ts.ID, 1)
	tsRecord.SUTVersion = sutVersion
	tsRecord.Environment = environment
	tsRecord.ID = 0
	setup(&tcrLast, &tsRecord)

	const comment = "I'm not motivated anymore for further testing!"
	const result = test.Pass
	const min = 4
	const sec = 30

	var stepNr = len(tcvLast.Steps) + 1

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, strconv.Itoa(stepNr))
	values.Add(keyCaseNr, strconv.Itoa(caseNrLast))
	values.Add(keyComment, comment)
	values.Add(keyResult, result.String())
	values.Add(keyMinutes, strconv.Itoa(min))
	values.Add(keySeconds, strconv.Itoa(sec))

	req, _ := testPostRequest(t, tsPath, values, ProjectHandler, http.StatusOK, false)
	getCurrentCaseRecord(t, req, true)
	getCurrentSequenceRecord(t, req, true)

	//check case record
	caseRecSlice, err := execution.GetRecordStore().GetCaseExecutionRecords(p.ID, tcLast.ID)
	if err != nil {
		t.Error(err)
	}
	CaseRec := caseRecSlice[len(caseRecSlice)-1]
	if CaseRec.Comment != comment {
		t.Errorf(commentErrorMessage, CaseRec.Comment, comment)
	}
	if CaseRec.Result != result {
		t.Errorf(resultsErrorMessage, CaseRec.Result, result)
	}
	if CaseRec.OtherNeededTime != min*time.Minute+sec*time.Second {
		t.Errorf(neededTimeErrorMessage, CaseRec.OtherNeededTime, min*time.Minute+sec*time.Second)
	}

	//check sequence record
	seqRecSlice, err := execution.GetRecordStore().GetSequenceExecutionRecords(p.ID, ts.ID)
	if err != nil {
		t.Error(err)
	}
	SeqRec := seqRecSlice[len(seqRecSlice)-1]
	if SeqRec.Environment != environment {
		t.Errorf(environmentErrorMessage, SeqRec.Environment, environment)
	}
	if SeqRec.SUTVersion != sutVersion {
		t.Errorf(sutErrorMessage, SeqRec.SUTVersion, sutVersion)
	}
}
func TestPostSequenceSummary(t *testing.T) {
	setup(nil, nil)

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, "1")
	values.Add(keyCaseNr, "0")

	testPostRequest(t, tsPath, values, ProjectHandler, http.StatusSeeOther, true)
}

func TestBadRequest(t *testing.T) {
	setup(nil, nil)
	testPostRequestEmpty(t, tcPath, http.StatusBadRequest)
	testPostRequestEmpty(t, tsPath, http.StatusBadRequest)
	values := url.Values{}
	values.Add(keyStepNr, strconv.Itoa(len(tcv.Steps)+5))
	testPostRequest(t, tcPath, values, ProjectHandler, http.StatusBadRequest, true)
	values = url.Values{}
	values.Add(keyCaseNr, "0")
	values.Add(keyStepNr, "2")
	testPostRequest(t, tsPath, values, ProjectHandler, http.StatusBadRequest, true)
	values = url.Values{}
	values.Add(keyCaseNr, strconv.Itoa(len(tsv.Cases)+1))
	testPostRequest(t, tsPath, values, ProjectHandler, http.StatusBadRequest, true)
	values = url.Values{}
	values.Add(keyCaseNr, "1")
	values.Add(keyStepNr, "200")
	testPostRequest(t, tsPath, values, ProjectHandler, http.StatusBadRequest, true)
}

func getCurrentSequenceRecord(t *testing.T, r http.Request, expectedEmpty bool) *test.SequenceExecutionRecord {
	rec, err := sessions.GetSessionStore().GetCurrentSequenceRecord(&r)
	if err != nil {
		t.Error(getRecordErrorMessage)
	}
	if !expectedEmpty && rec == nil {
		t.Error(noRecordInSessionErrorMessage)
	}
	if expectedEmpty && rec != nil {
		t.Errorf(RecordInSessionErrorMessage, rec)
	}
	return rec
}

func testPostRequest(t *testing.T, path string, formValues url.Values, handler http.HandlerFunc,
	statusWant int, emptyAnswerBodyOk bool) (http.Request, httptest.ResponseRecorder) {
	req, err := http.NewRequest(http.MethodPost, path, nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Form = formValues
	rec := httptest.NewRecorder()
	handler.ServeHTTP(rec, req)

	if statusWant != rec.Code {
		t.Errorf("Returned wrong status code. Expected %v but got %v", statusWant, rec.Code)
	}
	if !emptyAnswerBodyOk && rec.Body.Len() == 0 {
		t.Error(emptyBodyErrorMessage)
	}
	return *req, *rec
}
