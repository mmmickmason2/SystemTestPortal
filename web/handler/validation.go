/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

// validation.go contains the interfaces and functions to validate projects, groups, test cases and test sequences

import (
	"gitlab.com/stp-team/systemtestportal/domain/group"
	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/test"
	"gitlab.com/stp-team/systemtestportal/domain/visibility"
	groupStore "gitlab.com/stp-team/systemtestportal/store/group"
	projectStore "gitlab.com/stp-team/systemtestportal/store/project"
	testStore "gitlab.com/stp-team/systemtestportal/store/test"
)

// ValidationItemBasic is an item with a name and an id to verify. This item can be a project, a group, a test case
// or a test sequence.
type ValidationItemBasic interface {
	ItemName() string
	ItemID() string
}

// ValidationVisibilityItem is an item with a name, an id and a visibility. This item can be a project or a group.
type ValidationVisibilityItem interface {
	ValidationItemBasic
	ItemVisibility() visibility.Visibility
}

// validateItem checks that a given test (test case or test sequence) contains valid information and does not exist yet
func validateItem(p project.Project, v ValidationItemBasic) error {
	if err := validateNameAndID(v); err != nil {
		return err
	}

	if err := validateTestUniqueness(v, p); err != nil {
		return err
	}

	return nil
}

// validateContainer checks that a given item to validateContainer contains valid information
func validateContainer(v ValidationVisibilityItem) error {
	if err := validateNameAndID(v); err != nil {
		return err
	}

	if err := validateContainerUniqueness(v); err != nil {
		return err
	}

	if v.ItemVisibility() != visibility.Public && v.ItemVisibility() != visibility.Internal &&
		v.ItemVisibility() != visibility.Private {
		return InvalidVisibility()
	}
	return nil
}

// validateNameAndID validates the name and the id of a given item
func validateNameAndID(v ValidationItemBasic) error {
	if v.ItemName() == "" {
		return EmptyName()
	}

	if v.ItemID() == "" {
		return EmptyID()
	}

	return nil
}

func validateTestUniqueness(v ValidationItemBasic, p project.Project) error {
	switch v.(type) {
	case test.Case:
		if _, ok, _ := testStore.GetCaseStore().Get("user", p.ID, v.ItemID()); ok {
			return DuplicateID()
		}
	case test.Sequence:
		if _, ok, _ := testStore.GetSequenceStore().Get("user", p.ID, v.ItemID()); ok {
			return DuplicateID()
		}
	}

	return nil
}

func validateContainerUniqueness(v ValidationVisibilityItem) error {
	switch i := v.(type) {
	case project.Project:
		if _, ok, _ := projectStore.GetStore().Get("user", i.ID); ok {
			return DuplicateID()
		}
	case group.Group:
		if _, ok, _ := groupStore.GetStore().Get(v.ItemID()); ok {
			return DuplicateID()
		}
	}

	return nil
}
