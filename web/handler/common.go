/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"html/template"
	"net/http"
	"time"

	"gitlab.com/stp-team/systemtestportal/web/errors"
	"gitlab.com/stp-team/systemtestportal/web/templates"
)

// EmptyID returns an ErrorMessage describing a situation where a given name would return an empty ID.
func EmptyID() errors.HandlerError {
	return errors.Construct(http.StatusBadRequest).
		WithLog("An empty id was send by the client.").
		WithBodyModal("Invalid name",
			"The given name must contain at least one alphanumeric character.").
		WithStackTrace(2).
		Finish()
}

// EmptyName returns an ErrorMessage detailing that a given name is considered empty by the system.
func EmptyName() errors.HandlerError {
	return errors.Construct(http.StatusBadRequest).
		WithLog("An empty name was send by the client.").
		WithBodyModal("Invalid name",
			"The given name is empty or "+
				"consists only of whitespace.").
		WithStackTrace(2).
		Finish()
}

// DuplicateID returns an ErrorMessage detailing that a given name is already used elsewhere.
func DuplicateID() errors.HandlerError {
	return errors.Construct(http.StatusConflict).
		WithLog("The client send a name that already exists.").
		WithBodyModal("Invalid name",
			"The given name already exists.").
		WithStackTrace(2).
		Finish()
}

// InvalidVisibility returns an ErrorMessage describing that a value is not valid.
func InvalidVisibility() errors.HandlerError {
	return errors.Construct(http.StatusBadRequest).
		WithLog("The client send an invalid visibility handle.").
		WithBodyModal("Invalid visibility", "The visibility has to be "+
			"either public, internal or private").
		WithStackTrace(2).
		Finish()
}

// InvalidID returns an ErrorMessage describing that an id is not valid
func InvalidID() errors.HandlerError {
	return errors.Construct(http.StatusNotFound).
		WithLog("The client tried to access an id that doesn't exist.").
		WithBodyModal("Invalid id", "The id is not valid").
		WithStackTrace(2).
		Finish()
}

// HandleError checks what type of error occurred and send an approriate
// response to the client.
func HandleError(err error, w http.ResponseWriter) {
	if err == nil {
		return
	}
	switch herr := err.(type) {
	case errors.HandlerError:
		errors.Respond(herr, w)
	default:
		errors.Respond(errors.
			Construct(http.StatusInternalServerError).
			WithLog("An unknown error occurred.").
			WithStackTrace(2).
			WithCause(herr).
			Finish(),
			w,
		)
	}
}

var (
	defaultFuncMap = template.FuncMap{
		"TimeSince": func(t time.Time) string { return formatTimeSince(time.Since(t)) },
	}
)

// getBaseTree returns a loaded base template containing
// only utility function used by the templates.
func getBaseTree() templates.LoadedTemplate {
	return templates.Load().Funcs(defaultFuncMap)
}

// getNoSideBarTree returns a loaded template structure for a
// view containing the sidebar.
func getSideBarTree() templates.LoadedTemplate {
	return getBaseTree().Append("header", "navbar", "footer"). // Navbar tree
		// Modal login tree
		Append("modal-login").
		// Sidebar tree
		Append("content-sidebar", "menu-explore")
}

// getNoSideBarTree returns a loaded template structure for a
// view not containing the sidebar.
func getNoSideBarTree() templates.LoadedTemplate {
	return getBaseTree().Append("header", "navbar", "footer"). // Navbar tree
		// Modal login tree
		Append("modal-login").
		// No sidebar tree
		Append("content-no-sidebar")
}
