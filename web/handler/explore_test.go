/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"
	"testing"
)

func TestExploreHandler(t *testing.T) {
	urls := []struct {
		url        string
		statusWant int
	}{
		{"/explore", http.StatusOK},
		{"/explore/", http.StatusOK},
		{"/explore/invalid", http.StatusNotFound},
		{"/explore/projects", http.StatusOK},
		{"/explore/projects/", http.StatusOK},
		{"/explore/groups", http.StatusOK},
		{"/explore/groups/", http.StatusOK},
	}
	for _, url := range urls {
		getRequest(t, url.url, url.statusWant, false, ExploreHandler)
		getRequest(t, url.url, url.statusWant, true, ExploreHandler)
	}
}
