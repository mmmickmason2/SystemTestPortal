/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"log"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/test"
	testStore "gitlab.com/stp-team/systemtestportal/store/test"
	"gitlab.com/stp-team/systemtestportal/web/action"
)

// TestTestStepHandlerBadRequest checks if bad requests are handled correctly
func TestTestStepHandlerBadRequest(t *testing.T) {
	req, err := http.NewRequest(http.MethodOptions,
		"/user/"+dummyProject.ID+"/testcases/test-case-1/teststeps/", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	ProjectHandler(rr, req)

	if status := rr.Code; status != http.StatusMethodNotAllowed {
		t.Errorf("function returned wrong status code: got %v want %v",
			status, http.StatusMethodNotAllowed)
	}
}

func TestTestStepGetRequest(t *testing.T) {
	urls := []struct {
		url        string
		statusWant int
	}{
		{"/user/" + dummyProject.ID + "/testcases/test-case-1/teststeps/", http.StatusOK},
		{"/user/" + dummyProject.ID + "/testcases/test-case-1/teststeps/1", http.StatusOK},
		{"/user/" + dummyProject.ID + "/testcases/test-case-1/teststeps/----", http.StatusNotFound},
		{"/user/" + dummyProject.ID + "/testcases/test-case-1/teststeps/new", http.StatusOK},
		{"/user/" + dummyProject.ID + "/testcases/test-case-1/teststeps/1/" + action.Edit, http.StatusOK},
	}
	for _, stepURL := range urls {
		req, err := http.NewRequest(http.MethodGet, stepURL.url, nil)
		if err != nil {
			t.Fatal(err)
		}
		rr := httptest.NewRecorder()
		ProjectHandler(rr, req)

		if status := rr.Code; status != stepURL.statusWant {
			t.Errorf("%s returned wrong status code: got %v want %v", stepURL.url, status, stepURL.statusWant)
		}
	}
}

func TestTestStepSaving(t *testing.T) {
	steps := []struct {
		sa          string
		se          string
		tc          test.Case
		project     project.Project
		statusWant  int
		isValidStep bool
	}{
		{"TEST THE NEW TEST STEP", "EXPECT TO SUCCEED", dummyTestCase,
			*dummyProject, http.StatusCreated, true},
		{"", "EXPECT TO FAIL", dummyTestCase,
			*dummyProject, http.StatusBadRequest, false},
		{"TEST INVALID EXPECTED RESULT", "", dummyTestCase,
			*dummyProject, http.StatusBadRequest, false},
	}
	for _, step := range steps {
		testStore.GetCaseStore().Add("user", step.project.ID, &step.tc)
		tc, _, _ := testStore.GetCaseStore().Get("user", step.project.ID, "test-case-1")
		tcSteps := tc.TestCaseVersions[0].Steps
		s := test.NewTestStep(step.sa, step.se)
		s.ID = len(tcSteps) + 1

		form := url.Values{}
		form.Add("inputTestStepAction", step.sa)
		form.Add("inputTestStepExpected", step.se)

		req, err := http.NewRequest(http.MethodPost,
			"/user/"+step.project.ID+"/testcases/test-case-1/teststeps/save", nil)
		if err != nil {
			t.Fatal(err)
		}

		req.Form = form
		rr := httptest.NewRecorder()
		ProjectHandler(rr, req)

		updatedTC, _, _ := testStore.GetCaseStore().Get("user", step.project.ID, step.tc.ID)
		log.Printf("%p %p", tc, updatedTC)

		if status := rr.Code; status != step.statusWant {
			t.Errorf("function returned wrong status code: got %v want %v",
				status, step.statusWant)
		}

		if !step.isValidStep && len(step.tc.TestCaseVersions[0].Steps) > s.ID {
			if ts := tc.TestCaseVersions[0].Steps[s.ID-1]; ts == s {
				t.Errorf("test step was mistakenly created, want %v got %v", s, ts)
			}
		}
		if step.isValidStep {
			log.Println(tc.TestCaseVersions)
			if ts := tc.TestCaseVersions[0].Steps[s.ID-1]; ts != s {
				t.Errorf("test step was not created, want %v got %v", s, ts)
			}
		}
	}

}

func TestTestStepUpdating(t *testing.T) {
	steps := []struct {
		sa         string
		se         string
		project    project.Project
		tc         test.Case
		statusWant int
	}{
		{"TEST UPDATE TEST STEP", "EXPECT TO UPDATE SUCCESSFULLY", *dummyProject,
			dummyTestCase, http.StatusCreated},
		{"", "EXPECT TO FAIL UPDATING", *dummyProject, dummyTestCase,
			http.StatusBadRequest},
	}
	for _, step := range steps {
		tc, _, _ := testStore.GetCaseStore().Get("user", step.project.ID, step.tc.ID)
		steps := tc.TestCaseVersions[0].Steps
		stepToUpdate := steps[1]

		form := url.Values{}
		form.Add("inputTestStepAction", step.sa)
		form.Add("inputTestStepExpected", step.se)

		putURL := "/user/" + step.project.ID + "/testcases/test-case-1/teststeps/2/" + action.Update

		testTestStepPutRequest(t, putURL, form, step.statusWant)

		updatedTC, _, _ := testStore.GetCaseStore().Get("user", step.project.ID, step.tc.ID)

		updatedStep := updatedTC.TestCaseVersions[0].Steps[stepToUpdate.ID-1]

		if step.statusWant == http.StatusBadRequest {
			alertResultsTestStepUpdatingInvalid(t, updatedStep, stepToUpdate)
		}
		if step.statusWant == http.StatusCreated {
			alertResultsTestStepUpdatingValid(t, updatedStep, stepToUpdate, step.sa, step.se)
		}
	}
}

// testTestStepPutRequest sends a put request to the url with the given values and checks if the response status
// is correct
func testTestStepPutRequest(t *testing.T, url string, values url.Values, statusWant int) {

	req, err := http.NewRequest(http.MethodPut, url, nil)
	if err != nil {
		t.Fatal(err)
	}

	req.Form = values
	rr := httptest.NewRecorder()
	ProjectHandler(rr, req)

	if status := rr.Code; status != statusWant {
		t.Errorf("%s returned wrong status code: got %v want %v", url, status, statusWant)
	}
}

// alertResultsTestStepUpdatingInvalid alerts the results of updating an invalid test step
func alertResultsTestStepUpdatingInvalid(t *testing.T, updatedStep, stepToUpdate test.Step) {
	if updatedStep != stepToUpdate {
		t.Errorf("test step was mistakenly updated, want %v got %v", stepToUpdate, updatedStep)
	}
}

// alertResultsTestStepUpdatingValid alerts the results of updating a valid test step
func alertResultsTestStepUpdatingValid(t *testing.T, updatedStep, stepToUpdate test.Step, action, expected string) {

	if updatedStep == stepToUpdate {
		t.Errorf("test step was not updated, want %v got %v", updatedStep, stepToUpdate)
	}
	if updatedStep.ID != stepToUpdate.ID {
		t.Errorf("test step ID changed, want %v got %v", stepToUpdate.ID, updatedStep.ID)
	}
	if updatedStep.Action != action {
		t.Errorf("test step action was not updated, want '%v' got '%v'", action, updatedStep.Action)
	}
	if updatedStep.ExpectedResult != expected {
		t.Errorf("test step expected result was not updated, want '%v' got '%v'", expected,
			updatedStep.ExpectedResult)
	}
}

func TestTestStepDeleting(t *testing.T) {
	steps := []struct {
		url        string
		statusWant int
	}{
		{"/user/" + dummyProject.ID + "/testcases/test-case-1/teststeps/asda", http.StatusNotFound},
		{"/user/" + dummyProject.ID + "/testcases/test-case-1/teststeps/3", http.StatusOK},
	}
	for _, step := range steps {

		req, err := http.NewRequest(http.MethodDelete, step.url, nil)
		if err != nil {
			t.Fatal(err)
		}

		tc, _, _ := testStore.GetCaseStore().Get("user", dummyProject.ID, "test-case-1")
		s := tc.TestCaseVersions[0].Steps[2]
		rr := httptest.NewRecorder()
		ProjectHandler(rr, req)

		tc, _, _ = testStore.GetCaseStore().Get("user", dummyProject.ID, "test-case-1")

		if status := rr.Code; status != step.statusWant {
			t.Errorf("function returned wrong status code: got %v want %v",
				status, step.statusWant)
		}
		for _, v := range tc.TestCaseVersions[0].Steps {
			if step.statusWant == http.StatusOK && v == s {
				t.Errorf("test step not removed, %v", v)
				break
			}
		}
	}
}
