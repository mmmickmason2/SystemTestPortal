/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"log"
	"net/http"

	"strings"

	"html/template"

	groupStore "gitlab.com/stp-team/systemtestportal/store/group"
	project2 "gitlab.com/stp-team/systemtestportal/store/project"
	userServer "gitlab.com/stp-team/systemtestportal/store/user"
	"gitlab.com/stp-team/systemtestportal/web/action"
	"gitlab.com/stp-team/systemtestportal/web/context"
	"gitlab.com/stp-team/systemtestportal/web/errors"
)

// ExploreHandler shows the default exploration screen
func ExploreHandler(w http.ResponseWriter, r *http.Request) {
	urlSeg := strings.Split(r.URL.EscapedPath(), "/")

	if len(urlSeg) > 2 {
		switch urlSeg[2] {
		case action.Show, Projects:
			tmpl := GetExploreProjectsTree()
			if err := tmpl.Execute(w, context.New().
				WithUserInformation(r).With("Projects", project2.GetStore().List("user"))); err != nil {
				log.Println(err)
			}
		case Groups:
			tmpl := GetExploreGroupsTree()
			if err := tmpl.Execute(w, context.New().
				WithUserInformation(r).With("Groups", groupStore.GetStore().List())); err != nil {
				log.Println(err)
			}
		case Users:
			tmpl := GetExploreUsersTree()
			users := userServer.GetStore().List()
			if err := tmpl.Execute(w, context.New().
				WithUserInformation(r).With("Users", users)); err != nil {
				log.Println(err)
			}
		default:
			errors.WrongPath(w, r)
		}
	}
}

// GetExploreGroupsTree returns the list groups template with all parent templates
func GetExploreGroupsTree() *template.Template {
	return getSideBarTree().
		// Explore projects tree
		Append("list-groups").
		Get().Lookup("header")
}

// GetExploreProjectsTree returns the list projects template with all parent templates
func GetExploreProjectsTree() *template.Template {
	return getSideBarTree().
		// Explore projects tree
		Append("list-projects").
		Get().Lookup("header")
}

// GetExploreUsersTree returns the list users template with all parent templates
func GetExploreUsersTree() *template.Template {
	return getSideBarTree().
		// Explore users tree
		Append("list-users").
		Get().Lookup("header")
}
