/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"html/template"
	"log"
	"net/http"

	"gitlab.com/stp-team/systemtestportal/web/context"
)

// AboutHandler is a handler for showing the about screen
func AboutHandler(w http.ResponseWriter, r *http.Request) {
	tmpl := GetAboutTree()
	if err := tmpl.Execute(w, context.New().WithUserInformation(r)); err != nil {
		log.Println(err)
	}
}

// GetAboutTree returns the about screen template with all parent templates
func GetAboutTree() *template.Template {
	return getNoSideBarTree().
		// About tree
		Append("about").
		Get().Lookup("header")
}
