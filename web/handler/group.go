/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"fmt"
	"log"
	"net/http"

	"html/template"

	"strings"

	"io"

	"gitlab.com/stp-team/systemtestportal/domain/group"
	"gitlab.com/stp-team/systemtestportal/domain/user"
	"gitlab.com/stp-team/systemtestportal/domain/visibility"
	groupStore "gitlab.com/stp-team/systemtestportal/store/group"
	userServer "gitlab.com/stp-team/systemtestportal/store/user"
	"gitlab.com/stp-team/systemtestportal/web/context"
	"gitlab.com/stp-team/systemtestportal/web/errors"
	"gitlab.com/stp-team/systemtestportal/web/slugs"
	"gitlab.com/stp-team/systemtestportal/web/templates"
)

// GroupHandler routes the request to the correct handler
func GroupHandler(w http.ResponseWriter, r *http.Request) {
	g, err := getGroupFromURL(r)
	if err != nil {
		errors.WrongPath(w, r)
		return
	}

	switch r.Method {
	case http.MethodGet:
		showGroupHandler(w, r, g)
	default:
		errors.WrongRequestMethod(w, r)
	}

}

// showGroupHandler shows the selected tab of a group
func showGroupHandler(w http.ResponseWriter, r *http.Request, g group.Group) {
	path := r.URL.EscapedPath()
	if strings.HasSuffix(path, "/") {
		path = strings.TrimSuffix(path, "/")
	}

	pathSeg := strings.Split(path, "/")

	if len(pathSeg) <= 2 {
		http.Redirect(w, r, path+"/activity", http.StatusSeeOther)
		return
	}
	tab := pathSeg[2]
	switch tab {
	case Activity:
		showGroupActivity(w, r, g)
	case Projects:
	case Members:
	case Settings:
	default:
		errors.WrongPath(w, r)
	}
}

// showGroupActivity shows the groups activity
func showGroupActivity(w io.Writer, r *http.Request, g group.Group) {
	tmpl := getTabGroupActivity(r)
	if err := tmpl.Execute(w, context.New().WithUserInformation(r).With("Group", g)); err != nil {
		log.Println(err)
	}
}

// getTabGroupActivity returns the templates for the groups activity tab
func getTabGroupActivity(r *http.Request) *template.Template {
	if isFragmentRequest(r) {
		return GetTabGroupActivityFragment()
	}
	return GetTabGroupActivityTree()
}

// NewGroupHandler is a handler for creating a group
func NewGroupHandler(w http.ResponseWriter, r *http.Request) {
	tmpl := GetNewGroupTree()
	users := userServer.GetStore().List()
	if err := tmpl.Execute(w, context.New().
		WithUserInformation(r).
		With("Users", users)); err != nil {
		log.Println(err)
	}
}

// SaveGroupHandler is a handler for saving a group
func SaveGroupHandler(w http.ResponseWriter, r *http.Request) {
	gn := r.FormValue("inputGroupName")
	gd := r.FormValue("inputGroupDesc")

	gvs := r.FormValue("inputGroupVisibility")
	gv, err := visibility.StringToVis(gvs)
	if err != nil {
		errors.Respond(InvalidVisibility(), w)
		return
	}

	if err = r.ParseForm(); err != nil {
		log.Println(err)
	}
	gms := r.Form["inputGroupMembers"]
	gm := make(map[string]user.User)
	for _, m := range gms {
		fmt.Println(m)
	}

	g := group.NewGroup(gn, gd, gv, gm)
	if vErr := validateContainer(g); vErr != nil {
		HandleError(vErr, w)
		return
	}

	err = groupStore.GetStore().Add(&g)
	if err != nil {
		HandleError(err, w)
		return
	}

	w.Header().Set("newName", slugs.Slugify(g.Name))
	w.WriteHeader(http.StatusOK)
}

// GetNewGroupTree returns the new group template with all parent templates
func GetNewGroupTree() *template.Template {
	return getNoSideBarTree().
		// New group tree
		Append("new-group").
		Get().Lookup("header")
}

// getGroupFromURL retrieves a group depending on the url. Returns an error InvalidID if the group does not exist.
func getGroupFromURL(r *http.Request) (group.Group, errors.HandlerError) {
	url := r.URL.EscapedPath()
	urlSeg := strings.Split(url, "/")

	g, ok, err := groupStore.GetStore().Get(urlSeg[1])

	if err != nil {
		return group.Group{}, errors.Construct(http.StatusInternalServerError).WithCause(err).Finish()
	}

	if !ok {
		return group.Group{}, InvalidID()
	}

	return *g, nil
}

// GetTabGroupActivityFragment returns only the activity tab template
func GetTabGroupActivityFragment() *template.Template {
	return templates.Load("group-tab-activity").
		Get().Lookup("tab-content")
}

// GetTabGroupActivityTree returns the group activity tab with all parent templates
func GetTabGroupActivityTree() *template.Template {
	return getNoSideBarTree().
		Append("content-group-tabs").
		Append("group-tab-activity").
		Get().Lookup("header")
}
