/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"
	"strings"
)

// The const contains the common tabs that can be selected in a project, group or in a users profile
const (
	Activity = "activity"
)

// The const contains the common tabs that can be chosen in a project or a group
const (
	Members  = "members"
	Settings = "settings"
)

// The const contains the exploration elements
const (
	Projects = "projects"
	Groups   = "groups"
	Users    = "users"
)

// RootHandler displays the starting page for the application
func RootHandler(w http.ResponseWriter, r *http.Request) {
	path := r.URL.EscapedPath()
	if strings.HasSuffix(path, "/") {
		path = strings.TrimSuffix(path, "/")
	}

	pathSeg := strings.Split(path, "/")

	if len(pathSeg) <= 1 || pathSeg[1] == "explore" {
		http.Redirect(w, r, "/explore", http.StatusSeeOther)
		return
	}
	GroupHandler(w, r)
}
