/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"encoding/json"
	"html/template"
	"io"
	"log"
	"net/http"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/test"
	"gitlab.com/stp-team/systemtestportal/store/execution"
	test2 "gitlab.com/stp-team/systemtestportal/store/test"
	"gitlab.com/stp-team/systemtestportal/web/context"
	"gitlab.com/stp-team/systemtestportal/web/errors"
)

func ProtocolHandler(w http.ResponseWriter, r *http.Request, p project.Project, pathSegments []string) {
	switch pathSegments[1] {
	case Protocols:
		handleProtocolGUI(w, r, p, pathSegments)
	case Records:
		handleProtocolData(w, r, pathSegments)
	default:
		errors.WrongRequestMethod(w, r)
		return
	}
}

// handleProtocolData sends a json encoded list of records (either cases or sequences)
func handleProtocolData(w io.Writer, r *http.Request, pathSegments []string) {
	if len(pathSegments) > 2 {
		var jsonBytes []byte
		if (pathSegments[2] == "testCases") || (pathSegments[2] == "testcases") {
			var records []test.CaseExecutionRecord
			records, _ = execution.GetRecordStore().GetCaseExecutionRecords(pathSegments[0], pathSegments[3])
			if records != nil {
				jsonBytes, _ = json.Marshal(records)
			}
		} else if pathSegments[2] == "testSequences" || (pathSegments[2] == "testsequences") {
			var records []test.SequenceExecutionRecord
			records, _ = execution.GetRecordStore().GetSequenceExecutionRecords(pathSegments[0], pathSegments[3])
			if records != nil {
				jsonBytes, _ = json.Marshal(records)
			}
		}
		w.Write(jsonBytes)
	}

}

// handleProtocolGUI executes the
func handleProtocolGUI(w http.ResponseWriter, r *http.Request, p project.Project, pathSegments []string) {
	if len(pathSegments) > 3 {
		//Protocol Details
		selType := r.FormValue("type")
		selCase := r.FormValue("selected")
		selSequence := r.FormValue("selected")
		if pathSegments[2] == "testcases" {
			getTabProtocolsShowTestCase(w, r, selType, selCase, selSequence, p)
		} else if pathSegments[2] == "testsequences" {
			getTabProtocolsShowTestSequence(w, r, selType, selCase, selSequence, p)
		}
	} else {
		//Protocols List
		selType := r.FormValue("type")
		selCase := r.FormValue("selected")
		selSequence := r.FormValue("selected")
		if (selType == "testCases") || (selType == "testSequences") {
			getTabProtocolsList(w, r, selType, selCase, selSequence, p)
		} else {
			getTabProtocolsList(w, r, "testCases", "", "", p)
		}
	}
}

//TEMPLATES - FRAGMENTS/TREES

// getTabProtocolsList executes either the protocol list fragment only or the protocol list fragment with all its parent
// fragments, depending on the isFrag parameter
func getTabProtocolsList(w http.ResponseWriter, r *http.Request, t string, c string, s string, p project.Project) {
	tmpl := GetTabProtocolsListTree()
	if isFragmentRequest(r) {
		tmpl = GetTabProtocolsListFragment()
	}

	ctx := context.New().
		WithUserInformation(r).
		With("SelectedTestCase", c).
		With("SelectedTestSequence", s).
		With("SelectedType", t).
		With("Project", p).
		With("TestCases", test2.GetCaseStore().List("user", p.ID)).
		With("TestSequences", test2.GetSequenceStore().List("user", p.ID))
	if err := tmpl.Execute(w, ctx); err != nil {
		log.Printf("'Failed to show protocols' error on template parsing: %s", err)
	}
}

// GetTabProtocolsListFragment returns only the protocol list tab template
func GetTabProtocolsListFragment() *template.Template {
	return getBaseTree().
		Append("tab-protocols-list").
		Get().Lookup("tab-content")
}

// GetTabProtocolsListTree returns the protocol list tab template with all parent templates
func GetTabProtocolsListTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab test sequence edit tree
		Append("tab-protocols-list").
		Get().Lookup("header")
}

// getTabProtocolsShowTestCase executes either the protocol show test case fragment only or the protocol show test case
// fragment with all its parent fragments, depending on the isFrag parameter
func getTabProtocolsShowTestCase(w http.ResponseWriter, r *http.Request, t string, c string, s string, p project.Project) {
	tmpl := GetTabProtocolsShowTestCaseTree()
	if isFragmentRequest(r) {
		tmpl = GetTabProtocolsShowTestCaseFragment()
	}

	ctx := context.New().
		WithUserInformation(r).
		With("SelectedTestCase", c).
		With("SelectedTestSequence", s).
		With("SelectedType", t).
		With("Project", p)
	if err := tmpl.Execute(w, ctx); err != nil {
		log.Printf("'Failed to show protocol' error on template parsing: %s", err)
	}
}

// GetTabProtocolsShowTestCaseFragment returns only the protocol show test case tab template
func GetTabProtocolsShowTestCaseFragment() *template.Template {
	return getBaseTree().
		Append("tab-protocols-testcase").
		Get().Lookup("tab-content")
}

// GetTabProtocolsShowTestCaseTree returns the protocol show test case tab template with all parent templates
func GetTabProtocolsShowTestCaseTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab test sequence edit tree
		Append("tab-protocols-testcase").
		Get().Lookup("header")
}

// getTabProtocolsShowTestSequence executes either the protocol show test sequence fragment only or the protocol show
// test sequence fragment with all its parent fragments, depending on the isFrag parameter
func getTabProtocolsShowTestSequence(w http.ResponseWriter, r *http.Request, t string, c string, s string, p project.Project) {
	tmpl := GetTabProtocolsShowTestSequenceTree()
	if isFragmentRequest(r) {
		tmpl = GetTabProtocolsShowTestSequenceFragment()
	}

	ctx := context.New().
		WithUserInformation(r).
		With("SelectedTestCase", c).
		With("SelectedTestSequence", s).
		With("SelectedType", t).
		With("Project", p)
	if err := tmpl.Execute(w, ctx); err != nil {
		log.Printf("'Failed to show protocol' error on template parsing: %s", err)
	}
}

// GetTabProtocolsShowTestCaseFragment returns only the protocol show test sequence tab template
func GetTabProtocolsShowTestSequenceFragment() *template.Template {
	return getBaseTree().
		Append("tab-protocols-testsequence").
		Get().Lookup("tab-content")
}

// GetTabProtocolsShowTestSequenceTree returns the protocol show test sequence tab template with all parent templates
func GetTabProtocolsShowTestSequenceTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab test sequence edit tree
		Append("tab-protocols-testsequence").
		Get().Lookup("header")
}
