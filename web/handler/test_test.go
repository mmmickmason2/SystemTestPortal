/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

// test_test.go contains all test code that is similar for test cases and test sequences

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"time"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	testStore "gitlab.com/stp-team/systemtestportal/store/test"
	"gitlab.com/stp-team/systemtestportal/web/action"
	"gitlab.com/stp-team/systemtestportal/web/slugs"
)

// TestTestHandlerBadRequest checks if unsupported requests are handled correctly. The unsupported request is
// an OPTION request
func testBadRequest(t *testing.T, url string) {
	req, err := http.NewRequest(http.MethodOptions, url, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()

	hut := http.HandlerFunc(ProjectHandler)
	hut.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusMethodNotAllowed {
		t.Errorf("%s returned wrong status code: got %v want %v", url, status, http.StatusMethodNotAllowed)
	}
}

func testGetRequestTest(t *testing.T, testType, testName string) {
	p := dummyProject

	urls := []struct {
		url        string
		statusWant int
	}{
		{"/user/" + p.ID + "/" + testType + "/", http.StatusOK},
		{"/user/" + p.ID + "/" + testType + "/" + testName, http.StatusOK},
		{"/user/" + p.ID + "/" + testType + "/----", http.StatusNotFound},
		{"/user/" + p.ID + "/" + testType + "/" + action.New, http.StatusOK},
		{"/user/" + p.ID + "/" + testType + "/" + testName + "/" + action.Edit, http.StatusOK},
		{"/user/" + p.ID + "/" + testType + "/NO_TEST_ID/" + action.Edit, http.StatusNotFound},
		{"/user/" + p.ID + "/" + testType + "/" + testName + "/" + action.History, http.StatusOK},
	}
	for _, caseURL := range urls {
		getRequest(t, caseURL.url, caseURL.statusWant, false, ProjectHandler)
		getRequest(t, caseURL.url, caseURL.statusWant, true, ProjectHandler)
	}
}

// getRequest creates a GET request to the given path and passes it to the given handler. Depending on the isFrag
// parameter it creates a fragment request and passes the request to the given handler
func getRequest(t *testing.T, path string, statusWant int, isFrag bool, handler http.HandlerFunc) {

	req, err := http.NewRequest(http.MethodGet, path, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	hut := http.HandlerFunc(handler)

	if isFrag {
		form := url.Values{}
		form.Add("fragment", "true")
		req.Form = form
	}

	hut.ServeHTTP(rr, req)

	if status := rr.Code; status != statusWant {
		t.Errorf("%s returned wrong status code: got %v want %v", path, status, statusWant)
	}
}

func testTestDeleteRequest(p *project.Project, testType, name string, statusWant int) func(*testing.T) {
	return func(t *testing.T) {
		testURL := "/user/" + p.ID + "/" + testType + "/" + name + "/"

		req, err := http.NewRequest(http.MethodDelete, testURL, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		hut := http.HandlerFunc(ProjectHandler)
		hut.ServeHTTP(rr, req)

		if status := rr.Code; status != statusWant {
			t.Errorf("%s returned wrong status code: got %v want %v", testURL, status, statusWant)
		}

		switch testType {
		case TestCases:
			if _, ok, _ := testStore.GetCaseStore().Get("user", p.ID, slugs.Slugify(name)); ok {
				t.Errorf("test case %s was not deleted", name)
			}
		case TestSequences:
			if _, ok, _ := testStore.GetSequenceStore().Get("user", p.ID, slugs.Slugify(name)); ok {
				t.Errorf("test sequence %s was not deleted", name)
			}
		}
	}
}

// testPostRequestEmpty tests an empty post request with the given url
// Useful for testing post requests on wrong urls
func testPostRequestEmpty(t *testing.T, url string, statusWant int) {

	req, err := http.NewRequest(http.MethodPost, url, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	hut := http.HandlerFunc(ProjectHandler)

	hut.ServeHTTP(rr, req)

	if status := rr.Code; status != statusWant {
		t.Errorf("%s returned wrong status code: got %v want %v", url, status, statusWant)
	}

}

func TestTimeFormatSince(t *testing.T) {
	tme := []struct {
		in   time.Duration
		want string
	}{
		{time.Since(time.Now()), "1 Minute ago"},
		{time.Since(time.Now().Add(-time.Minute * 2)), "2 Minutes ago"},
		{time.Since(time.Now().Add(-time.Hour)), "1 Hour ago"},
		{time.Since(time.Now().Add(-time.Hour * 2)), "2 Hours ago"},
		{time.Since(time.Now().AddDate(0, 0, -1)), "1 Day ago"},
		{time.Since(time.Now().AddDate(0, 0, -2)), "2 Days ago"},
		{time.Since(time.Now().AddDate(0, 0, -7)), "1 Week ago"},
		{time.Since(time.Now().AddDate(0, 0, -14)), "2 Weeks ago"},
		{time.Since(time.Now().AddDate(0, -1, 0)), "1 Month ago"},
		{time.Since(time.Now().AddDate(0, -2, 0)), "2 Months ago"},
		{time.Since(time.Now().AddDate(-1, 0, 0)), "1 Year ago"},
		{time.Since(time.Now().AddDate(-2, 0, 0)), "2 Years ago"},
	}

	for _, ti := range tme {
		got := formatTimeSince(ti.in)

		if got != ti.want {
			t.Errorf("formatTimeSince(%d) = %s, want %s", ti.in, got, ti.want)
		}
	}
}
