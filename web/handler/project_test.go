/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"time"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/visibility"
	projectStore "gitlab.com/stp-team/systemtestportal/store/project"
	"gitlab.com/stp-team/systemtestportal/web/slugs"
)

var dummyProject = &project.Project{
	ID:          "project-1",
	Name:        "Project 1",
	Description: "The first example project",
	Visibility:  visibility.Public,
	SUTVersions: []string{
		"1.0.1", "1.0.2", "2.0.1", "3.0.5",
	},
	CreationDate: time.Now().Round(time.Second),
}

// TestSavingProject tests the saving of a project with various values
func TestSavingProject(t *testing.T) {
	ps := []struct {
		name       string
		visibility string
		statusWant int
		okWant     bool
	}{
		{"TestProject", "public", http.StatusOK, true},
		{"", "internal", http.StatusBadRequest, false},
		{"?", "private", http.StatusBadRequest, false},
		{"TEST PROJECT 900", "vegetable", http.StatusBadRequest, false},
		{"Project 1", "private", http.StatusConflict, true},
	}
	for _, p := range ps {
		testSavingProject(t, p.name, p.visibility, p.statusWant, p.okWant)
	}
}

func testSavingProject(t *testing.T, name, visibility string, statusWant int, okWant bool) {

	form := url.Values{}
	form.Add("inputProjectName", name)
	form.Add("inputProjectDescription", "")
	form.Add("optionsProjectVisibility", visibility)

	req, err := http.NewRequest(http.MethodPost, "/user/projects/save", nil)
	if err != nil {
		t.Fatal(err)
	}

	req.Form = form

	rr := httptest.NewRecorder()
	hut := http.HandlerFunc(ProjectHandler)

	hut.ServeHTTP(rr, req)

	if status := rr.Code; status != statusWant {
		t.Errorf("hut returned wrong status code: got %v want %v",
			status, statusWant)
	}

	if _, ok, _ := projectStore.GetStore().Get("user", slugs.Slugify(name)); ok != okWant {
		t.Errorf("project %s was not created", name)
	}
}

// TestProjectHandlerGetRequest tests the GET request on various urls and checks the response status
func TestProjectHandlerGetRequest(t *testing.T) {
	urls := []struct {
		url        string
		statusWant int
	}{
		{"/user/projects/new", http.StatusOK},
		{"/user/projects/", http.StatusNotFound},
		{"/user/projects/    ", http.StatusNotFound},
		{"/user/" + dummyProject.ID, http.StatusFound},
		{"/user/" + dummyProject.ID + "/", http.StatusSeeOther},
		{"/user/" + dummyProject.ID + "/vegetable", http.StatusNotFound},
		{"/not/" + dummyProject.ID, http.StatusOK},
		{"/user/not-a-project/", http.StatusNotFound},
		{"/user/not-a-project/", http.StatusNotFound},
	}

	for _, pURL := range urls {
		getRequest(t, pURL.url, pURL.statusWant, false, ProjectHandler)
		getRequest(t, pURL.url, pURL.statusWant, true, ProjectHandler)
	}
}

// TestNewProjectWithBadRequest tests the project creation with an unsupported request method
func TestNewProjectWithBadRequest(t *testing.T) {
	testBadRequest(t, "/user/projects/new")
}

// TestProjectPostRequestInvalidURL tests a post request on an invalid url
func TestProjectPostRequestInvalidURL(t *testing.T) {
	testPostRequestEmpty(t, "/user/projects/saves", http.StatusNotFound)
}
