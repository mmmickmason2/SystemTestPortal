/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"log"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/test"
	testStore "gitlab.com/stp-team/systemtestportal/store/test"
	"gitlab.com/stp-team/systemtestportal/web/action"
	"gitlab.com/stp-team/systemtestportal/web/errors"
	"gitlab.com/stp-team/systemtestportal/web/slugs"
)

// handleTestStepActionGet processes the GET request
func handleTestStepActionGet(w http.ResponseWriter, r *http.Request, path string, tc test.Case) {
	switch path {
	case action.Show:
		ListTestStepsHandler(w, r, tc)
	case action.New:
		NewTestStepHandler(w, r, tc)
	default:
		pathSegments := strings.Split(path, "/")

		s, err := getTestStepFromPath(r, tc, w)
		if err != nil {
			return
		}

		if len(pathSegments) == 1 {
			ShowTestStepHandler(w, r, tc, s)
		}
		if len(pathSegments) > 1 && pathSegments[1] == action.Edit {
			EditTestStepHandler(w, r, tc, s)
		}
	}
}

// handleTestStepActionPost processes the POST request
func handleTestStepActionPost(w http.ResponseWriter, r *http.Request, path string, tc test.Case, p project.Project) {
	if path == TestSteps+"/"+action.Save {
		SaveNewTestStepHandler(w, r, tc, p)
	}
}

// handleTestStepActionPut processes the PUT request
func handleTestStepActionPut(w http.ResponseWriter, r *http.Request, path string, tc test.Case, p project.Project) {
	path = strings.TrimPrefix(path, TestSteps+"/")
	segments := strings.Split(path, "/")
	if len(segments) > 1 {
		if segments[1] == action.Update {
			s, err := getTestStepFromPath(r, tc, w)
			if err != nil {
				return
			}
			UpdateTestStepHandler(w, r, tc, s, p)
		}
	}
}

// handleTestStepActionDelete processes the DELETE request
func handleTestStepActionDelete(w http.ResponseWriter, r *http.Request, tc test.Case,
	p project.Project) {

	s, err := getTestStepFromPath(r, tc, w)
	if err != nil {
		return
	}
	DeleteTestStepHandler(w, r, tc, s, p)
}

// ListTestStepsHandler displays the complete page with a list containing all test steps of a test case
func ListTestStepsHandler(w http.ResponseWriter, r *http.Request, tc test.Case) {
	// TODO: Add template execution
	log.Println("Listing Test Steps")
}

// NewTestStepHandler displays the complete page with the test step creation form in it
func NewTestStepHandler(w http.ResponseWriter, r *http.Request, tc test.Case) {
	// TODO: Add template execution
	log.Println("Displaying test step creation dialog")
}

// ShowTestStepHandler displays the complete page containing a detail view of a test step
func ShowTestStepHandler(w http.ResponseWriter, r *http.Request, tc test.Case, s test.Step) {
	// TODO: Add template execution
	log.Println("Showing specific test step")
}

// EditTestStepHandler displays the complete page containing the test step editor
func EditTestStepHandler(w http.ResponseWriter, r *http.Request, tc test.Case, s test.Step) {
	// TODO: Add template execution
	log.Printf("Editing test step %v now", s.ID)
}

// SaveNewTestStepHandler creates a test step from given input and displays a list with all test steps
// if successful
func SaveNewTestStepHandler(w http.ResponseWriter, r *http.Request, tc test.Case, p project.Project) {
	action := r.FormValue("inputTestStepAction")
	expect := r.FormValue("inputTestStepExpected")

	s := test.NewTestStep(action, expect)

	if vErr := validateTestStep(s); vErr != nil {
		HandleError(vErr, w)
		return
	}

	s.ID = len(tc.TestCaseVersions[0].Steps) + 1
	tc.TestCaseVersions[0].Steps = append(tc.TestCaseVersions[0].Steps, s)
	err := testStore.GetCaseStore().Add("user", p.ID, &tc)

	if err != nil {
		HandleError(err, w)
		return
	}

	w.Header().Set("newName", slugs.Slugify(tc.Name))
	w.WriteHeader(http.StatusCreated)

	ListTestStepsHandler(w, r, tc)
}

// UpdateTestStepHandler updates one test step with input values from request and
// saves the changes if the input is valid
func UpdateTestStepHandler(w http.ResponseWriter, r *http.Request, tc test.Case, s test.Step, p project.Project) {
	sa := r.FormValue("inputTestStepAction")
	se := r.FormValue("inputTestStepExpected")

	us := test.NewTestStep(sa, se)
	us.ID = s.ID

	if vErr := validateTestStep(us); vErr != nil {
		HandleError(vErr, w)
		return
	}
	utc, _, _ := testStore.GetCaseStore().Get("user", p.ID, tc.ID)
	utc.TestCaseVersions[0].Steps[s.ID-1] = us
	w.WriteHeader(http.StatusCreated)
	ShowTestStepHandler(w, r, tc, us)
}

// DeleteTestStepHandler deletes one test step of a test case from the system
func DeleteTestStepHandler(w http.ResponseWriter, r *http.Request, tc test.Case, s test.Step, p project.Project) {
	i := s.ID - 1
	// take all elements before s and append every element after s to it
	st := tc.TestCaseVersions[0].Steps
	tc.TestCaseVersions[0].Steps = append(st[:i], st[i+1:]...)
	// update ID of s' successors
	for j, v := range tc.TestCaseVersions[0].Steps {
		if j >= i {
			v.ID--
		}
	}

	err := testStore.GetCaseStore().Add("user", p.ID, &tc)
	if err != nil {
		HandleError(err, w)
		return
	}
}

// getTestStepFromPath returns the test step associated to the url if existing
func getTestStepFromPath(r *http.Request, tc test.Case, w http.ResponseWriter) (test.Step, error) {
	sID := strings.Split(r.URL.EscapedPath(), "/")[6]
	i, err := strconv.Atoi(sID)
	s := test.Step{ID: -1}
	if err != nil {
		log.Printf("Test step '%v' does not exist", sID)
		w.WriteHeader(http.StatusNotFound)
		return s, InvalidID()
	}
	s = tc.TestCaseVersions[0].Steps[i-1]
	return s, nil
}

// checks if the given test step has acceptable values
func validateTestStep(s test.Step) error {
	if s.Action == "" {
		return errors.ConstructWithStackTrace(http.StatusBadRequest,
			"Client send an empty test step action.").
			WithBodyModal("Invalid action", "The given description of an action was empty").
			Finish()
	}

	if s.ExpectedResult == "" {
		return errors.ConstructWithStackTrace(http.StatusBadRequest,
			"Client send an empty string as "+
				"value for  a test step result.").
			WithBodyModal("Invalid expected result", "The given description of an result was empty").
			Finish()
	}
	return nil
}

func computeTestStepPath(w http.ResponseWriter, r *http.Request, p project.Project, path string) (
	string, bool, test.Case) {

	tc, _, err := getTestObjectFromPath(w, r, p)
	if err != nil {
		return "", false, test.Case{}
	}
	path = strings.TrimPrefix(path, tc.ID+"/")
	pathSegments := strings.Split(path, "/")

	if len(pathSegments) < 1 {
		return "", false, test.Case{}
	}

	if len(pathSegments) >= 2 {
		return path, true, tc
	}
	return "", false, test.Case{}
}
