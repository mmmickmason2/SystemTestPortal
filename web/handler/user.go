/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"

	"fmt"

	"html/template"

	"gitlab.com/stp-team/systemtestportal/domain/user"
	"gitlab.com/stp-team/systemtestportal/web/context"
	"gitlab.com/stp-team/systemtestportal/web/errors"
)

// SessionHandler is used to start sessions for a user and end them.
type SessionHandler interface {
	// StartFor starts the session for given user.
	StartFor(w http.ResponseWriter, r *http.Request, u *user.User) error
	// EndFor ends the session for given user.
	EndFor(w http.ResponseWriter, r *http.Request, u *user.User) error
	// GetCurrent gets the user that hold the session. If there is no
	// user session the returned user will be nil.
	GetCurrent(r *http.Request) (*user.User, error)
}

// UserAuth is used to authenticate users.
type UserAuth interface {
	// Validate validates user credentials and returns the corresponding user.
	Validate(identifier string, password string) (*user.User, bool, error)
}

//UserServer is used to register a new user
type UserServer interface {
	Get(name string) (*user.User, bool)
	Add(u *user.User) error
}

// login is the handler for login requests from the client.
type login struct {
	Session SessionHandler
	Auth    UserAuth
}

// Error messages
const (
	failedLogin          = "Login failed."
	unableToStartSession = "We were unable to start your session. This is our fault. " +
		"We don't know how this happened and are terribly sorry :|"
	unableToAuthenticate = "We were unable to check your " +
		"credentials. We don't know how this happened and are terribly sorry :|"
	unableToCheckSession = "We were unable to check the current session status. " +
		"The cookie monster probably stole your cookies :o"
	unableToEndSession = "We were unable to end your session. This is our fault. " +
		"We don't know how this happened and are terribly sorry :|"
	failedLogout   = "Logout failed."
	noLoggedInUser = "We don't know how this happened, but it seems you tried to logout " +
		"without being logged in. So don't worry you are logged out already ;)"
	alreadyLoggedIn = "We don't know how you got here, but your tried to login while you " +
		"are already logged in. If you want to log in with a different account please logout first ;)"
	givenCredentialsAreWrong = "The given user doesn't exist or the " +
		"given password doesn't match the username."
	failedRegister          = "Registration failed"
	unableToRegisterNewUser = "We were unable to register you. This is our fault. " +
		"We don't know how this happened and are terribly sorry :|"
	accountNameAlreadyExists = "We were unable to register you. Your chosen username is already assigned."
	emailAlreadyInUse        = "We were unable to register you. Your chosen email address is already in use."
)

// ServeHTTP Handles login requests from the client.
func (l *login) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		if err := l.loginPossible(r); err != nil {
			HandleError(err, w)
			return
		}
		identifier := r.FormValue("inputIdentifier")
		password := r.FormValue("inputPassword")

		if u, err := l.login(w, r, identifier, password); err != nil {
			HandleError(err, w)
		} else {
			w.WriteHeader(http.StatusOK)
			w.Header().Set("dashboard", u.AccountName)
		}
	} else {
		errors.WrongRequestMethod(w, r)
	}
}

// login tries to login the user with given credentials.
// If its successful the logged in user is returned
// if not an error is returned that can be written to the response.
func (l *login) login(w http.ResponseWriter, r *http.Request,
	identifier string, password string) (*user.User, error) {
	u, ok, authErr := l.Auth.Validate(identifier, password)

	if authErr != nil {
		return nil, errors.
			ConstructWithStackTrace(
				http.StatusInternalServerError,
				fmt.Sprintf("Authenification error occured for user %+v", u),
			).
			WithBodyModal(failedLogin, unableToAuthenticate).
			WithCause(authErr).Finish()
	}

	if !ok {
		return nil, errors.
			Construct(http.StatusUnauthorized).
			WithBody(givenCredentialsAreWrong).
			Finish()
	}

	err := l.Session.StartFor(w, r, u)
	if err != nil {
		return nil, errors.
			ConstructWithStackTrace(
				http.StatusInternalServerError,
				fmt.Sprintf("Authenification error occured for user %+v", u),
			).
			WithBodyModal(failedLogin, unableToStartSession).
			WithCause(err).Finish()
	}
	return u, nil
}

// loginPossible checks whether a login is possible.
// This doesn't include checking the users credentials.
// If login is not possible it returns an error which can be
// written to the response.
func (l *login) loginPossible(r *http.Request) error {
	su, err := l.Session.GetCurrent(r)
	if err != nil {
		return errors.
			ConstructWithStackTrace(
				http.StatusInternalServerError,
				"Unable to get current session!",
			).
			WithBodyModal(failedLogin, unableToCheckSession).
			WithCause(err).Finish()
	}
	if su != nil {
		return errors.
			ConstructWithStackTrace(
				http.StatusBadRequest,
				"User tried to login while there is already a logged in user!").
			WithBody(alreadyLoggedIn).Finish()
	}
	return nil
}

// NewLoginHandler creates a new login handler that handles login requests from the client.
func NewLoginHandler(handler SessionHandler, auth UserAuth) http.Handler {
	return &login{handler, auth}
}

// logout is the handler for logout requests from the client.
type logout struct {
	Session SessionHandler
}

// ServeHTTP Handles logout requests from the client.
func (l *logout) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		errors.WrongRequestMethod(w, r)
		return
	}
	u, err := l.logoutPossible(r)
	if err != nil {
		HandleError(err, w)
		return
	}
	if endErr := l.Session.EndFor(w, r, u); endErr != nil {
		HandleError(errors.
			ConstructWithStackTrace(
				http.StatusInternalServerError,
				fmt.Sprintf("Session can't be ended for user %q.", u.ID),
			).
			WithBodyModal(failedLogout, unableToEndSession).
			WithCause(endErr).Finish(), w)
	}
}

// logoutPossible checks whether a logout is possible or not.
// If not an error is returned else the currently logged in user
// is returned.
func (l *logout) logoutPossible(r *http.Request) (*user.User, error) {
	u, getErr := l.Session.GetCurrent(r)
	if getErr != nil {
		return nil, errors.
			ConstructWithStackTrace(
				http.StatusInternalServerError,
				"Unable to get currently logged in user!",
			).
			WithBodyModal(failedLogout, unableToCheckSession).
			WithCause(getErr).Finish()
	}
	if u == nil {
		return nil, errors.
			ConstructWithStackTrace(
				http.StatusBadRequest,
				"Tried to logout while no user is logged in!",
			).
			WithBodyModal(failedLogout, noLoggedInUser).Finish()
	}
	return u, nil
}

// NewLogoutHandler creates a new logout handler that handles logout requests from the client.
func NewLogoutHandler(handler SessionHandler) http.Handler {
	return &logout{handler}
}

// register is the handler for register requests from the client.
type register struct {
	server UserServer
}

// NewRegisterHandler creates a new logout handler that handles logout requests from the client.
func NewRegisterHandler(server UserServer) http.Handler {
	return &register{server}
}

// ServeHTTP handles register requests from the client.
func (reg *register) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		display := r.FormValue("inputDisplayName")
		account := r.FormValue("inputAccountName")
		email := r.FormValue("inputEmail")
		password := r.FormValue("inputPassword")

		u, err := reg.register(account, email, display, password)

		if err != nil {
			HandleError(err, w)
			return
		}

		w.Header().Add("newAccount", u.ID)
		w.WriteHeader(http.StatusCreated)
	case http.MethodGet:
		tmpl := GetNewUserTree()
		if err := tmpl.Execute(w, context.New().WithUserInformation(r)); err != nil {
			fmt.Println(err)
		}
	default:
		errors.WrongRequestMethod(w, r)
	}
}

// register tries to register a user with given data. If the registration fails
// an error is returned and the returned user is nil otherwise the error is nil
// and the user is the freshly registered user.
func (reg *register) register(account, email, display, password string) (*user.User, error) {
	if u, exists := reg.server.Get(account); exists && u.AccountName == account {
		return nil, errors.Construct(http.StatusConflict).
			WithBodyModal(failedRegister, accountNameAlreadyExists).
			Finish()
	}

	if u, exists := reg.server.Get(email); exists && u.EMail == email {
		return nil, errors.Construct(http.StatusConflict).
			WithBodyModal(failedRegister, emailAlreadyInUse).
			Finish()
	}

	newUser := user.New(display, account, email, password)
	err := reg.server.Add(&newUser)
	if err != nil {
		return nil, errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Failed to add user to the persistence.").
			WithBodyModal(failedRegister, unableToRegisterNewUser).
			Finish()
	}
	return &newUser, nil
}

// GetNewUserTree returns the register template with all parent templates
func GetNewUserTree() *template.Template {
	return getNoSideBarTree().
		// New user tree
		Append("new-account").
		Get().Lookup("header")
}
