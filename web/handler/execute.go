/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"

	"html/template"
	"log"

	"time"

	"io"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/test"
	"gitlab.com/stp-team/systemtestportal/store/execution"
	"gitlab.com/stp-team/systemtestportal/web/action"
	"gitlab.com/stp-team/systemtestportal/web/context"
	"gitlab.com/stp-team/systemtestportal/web/errors"
	"gitlab.com/stp-team/systemtestportal/web/sessions"
)

const keyStepNr = "step"
const keyCaseNr = "case"
const keyAllSubObjects = "SubObjects"
const keySUTVersion = "inputSUTVersion"
const keyEnvironment = "inputEnvironment"
const keyProject = "Project"
const keyTestObject = "TestObject"
const keyTestObjectVersion = "TestObjectVersion"
const keyObservedBehavior = "inputTestStepActualResult"
const keyComment = "notes"
const keyResult = "result"
const keyAction = "Action"
const keyExpectedResult = "ExpectedResult"
const keyMinutes = "minutes"
const keySeconds = "seconds"
const keyEstimatedMinutes = "EstimatedMin"
const keyEstimatedHours = "EstimatedHours"
const keyProgress = "ProgressBarCurrent"
const keyProgressPercent = "ProgressBarPercent"
const keyProgressTotal = "ProgressBarTotal"
const keyIsSeq = "isSequence"
const keyIsInSeq = "isInSequence"

const failedSave = "Saving input failed"
const unableToSaveStep = "We were unable to save the results for this step. This is our fault. " +
	"We don't know how this happened and are terribly sorry :|"
const unableToSaveStartPage = "We were unable to save the information for this test case. This is our fault. " +
	"We don't know how this happened and are terribly sorry :|"
const unableToSaveSummaryPage = "We were unable to save the information for this test case. This is our fault. " +
	"We don't know how this happened and are terribly sorry :|"
const unableToLoadSummaryPage = "We were unable to load the information for the summary of this test case. " +
	"This is our fault. We don't know how this happened and are terribly sorry :|"
const unableToLoadStepPage = "We were unable to load the information for the next test step. This is our fault. " +
	"We don't know how this happened and are terribly sorry :|"

func handleTestCaseExecutionPost(w http.ResponseWriter, r *http.Request, p project.Project, tc test.Case) {
	testCaseVersionNr := getTestCaseVersionToShowOrEdit(w, r, tc, action.Execute)
	if testCaseVersionNr <= 0 {
		return
	}
	tcv := tc.TestCaseVersions[len(tc.TestCaseVersions)-testCaseVersionNr]
	stepNr := getFormValueInt(r, keyStepNr)
	var err error

	switch {
	case stepNr < 0:
		//invalid, negative stepNr
		errors.BadRequest(w)
	case stepNr == 0:
		//start page
		err = saveStartPageCase(w, r, p, tc, testCaseVersionNr)
		if err == nil {
			err = sendStepPage(w, r, p, tc, tcv, 0, 1, 2, len(tcv.Steps)+2)
		}
	case stepNr == len(tcv.Steps):
		//last step page
		err = saveStepPage(w, r, stepNr)
		if err == nil {
			err = sendSummaryPageCase(w, r, p, tc, tcv, 0, len(tcv.Steps)+2, len(tcv.Steps)+2, false)
		}
	case stepNr == len(tcv.Steps)+1:
		//summary page
		_, err := saveSummaryPage(w, r)
		if err == nil {
			http.Redirect(w, r, ".?fragment=true", http.StatusSeeOther)
		}

	case stepNr > len(tcv.Steps)+1:
		//invalid, stepNr out of range
		errors.BadRequest(w)
	default:
		//step page
		err = saveStepPage(w, r, stepNr)
		if err == nil {
			err = sendStepPage(w, r, p, tc, tcv, 0, stepNr+1, stepNr+2, len(tcv.Steps)+2)
		}
	}
	if err != nil {
		HandleError(err, w)
	}
}
func handleTestSequenceExecutionPost(w http.ResponseWriter, r *http.Request, p project.Project, ts test.Sequence) {
	testSequenceVersionNr := getTestSequenceVersionToShowOrEdit(w, r, ts, action.Execute)
	if testSequenceVersionNr <= 0 {
		return
	}
	tsv := ts.SequenceVersions[len(ts.SequenceVersions)-testSequenceVersionNr]
	caseNr := getFormValueInt(r, keyCaseNr)
	stepNr := getFormValueInt(r, keyStepNr)
	if caseNr < 0 || stepNr < 0 {
		errors.BadRequest(w)
		return
	}
	caseCount := len(tsv.Cases)
	var c test.Case
	var nextCase test.Case
	if caseNr > 0 {
		c = tsv.Cases[caseNr-1]
	}
	if caseNr < caseCount {
		nextCase = tsv.Cases[caseNr]
	}

	stepCount := 0
	if caseNr > 0 {
		stepCount = len(c.TestCaseVersions[0].Steps)
	}

	var err error

	switch {
	case caseNr == 0 && stepNr == 0:
		//sequence start page
		err = saveStartPageSeq(w, r, p, ts, testSequenceVersionNr)
		if err == nil {
			sendStartPageCase(w, r, p, nextCase, nextCase.TestCaseVersions[0], 1, 2, totalWorkSteps(tsv), true)
		}
	case caseNr > 0 && stepNr == 0:
		//case start page
		err = saveStartPageCase(w, r, p, c, len(c.TestCaseVersions))
		if err == nil {
			err = sendStepPage(w, r, p, c, c.TestCaseVersions[0], caseNr, 1,
				totalWorkStepsUpTo(tsv, caseNr-1)+2, totalWorkSteps(tsv))
		}
	case caseNr > 0 && stepNr > 0 && stepNr < stepCount:
		//Step
		err = saveStepPage(w, r, stepNr)
		if err == nil {
			err = sendStepPage(w, r, p, c, c.TestCaseVersions[0], caseNr, stepNr+1,
				totalWorkStepsUpTo(tsv, caseNr-1)+stepNr+2, totalWorkSteps(tsv))
		}
	case caseNr > 0 && stepNr == stepCount:
		//Last Step
		err = saveStepPage(w, r, stepNr)
		if err == nil {
			err = sendSummaryPageCase(w, r, p, c, c.TestCaseVersions[0], caseNr,
				totalWorkStepsUpTo(tsv, caseNr-1)+stepNr+2, totalWorkSteps(tsv), true)
		}
	case caseNr > 0 && caseNr < caseCount && stepNr == stepCount+1:
		//Summary page of middle case
		var id int
		id, err = saveSummaryPage(w, r)
		if err == nil {
			err = addCaseRecordToSequenceRecord(w, r, id, c.ID, p.ID)
		}
		if err == nil {
			sendStartPageCase(w, r, p, nextCase, nextCase.TestCaseVersions[0], caseNr+1,
				totalWorkStepsUpTo(tsv, caseNr-1)+stepNr+2, totalWorkSteps(tsv), true)
		}
	case caseNr == caseCount && stepNr == stepCount+1:
		//Summary page of last case
		var id int
		id, err = saveSummaryPage(w, r)
		if err == nil {
			err = addCaseRecordToSequenceRecord(w, r, id, c.ID, p.ID)
		}
		if err == nil {
			err = sendSummaryPageSeq(w, r, p, ts, tsv)
		}
		if err == nil {
			_, err = saveSequenceRecord(w, r)
		}

	case caseNr == 0 && stepNr == 1:
		//Sequence summary page
		http.Redirect(w, r, ".?fragment=true", http.StatusSeeOther)
	default:
		errors.BadRequest(w)
	}
	if err != nil {
		HandleError(err, w)
	}
}

func handleTestCaseExecutionGet(w http.ResponseWriter, r *http.Request, p project.Project, tc test.Case) {
	testCaseVersionNr := getTestCaseVersionToShowOrEdit(w, r, tc, action.Execute)
	tcv := tc.TestCaseVersions[len(tc.TestCaseVersions)-testCaseVersionNr]

	sendStartPageCase(w, r, p, tc, tcv, 0, 1, len(tcv.Steps)+2, false)
}
func handleTestSequenceExecutionGet(w http.ResponseWriter, r *http.Request, p project.Project, ts test.Sequence) {
	testSequenceVersionNr := getTestSequenceVersionToShowOrEdit(w, r, ts, action.Execute)
	tsv := ts.SequenceVersions[len(ts.SequenceVersions)-testSequenceVersionNr]

	sendStartPageSeq(w, r, p, ts, tsv)
}

func addCaseRecordToSequenceRecord(w http.ResponseWriter, r *http.Request,
	caseRecordID int, testCaseID string, projectID string) error {
	seqRec, err := sessions.GetSessionStore().GetCurrentSequenceRecord(r)
	if seqRec == nil {
		return err
	}
	seqRec.AddCaseExecutionRecord(caseRecordID, testCaseID, projectID)
	err = sessions.GetSessionStore().SetCurrentSequenceRecord(w, r, seqRec)
	return err
}

func sendStartPageSeq(w io.Writer, r *http.Request, p project.Project, ts test.Sequence,
	tsv test.SequenceVersion) {
	tmpl := getProjectTabPageByName(r, "tab-execute-startpage")
	min, hours := tsv.ItemEstimatedDuration()

	progressTotal := totalWorkSteps(tsv)
	progressPercent := int(1 / float64(progressTotal) * 100.0)

	ctx := context.New().
		WithUserInformation(r).
		With(keyIsSeq, true).
		With(keyIsInSeq, true).
		With(keyCaseNr, 0).
		With(keyProject, p).
		With(keyTestObject, ts).
		With(keyTestObjectVersion, tsv).
		With(keyProgress, 1).
		With(keyProgressTotal, progressTotal).
		With(keyProgressPercent, progressPercent).
		With(keyEstimatedMinutes, min).
		With(keyEstimatedHours, hours)
	if err := tmpl.Execute(w, ctx); err != nil {
		log.Printf("Failed to show execution error on template parsing: %s", err)
	}

}

func sendStartPageCase(w io.Writer, r *http.Request, p project.Project, tc test.Case, tcv test.CaseVersion,
	caseNr int, progress int, progressTotal int, isInSequence bool) {
	tmpl := getProjectTabPageByName(r, "tab-execute-startpage")
	seqRec, _ := sessions.GetSessionStore().GetCurrentSequenceRecord(r)
	var sutVersion, environment string
	if seqRec == nil {
		sutVersion = ""
		environment = ""
	} else {
		sutVersion = seqRec.SUTVersion
		environment = seqRec.Environment
	}
	progressPercent := int(float64(progress) / float64(progressTotal) * 100.0)

	ctx := context.New().
		WithUserInformation(r).
		With(keyIsSeq, false).
		With(keyIsInSeq, isInSequence).
		With(keyCaseNr, caseNr).
		With(keyProject, p).
		With(keyTestObject, tc).
		With(keyTestObjectVersion, tcv).
		With(keyProgress, progress).
		With(keyProgressTotal, progressTotal).
		With(keyProgressPercent, progressPercent).
		With(keySUTVersion, sutVersion).
		With(keyEnvironment, environment).
		With(keyEstimatedMinutes, tcv.DurationMin).
		With(keyEstimatedHours, tcv.DurationHours)
	if err := tmpl.Execute(w, ctx); err != nil {
		log.Printf("Failed to show execution error on template parsing: %s", err)
	}
}

func saveStartPageCase(w http.ResponseWriter, r *http.Request, p project.Project, tc test.Case, tcvNr int) error {

	sutVersion := getFormValueString(r, keySUTVersion)
	environment := getFormValueString(r, keyEnvironment)

	rec := test.NewCaseExecutionRecord(tc, tcvNr, p.ID, sutVersion, environment)
	err := sessions.GetSessionStore().SetCurrentCaseRecord(w, r, &rec)
	if err != nil {
		return errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to create and save current record.").
			WithBodyModal(failedSave, unableToSaveStartPage).Finish()
	}
	return nil
}
func saveStartPageSeq(w http.ResponseWriter, r *http.Request, p project.Project, ts test.Sequence, tcvNr int) error {

	sutVersion := getFormValueString(r, keySUTVersion)
	environment := getFormValueString(r, keyEnvironment)

	rec := test.NewSequenceExecutionRecord(ts, tcvNr, p.ID, sutVersion, environment)
	err := sessions.GetSessionStore().SetCurrentSequenceRecord(w, r, &rec)
	if err != nil {
		return errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to create and save current record.").
			WithBodyModal(failedSave, unableToSaveStartPage).Finish()
	}
	return nil
}
func sendStepPage(w io.Writer, r *http.Request, p project.Project, tc test.Case, tcv test.CaseVersion,
	caseNr int, stepNr int, progress int, progressTotal int) error {
	tmpl := getProjectTabPageByName(r, "tab-execute-step")
	rec, _ := sessions.GetSessionStore().GetCurrentCaseRecord(r)
	if rec == nil {
		return errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to get current record.").
			WithBodyModal(failedSave, unableToLoadStepPage).Finish()
	}
	//	time := rec.GetNeededTime().Round(time.Second)
	//	minutes := int(time.Minutes())
	//	seconds := int(time.Seconds()) - minutes * 60

	progressPercent := int(float64(progress) / float64(progressTotal) * 100.0)

	ctx := context.New().
		WithUserInformation(r).
		With(keyProject, p).
		With(keyTestObject, tc).
		With(keyTestObjectVersion, tcv).
		With(keyStepNr, stepNr).
		With(keyCaseNr, caseNr).
		With(keyProgress, progress).
		With(keyProgressTotal, progressTotal).
		With(keyProgressPercent, progressPercent).
		With(keyAction, tcv.Steps[stepNr-1].Action).
		With(keyExpectedResult, tcv.Steps[stepNr-1].ExpectedResult).
		With(keyMinutes, 0).
		With(keySeconds, 0)

	if err := tmpl.Execute(w, ctx); err != nil {
		log.Printf("Failed to show execution error on template parsing: %s", err)
	}
	return nil
}
func saveStepPage(w http.ResponseWriter, r *http.Request, stepNr int) error {
	observedBehavior := getFormValueString(r, keyObservedBehavior)
	comment := getFormValueString(r, keyComment)
	seconds := getFormValueInt(r, keySeconds)
	minutes := getFormValueInt(r, keyMinutes)
	neededTime := time.Duration(minutes)*time.Minute + time.Duration(seconds)*time.Second
	result, ok := getFormValueResult(r, keyResult)
	if !ok {
		return errors.ConstructWithStackTrace(http.StatusBadRequest, "Client send an invalid request.").
			WithBodyModal(failedSave, unableToSaveStep).
			Finish()
	}

	rec, _ := sessions.GetSessionStore().GetCurrentCaseRecord(r)
	if rec == nil {
		return errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to get current record.").
			WithBodyModal(failedSave, unableToSaveStep).Finish()
	}

	rec.SaveStep(stepNr, observedBehavior, result, comment, neededTime)
	err := sessions.GetSessionStore().SetCurrentCaseRecord(w, r, rec)
	if err != nil {
		return errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to save current record.").
			WithBodyModal(failedSave, unableToSaveStep).Finish()
	}
	return nil
}
func sendSummaryPageCase(w io.Writer, r *http.Request, p project.Project, tc test.Case, tcv test.CaseVersion,
	caseNr int, progress int, progressTotal int, isInSequence bool) error {
	tmpl := getProjectTabPageByName(r, "tab-execute-summary")
	rec, _ := sessions.GetSessionStore().GetCurrentCaseRecord(r)
	if rec == nil {
		return errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to get current record.").
			WithBodyModal(failedSave, unableToLoadSummaryPage).Finish()
	}
	//	time := rec.GetNeededTime().Round(time.Second)
	//	minutes := int(time.Minutes())
	//	seconds := int(time.Seconds()) - minutes * 60

	type stepSummary struct {
		Action           string
		Result           test.Result
		ObservedBehavior string
		Comment          string
	}
	var steps []stepSummary
	for i := 0; i < len(tcv.Steps); i++ {
		steps = append(steps, stepSummary{
			Action:           tcv.Steps[i].Action,
			Result:           rec.StepRecords[i].Result,
			ObservedBehavior: rec.StepRecords[i].ObservedBehavior,
			Comment:          rec.StepRecords[i].Comment,
		})
	}
	progressPercent := int(float64(progress) / float64(progressTotal) * 100.0)

	ctx := context.New().
		WithUserInformation(r).
		With(keyIsSeq, false).
		With(keyIsInSeq, isInSequence).
		With(keyProject, p).
		With(keyTestObject, tc).
		With(keyTestObjectVersion, tcv).
		With(keyAllSubObjects, steps).
		With(keyCaseNr, caseNr).
		With(keyStepNr, len(tcv.Steps)+1).
		With(keyProgress, progress).
		With(keyProgressTotal, progressTotal).
		With(keyProgressPercent, progressPercent).
		With(keyMinutes, 0).
		With(keySeconds, 0)
	if err := tmpl.Execute(w, ctx); err != nil {
		log.Printf("Failed to show execution error on template parsing: %s", err)
	}
	return nil
}
func sendSummaryPageSeq(w io.Writer, r *http.Request, p project.Project, ts test.Sequence,
	tsv test.SequenceVersion) error {
	tmpl := getProjectTabPageByName(r, "tab-execute-summary")
	seqRec, _ := sessions.GetSessionStore().GetCurrentSequenceRecord(r)
	if seqRec == nil {
		return errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to get current record.").
			WithBodyModal(failedSave, unableToLoadSummaryPage).Finish()
	}
	//	time := rec.GetNeededTime().Round(time.Second)
	//	minutes := int(time.Minutes())
	//	seconds := int(time.Seconds()) - minutes * 60

	type caseSummary struct {
		Result  test.Result
		Name    string
		Comment string
	}
	var cases []caseSummary
	for i := 0; i < len(tsv.Cases); i++ {
		caseRec, err := execution.GetRecordStore().GetCaseExecutionRecord(
			p.ID, tsv.Cases[i].ID, seqRec.CaseExecutionRecords[i].ID)
		if err != nil {
			return errors.ConstructWithStackTrace(http.StatusInternalServerError,
				"Error while trying to get record.").
				WithBodyModal(failedSave, unableToLoadSummaryPage).Finish()
		}
		cases = append(cases, caseSummary{
			Result:  caseRec.Result,
			Name:    tsv.Cases[i].Name,
			Comment: caseRec.Comment,
		})
	}
	ctx := context.New().
		WithUserInformation(r).
		With(keyIsSeq, true).
		With(keyIsInSeq, true).
		With(keyProject, p).
		With(keyTestObject, ts).
		With(keyTestObjectVersion, tsv).
		With(keyAllSubObjects, cases).
		With(keyCaseNr, 0).
		With(keyStepNr, 1).
		With(keyProgress, totalWorkSteps(tsv)).
		With(keyProgressTotal, totalWorkSteps(tsv)).
		With(keyProgressPercent, 100).
		With(keyMinutes, 0).
		With(keySeconds, 0)
	if err := tmpl.Execute(w, ctx); err != nil {
		log.Printf("Failed to show execution error on template parsing: %s", err)
	}
	return nil
}

func saveSummaryPage(w http.ResponseWriter, r *http.Request) (recordID int, err error) {
	comment := getFormValueString(r, keyComment)
	seconds := getFormValueInt(r, keySeconds)
	minutes := getFormValueInt(r, keyMinutes)
	neededTime := time.Duration(minutes)*time.Minute + time.Duration(seconds)*time.Second
	result, ok := getFormValueResult(r, keyResult)
	if !ok {
		return -1, errors.ConstructWithStackTrace(http.StatusBadRequest, "Client send an invalid request.").
			WithBodyModal(failedSave, unableToSaveStep).
			Finish()
	}

	//Get Current Record
	rec, _ := sessions.GetSessionStore().GetCurrentCaseRecord(r)
	if rec == nil {
		return -1, errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to get current record.").
			WithBodyModal(failedSave, unableToSaveSummaryPage).Finish()
	}

	//Save Data to record and record to store
	rec.Finish(result, comment, neededTime)
	recordID, err = execution.GetRecordStore().AddCaseRecord(rec)
	if err != nil {
		return -1, errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to save record into store.").
			WithBodyModal(failedSave, unableToSaveSummaryPage).Finish()
	}

	//Remove Record from Session
	err = sessions.GetSessionStore().RemoveCurrentCaseRecord(w, r)
	if err != nil {
		e := errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to save current record.").Finish()
		errors.Log(e)
	}
	return
}

func saveSequenceRecord(w http.ResponseWriter, r *http.Request) (recordID int, err error) {

	//Get Current Record
	rec, _ := sessions.GetSessionStore().GetCurrentSequenceRecord(r)
	if rec == nil {
		return -1, errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to get current record.").
			WithBodyModal(failedSave, unableToSaveSummaryPage).Finish()
	}

	//Save record to store
	recordID, err = execution.GetRecordStore().AddSequenceRecord(rec)
	if err != nil {
		return -1, errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to save record into store.").
			WithBodyModal(failedSave, unableToSaveSummaryPage).Finish()
	}

	//Remove Record from Session
	err = sessions.GetSessionStore().RemoveCurrentSequenceRecord(w, r)
	if err != nil {
		err := errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to save current record.").Finish()
		errors.Log(err)
	}
	return
}

// getFormValueInt gets the value to the given key from the request and converts it into an int.
// Returns -2 if the value empty.
// Returns -1 if the value is not an integer.
func getFormValueInt(r *http.Request, key string) int {
	ver := r.FormValue(key)
	if ver == "" {
		return -2
	}
	return stringToInt(ver)
}

// getFormValueString gets the value to the given key from the request.
// Returns empty string if the key does not exists.
func getFormValueString(r *http.Request, key string) string {
	return r.FormValue(key)
}

// getFormValueInt gets the value to the given key from the request and converts it into a test.result.
// ok is true, if the string could be matched to a result, false otherwise
func getFormValueResult(r *http.Request, key string) (result test.Result, ok bool) {
	ver := r.FormValue(key)
	return stringToResult(ver)
}

// getProjectTabPageByName returns the template with the given name either as fragment only or the with all its parent
// fragments, depending on the isFrag parameter
func getProjectTabPageByName(r *http.Request, templateName string) *template.Template {
	if isFragmentRequest(r) {
		return getProjectTabFragmentByName(templateName)
	}
	return getProjectTabTreeByName(templateName)
}

// getProjectTabFragmentByName returns only the tab template with the given name
func getProjectTabFragmentByName(templateName string) *template.Template {
	return getBaseTree().Append(templateName).Get().Lookup("tab-content")
}

// getProjectTabTreeByName returns tab template with the given name with all parent templates
func getProjectTabTreeByName(templateName string) *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab testcase edit tree
		Append(templateName).
		Get().Lookup("header")
}

// stringToResult gets a string and converts it to a test.result
// ok is true, if the string could be matched to a result, false otherwise
func stringToResult(s string) (result test.Result, ok bool) {
	switch s {
	case "0", "NotAssessed":
		return test.NotAssessed, true
	case "1", "Pass":
		return test.Pass, true
	case "2", "PassWithComment":
		return test.PassWithComment, true
	case "3", "Fail":
		return test.Fail, true
	default:
		return test.NotAssessed, false
	}
}

func totalWorkSteps(tsv test.SequenceVersion) int {
	result := tsv.CountIncludedSteps()
	result += 2 * len(tsv.Cases)
	result += 2
	return result
}
func totalWorkStepsUpTo(tsv test.SequenceVersion, maxCaseNr int) int {
	if maxCaseNr < 0 {
		return 0
	} else if maxCaseNr == 0 {
		return 1
	}
	result := tsv.CountIncludedStepsUpTo(maxCaseNr - 1)
	result += 2 * maxCaseNr
	result++
	return result
}
