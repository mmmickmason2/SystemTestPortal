/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"encoding/json"
	"html/template"
	"io"
	"net/http"
	"strings"
	"time"

	"log"

	"strconv"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/test"
	testStore "gitlab.com/stp-team/systemtestportal/store/test"
	"gitlab.com/stp-team/systemtestportal/web/action"
	"gitlab.com/stp-team/systemtestportal/web/context"
	"gitlab.com/stp-team/systemtestportal/web/errors"
	"gitlab.com/stp-team/systemtestportal/web/slugs"
)

// handleTestCaseActionGet handles the actions of the GET request
func handleTestCaseActionGet(w http.ResponseWriter, r *http.Request, p project.Project, path string) {

	url := r.URL.EscapedPath()
	id := strings.Split(url, "/")[4]
	if id == "json" {
		getJSONTestCases(w, r, p)
	}

	tc, _, err := getTestObjectFromPath(w, r, p)
	if err != nil {
		return
	}

	pathSegments := strings.Split(path, "/")
	if len(pathSegments) < 1 {
		return
	}

	if len(pathSegments) == 1 {
		specificTestCaseAction(w, r, p, tc, action.Show)
		return
	}

	switch pathSegments[1] {
	case action.Execute:
		handleTestCaseExecutionGet(w, r, p, tc)
	case action.Edit:
		specificTestCaseAction(w, r, p, tc, action.Edit)
	case action.History:
		historyTestCaseHandler(w, r, p, tc)
	case "json":
		getJSONTestCase(w, r, p, tc)
	case TestSteps:
		var path string
		if len(pathSegments) == 2 {
			path = action.Show
		} else {
			path = pathSegments[2]
		}
		handleTestStepActionGet(w, r, path, tc)
	default:
		errors.WrongPath(w, r)
	}
}

// specificTestCaseAction handles the showing or editing of a test case
func specificTestCaseAction(w http.ResponseWriter, r *http.Request, p project.Project, tc test.Case, ac string) {
	tmpl := getTabTestCaseShowEdit(r, ac)
	tcv := getTestCaseVersionToShowOrEdit(w, r, tc, ac)
	if tcv <= 0 {
		return
	}
	ctx := context.New().
		WithUserInformation(r).
		With("Project", p).
		With("TestCase", tc).
		With("TestCaseVersion", tc.TestCaseVersions[len(tc.TestCaseVersions)-tcv])
	if err := tmpl.Execute(w, ctx); err != nil {
		log.Printf("Failed to show test case error on template parsing: %s", err)
	}
}

// historyTestCaseHandler is a handler for showing the history of a test case
func historyTestCaseHandler(w io.Writer, r *http.Request, p project.Project, tc test.Case) {
	tmpl := getTabTestCaseHistory(r)
	ctx := context.New().
		WithUserInformation(r).
		With("Project", p).
		With("TestCase", tc)
	if err := tmpl.Execute(w, ctx); err != nil {
		log.Printf("Failed to show test case error on template parsing: %s", err)
	}
}

func handleTestCaseActionPost(w http.ResponseWriter, r *http.Request, p project.Project) {
	url := r.URL.EscapedPath()
	urlSeg := strings.Split(url, "/")
	tc, _, err := getTestObjectFromPath(w, r, p)
	if err != nil {
		return
	}

	if len(urlSeg) > 5 && urlSeg[5] == action.Duplicate {
		saveDuplicateTestCaseHandler(w, r, p)
	} else if len(urlSeg) > 5 && urlSeg[5] == action.Execute {
		handleTestCaseExecutionPost(w, r, p, tc)
	}
}

// saveNewTestCaseHandler is used to save a new test case in the system
func saveNewTestCaseHandler(w http.ResponseWriter, r *http.Request, p project.Project) {
	tcn, _, tcd, tcp, sutvFromText, sutvToText, dur, st, err := getTestCaseInput(r, p)
	if err != nil {
		HandleError(err, w)
		return
	}

	tc := test.NewTestCase(tcn, tcd, tcp, sutvFromText, sutvToText, dur)
	tc.TestCaseVersions[0].Steps = st
	if vErr := validateItem(p, tc); vErr != nil {
		HandleError(vErr, w)
		return
	}

	err = testStore.GetCaseStore().Add("user", p.ID, &tc)

	if err != nil {
		HandleError(err, w)
		return
	}

	w.Header().Set("newName", slugs.Slugify(tc.Name))
	w.WriteHeader(http.StatusCreated)

	generalTestsActionHandler(w, r, p, TestCases, action.Show)
}

// saveDuplicateTestCaseHandler saves a duplicated version of the current test case up to the currently
// selected version
func saveDuplicateTestCaseHandler(w http.ResponseWriter, r *http.Request, p project.Project) {
	tc, _, err := getTestObjectFromPath(w, r, p)
	if err != nil {
		HandleError(err, w)
		return
	}

	dupTc := tc
	dupName := r.FormValue("inputTestCaseName")
	dupTc.ID = slugs.Slugify(dupName)
	dupTc.Name = dupName
	if err = validateItem(p, dupTc); err != nil {
		HandleError(err, w)
		return
	}
	tcvLen := len(dupTc.TestCaseVersions)

	ver := getFormValueVersion(r)
	if ver <= 0 || ver > tcvLen {
		HandleError(InvalidTCVersion(), w)
		return
	}
	verIndex := tcvLen - ver
	// We allocate a new slice with a new underlying array for the duplicated test case and copy the test case versions
	// over afterwards. Since the test case versions are structures, that is no problem.
	dupTc.TestCaseVersions = make([]test.CaseVersion, ver)
	copy(dupTc.TestCaseVersions, tc.TestCaseVersions[verIndex:])

	err = testStore.GetCaseStore().Add("user", p.ID, &dupTc)
	if err != nil {
		HandleError(err, w)
		return
	}

	w.Header().Set("newName", dupTc.ID)
	w.WriteHeader(http.StatusCreated)

	specificTestCaseAction(w, r, p, dupTc, action.Show)
}

// deleteTestCaseHandler is a handler for deleting a test sequence from the system
func deleteTestCaseHandler(w http.ResponseWriter, r *http.Request, p project.Project, tc test.Case) {
	err := testStore.GetCaseStore().Delete("user", p.ID, tc.ID)

	if err != nil {
		HandleError(err, w)
		return
	}

	generalTestsActionHandler(w, r, p, TestCases, action.Show)
}

// updateTestCaseHandler is a handler for updating a test case
func updateTestCaseHandler(w http.ResponseWriter, r *http.Request, p project.Project, tc test.Case) {
	im, tcn, cm, tcd, tcp, sutvFrom, sutvTo, dur, steps, err := getTestCaseEditingInput(r, p)
	if err != nil {
		HandleError(err, w)
		return
	}

	tc, err = handleTestCaseVersion(r, tc, im, cm, tcd, tcp, sutvFrom, sutvTo, dur, steps)
	if err != nil {
		HandleError(err, w)
		return
	}

	p, tc, err = updateTestCase(p, tc, tcn)
	if err != nil {
		HandleError(err, w)
		return
	}

	w.Header().Set("newName", slugs.Slugify(tc.Name))
	w.WriteHeader(http.StatusCreated)
	if err := r.ParseForm(); err != nil {
		HandleError(err, w)
		return
	}
	r.Form.Set("version", "")

	specificTestCaseAction(w, r, p, tc, action.Show)
}

// getTestCaseInput gets the test case input from the request
// Returns -1 as duration if sut version is not valid or duration is not valid
func getTestCaseInput(r *http.Request, p project.Project) (string, string, string, string,
	string, string, time.Duration, []test.Step, error) {

	data := struct {
		InputTestCaseName,
		InputTestCaseDescription,
		InputTestCasePreconditions,
		InputTestCaseSUTVersionFrom,
		InputTestCaseSUTVersionTo string
		InputHours,
		InputMinutes int
		InputSteps []struct {
			ID               int
			Actual, Expected string
		}
	}{}

	if errJSON := json.NewDecoder(r.Body).Decode(&data); errJSON != nil {
		return "", "", "", "", "", "", -1, []test.Step{}, errJSON
	}
	tcn := data.InputTestCaseName
	tcd := data.InputTestCaseDescription
	tcp := data.InputTestCasePreconditions
	sutvFromText := data.InputTestCaseSUTVersionFrom
	sutvToText := data.InputTestCaseSUTVersionTo

	if err := validateSUTVersion(p, sutvFromText, sutvToText); err != nil {
		return "", "", "", "", "", "", -1, []test.Step{}, err
	}
	dur, err := getTestCaseVersionDuration(strconv.Itoa(data.InputHours), strconv.Itoa(data.InputMinutes))
	if err != nil {
		return "", "", "", "", "", "", -1, []test.Step{}, err
	}
	st := constructSteps(data.InputSteps)
	if st != nil {
		return tcn, "", tcd, tcp, sutvFromText, sutvToText, dur, st, nil
	}
	return tcn, "", tcd, tcp, sutvFromText, sutvToText, dur, []test.Step{}, nil
}

func getTestCaseEditingInput(r *http.Request, p project.Project) (string, string, string, string, string,
	string, string, time.Duration, []test.Step, error) {
	data := struct {
		Fragment,
		IsMinor bool
		InputTestCaseName,
		InputCommitMessage,
		InputTestCaseDescription,
		InputTestCasePreconditions,
		InputTestCaseSUTVersionFrom,
		InputTestCaseSUTVersionTo string
		InputHours,
		InputMinutes int
		InputSteps []struct {
			ID               int
			Actual, Expected string
		}
	}{}

	if errJSON := json.NewDecoder(r.Body).Decode(&data); errJSON != nil {
		return "", "", "", "", "", "", "", -1, []test.Step{}, errJSON
	}
	im := strconv.FormatBool(data.IsMinor)
	tcn := data.InputTestCaseName
	cm := data.InputCommitMessage
	tcd := data.InputTestCaseDescription
	tcp := data.InputTestCasePreconditions
	sutvFrom := data.InputTestCaseSUTVersionFrom
	sutvTo := data.InputTestCaseSUTVersionTo

	if err := validateSUTVersion(p, sutvFrom, sutvTo); err != nil {
		return "", "", "", "", "", "", "", -1, []test.Step{}, err
	}

	dur, err := getTestCaseVersionDuration(strconv.Itoa(data.InputHours), strconv.Itoa(data.InputMinutes))
	if err != nil {
		return "", "", "", "", "", "", "", -1, []test.Step{}, err
	}
	st := constructSteps(data.InputSteps)
	if st != nil {
		return im, tcn, cm, tcd, tcp, sutvFrom, sutvTo, dur, st, nil
	}

	return im, tcn, cm, tcd, tcp, sutvFrom, sutvTo, dur, []test.Step{}, nil
}

// handleTestCaseVersion creates a new test case version and either marks it as minor edit, depending on the
// "isMinor" parameter in the request
// Returns false if the version is not valid.
func handleTestCaseVersion(r *http.Request, tc test.Case, im, cm, tcd, tcp, sutvFrom, sutvTo string,
	dur time.Duration, steps []test.Step) (test.Case, error) {

	var tcv test.CaseVersion

	curVer := len(tc.TestCaseVersions)
	if curVer <= 0 {
		return tc, InvalidTCVersion()
	}

	if isMinorUpdate(im) {
		tcv = test.NewTestCaseVersion(curVer+1, true, cm, tcd, tcp, sutvFrom, sutvTo, dur)
		tcv.Steps = steps
	} else {
		tcv = test.NewTestCaseVersion(curVer+1, false, cm, tcd, tcp, sutvFrom, sutvTo, dur)
		tcv.Steps = steps
	}

	// Insert at beginning of slice
	tc.TestCaseVersions = append(tc.TestCaseVersions, test.CaseVersion{})
	copy(tc.TestCaseVersions[1:], tc.TestCaseVersions[:])
	tc.TestCaseVersions[0] = tcv

	return tc, nil
}

// InvalidTCVersion returns an ErrorMessage describing that the test case version is not valid
func InvalidTCVersion() errors.HandlerError {
	return errors.Construct(http.StatusBadRequest).
		WithLog("Invalid test case version send by client.").
		WithBodyModal("Invalid test case version", "The test case"+
			"version is invalid").
		WithStackTrace(1).
		Finish()
}

// getTestCaseVersionDuration gets the test case duration as time.Duration
// Returns -1 if the time is invalid
func getTestCaseVersionDuration(hours, mins string) (time.Duration, error) {

	durH, hErr := getHourDuration(hours)
	durM, mErr := getMinDuration(mins)

	if hErr != nil {
		return -1, hErr
	}
	if mErr != nil {
		return -1, mErr
	}

	return durH + durM, nil
}

// updateTestCase handles the updating of a test case.
// If the test case is updated, the old test case will be deleted and replaced with the new one
// If the test case is not valid, it shows an error and returns false.
func updateTestCase(p project.Project, tc test.Case, tcName string) (
	project.Project, test.Case, error) {

	backupTc, _, err := testStore.GetCaseStore().Get("user", p.ID, tc.ID)
	if err != nil {
		return p, tc, err
	}

	err = testStore.GetCaseStore().Delete("user", p.ID, tc.ID)
	if err != nil {
		return p, tc, err
	}

	tc.Name = tcName
	tc.ID = slugs.Slugify(tc.Name)
	if vErr := validateItem(p, tc); vErr != nil {
		err = testStore.GetCaseStore().Add("user", p.ID, backupTc)
		if err != nil {
			return p, tc, err
		}
		return p, tc, vErr
	}

	err = testStore.GetCaseStore().Add("user", p.ID, &tc)
	return p, tc, err
}

// validateSUTVersion checks if sutvFrom is older or equals sutvTo
func validateSUTVersion(p project.Project, sutvFromText, sutvToText string) error {
	ov := test.MinSUT
	nv := test.MaxSUT
	sutvFrom := sutvFromText
	sutvTo := sutvToText

	if len(p.SUTVersions) > 0 {

		sutvFrom, sutvTo = checkOldestAndCurrentVersion(p, sutvFromText, sutvToText)
		if sutvFrom == "" || sutvTo == "" {
			return InvalidSUTVersion()
		}

		iFrom := getIndex(sutvFrom, p.SUTVersions)
		iTo := getIndex(sutvTo, p.SUTVersions)
		if iFrom > iTo || iFrom == -1 || iTo == -1 {
			return InvalidSUTVersion()
		}
	} else if sutvFrom == nv && sutvTo == ov {
		return InvalidSUTVersion()
	}

	return nil
}

// InvalidSUTVersion returns an ErrorMessage describing that the from sut version is not older than the to sut version
func InvalidSUTVersion() errors.HandlerError {
	return errors.Construct(http.StatusBadRequest).
		WithLog("Invalid SUT version send by client.").
		WithBodyModal("Invalid SUT Version", "The from SUT Version "+
			"has to be older than the to SUT Version").
		WithStackTrace(1).
		Finish()
}

// checkOldestAndCurrentVersions checks if sutvFrom or sutvTo is the oldest or current version
// If the order is invalid, return "", ""
func checkOldestAndCurrentVersion(p project.Project, sutvFromText, sutvToText string) (string, string) {
	ov := test.MinSUT
	nv := test.MaxSUT
	sutvFrom := sutvFromText
	sutvTo := sutvToText

	if sutvFromText == ov {
		sutvFrom = p.SUTVersions[0]
	} else if sutvFromText == nv {
		if sutvToText != nv {
			return "", ""
		}
		sutvFrom = p.SUTVersions[len(p.SUTVersions)-1]
	}
	if sutvToText == ov {
		if sutvFromText != ov {
			return "", ""
		}
		sutvTo = p.SUTVersions[0]
	} else if sutvToText == nv {
		sutvTo = p.SUTVersions[len(p.SUTVersions)-1]
	}

	return sutvFrom, sutvTo
}

// getTestCaseVersionToShowOrEdit returns the version of the test case to show or edit
func getTestCaseVersionToShowOrEdit(w http.ResponseWriter, r *http.Request, tc test.Case, actn string) int {
	if actn == action.Edit {
		return getTestVersionToEdit(w, r, tc)
	}
	return getTestVersionToShow(w, r, tc)
}

// getHourDuration gets the hours of the duration from the request
// Returns -1 if duration is invalid
func getHourDuration(timeHourString string) (time.Duration, error) {
	if timeHourString == "" {
		return -1, InvalidTestDuration()
	}
	countHours := stringToDuration(timeHourString)
	if countHours < 0 || countHours > 2562046 {
		return -1, InvalidTestDuration()
	}
	return time.Hour * countHours, nil
}

// getMinDuration gets the minutes of the duration from the request
// Returns -1 if duration is invalid
func getMinDuration(timeMinString string) (time.Duration, error) {
	if timeMinString == "" {
		return -1, InvalidTestDuration()
	}
	countMin := stringToDuration(timeMinString)
	if 0 > countMin || 60 <= countMin {
		return -1, InvalidTestDuration()
	}
	return time.Minute * countMin, nil
}

func constructSteps(rawSteps []struct {
	ID               int
	Actual, Expected string
}) []test.Step {
	if rawSteps != nil {
		var resSteps []test.Step

		for i, s := range rawSteps {
			resSteps = append(resSteps, test.NewTestStep(s.Actual, s.Expected))
			resSteps[i].ID = s.ID
		}
		return resSteps
	}
	return nil
}

// InvalidTestDuration returns an ErrorMessage describing that the test duration is invalid
func InvalidTestDuration() errors.HandlerError {
	return errors.
		Construct(http.StatusBadRequest).
		WithLog("Invalid test duration send by client.").
		WithBodyModal("Invalid Test Duration", "The test duration "+
			"is invalid").
		WithStackTrace(1).
		Finish()
}

// getJSONTestCases gets all test cases of the given project as json and writes them in the response
func getJSONTestCases(w io.Writer, r *http.Request, p project.Project) {
	tcs, _ := json.Marshal(testStore.GetCaseStore().List("user", p.ID))
	w.Write(tcs)
}

// getJSONTestCase gets a test case of a project and writes it in the response
func getJSONTestCase(w io.Writer, r *http.Request, p project.Project, tc test.Case) {
	tcjson, _ := json.Marshal(tc)
	w.Write(tcjson)
}

// getTestCaseListFragment returns either only the test case list fragment or the fragment with all parent templates,
// depending of the "fragment" parameter in the request
func getTestCaseListFragment(r *http.Request) *template.Template {
	if isFragmentRequest(r) {
		return GetTabTestCasesListFragment()
	}
	return GetTabTestCasesListTree()
}

// getTestCaseNewFragment returns either only the test case new fragment or the fragment with all parent templates,
// depending of the "fragment" parameter in the request
func getTestCaseNewFragment(r *http.Request) *template.Template {
	if isFragmentRequest(r) {
		return GetTabTestCasesNewFragment()
	}
	return GetTabTestCasesNewTree()
}

// getTabTestCaseShowEdit returns either the show or edit fragments for a test case
func getTabTestCaseShowEdit(r *http.Request, actn string) *template.Template {
	switch actn {
	case action.Show:
		return getTabTestCaseShow(r)
	case action.Edit:
		return getTabTestCaseEdit(r)
	}
	return nil
}

// getTabTestCaseShow returns either the test case show fragment only or the show fragment with all its parent
// fragments, depending on the isFrag parameter
func getTabTestCaseShow(r *http.Request) *template.Template {
	if r.Method == http.MethodPut {
		return GetTabTestCasesShowFragment()
	}

	if isFragmentRequest(r) {
		return GetTabTestCasesShowFragment()
	}
	return GetTabTestCasesShowTree()
}

// getTabTestCaseEdit returns either the test case edit fragment only or the edit fragment with all its parent
// fragments, depending on the isFrag parameter
func getTabTestCaseEdit(r *http.Request) *template.Template {
	if isFragmentRequest(r) {
		return GetTabTestCasesEditFragment()
	}
	return GetTabTestCasesEditTree()
}

// getTabTestCaseHistory returns either only the test case history fragment or the fragment with all parent templates,
// depending of the "fragment" parameter in the request
func getTabTestCaseHistory(r *http.Request) *template.Template {
	if isFragmentRequest(r) {
		return GetTabTestCasesHistoryFragment()
	}
	return GetTabTestCasesHistoryTree()

}

// GetTabTestCasesListTree returns the test case list tab template with all parent templates
func GetTabTestCasesListTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab testcase list tree
		Append("tab-testcases-list").
		Get().Lookup("header")
}

// GetTabTestCasesNewTree returns the new test case tab template with all parent templates
func GetTabTestCasesNewTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs", "modal-teststep-edit").
		// Tab testcase new tree
		Append("tab-testcases-new").
		Get().Lookup("header")
}

// GetTabTestCasesShowTree returns the test case show tab template with all parent templates
func GetTabTestCasesShowTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab testcase show tree
		Append("tab-testcases-show").
		Get().Lookup("header")
}

// GetTabTestCasesEditTree returns the test case edit tab template with all parent templates
func GetTabTestCasesEditTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs", "modal-teststep-edit").
		// Tab testcase edit tree
		Append("tab-testcases-edit").
		Get().Lookup("header")
}

// GetTabTestCasesHistoryTree returns the test case history tab template with all parent templates
func GetTabTestCasesHistoryTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab testcase history tree
		Append("tab-testcases-history").
		Get().Lookup("header")
}

// GetTabTestCasesListFragment returns only the test case list tab template
func GetTabTestCasesListFragment() *template.Template {
	return getBaseTree().Append("tab-testcases-list").Get().Lookup("tab-content")
}

// GetTabTestCasesNewFragment returns only the new test case tab template
func GetTabTestCasesNewFragment() *template.Template {
	return getBaseTree().Append("tab-testcases-new", "modal-teststep-edit").Get().Lookup("tab-content")
}

// GetTabTestCasesShowFragment returns only the test case show tab template
func GetTabTestCasesShowFragment() *template.Template {
	return getBaseTree().Append("tab-testcases-show").Get().Lookup("tab-content")
}

// GetTabTestCasesEditFragment returns only the test case edit template
func GetTabTestCasesEditFragment() *template.Template {
	return getBaseTree().Append("tab-testcases-edit", "modal-teststep-edit").Get().Lookup("tab-content")
}

// GetTabTestCasesHistoryFragment returns only the test case history template
func GetTabTestCasesHistoryFragment() *template.Template {
	return getBaseTree().Append("tab-testcases-history").Get().Lookup("tab-content")
}

//prepared for Execution:

// GetTabRunStartpageTree returns the run startpage tab template with all parent templates
func GetTabRunStartpageTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab Run Startpage Tree
		Append("tab-run-startpage").
		Get().Lookup("header")
}

// GetTabRunStepTree returns the run step tab template with all parent templates
func GetTabRunStepTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab Run Startpage Tree
		Append("tab-run-step").
		Get().Lookup("header")
}

// GetTabRunSummaryTree returns the run summary tab template with all parent templates
func GetTabRunSummaryTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab Run Startpage Tree
		Append("tab-run-summary").
		Get().Lookup("header")
}

// GetTabRunStartpageFragment returns only the run startpage tab template
func GetTabRunStartpageFragment() *template.Template {
	return getBaseTree().Append("tab-run-startpage").Get().Lookup("tab-content")
}

// GetTabRunStepFragment returns only the run step tab template
func GetTabRunStepFragment() *template.Template {
	return getBaseTree().Append("tab-run-step").Get().Lookup("tab-content")
}

// GetTabRunSummaryFragment returns only the run summary tab template
func GetTabRunSummaryFragment() *template.Template {
	return getBaseTree().Append("tab-run-summary").Get().Lookup("tab-content")
}
