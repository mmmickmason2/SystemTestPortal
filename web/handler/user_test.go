/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"
	"testing"

	"net/http/httptest"
	"net/url"

	"errors"

	"gitlab.com/stp-team/systemtestportal/domain/user"
)

// AuthMock mocks the UserAuth interface
type AuthMock struct {
	GivenIdentifier string
	GivenPassword   string
	accept          bool
	returnUser      *user.User
	returnError     error
}

// Validate is a mock method
func (a *AuthMock) Validate(identifier string, password string) (*user.User, bool, error) {
	a.GivenIdentifier = identifier
	a.GivenPassword = password
	return a.returnUser, a.accept, a.returnError
}

// NewAuthMock creates a new UserAuth mock
func NewAuthMock(returnedUser *user.User, returnedError error, accept bool) *AuthMock {
	return &AuthMock{
		"",
		"",
		accept,
		returnedUser,
		returnedError,
	}
}

// SessionHandlerMock mocks a SessionHandler
type SessionHandlerMock struct {
	returnUser      *user.User
	StartForCalls   int
	EndForCalls     int
	GetCurrentCalls int
	startForError   error
	endForError     error
	getCurrentError error
}

// StartFor is a mock method
func (s *SessionHandlerMock) StartFor(w http.ResponseWriter, r *http.Request, u *user.User) error {
	s.StartForCalls++
	return s.startForError
}

// EndFor is a mock method
func (s *SessionHandlerMock) EndFor(w http.ResponseWriter, r *http.Request, u *user.User) error {
	s.EndForCalls++
	return s.endForError
}

// GetCurrent is a mock method
func (s *SessionHandlerMock) GetCurrent(r *http.Request) (*user.User, error) {
	s.GetCurrentCalls++
	return s.returnUser, s.getCurrentError
}

// configSet holds a test configuration.
type configSet struct {
	session *SessionHandlerMock
	auth    *AuthMock
}

// testSet holds a test assumptions  applied to a test configuration.
type testSet struct {
	email           string
	password        string
	statusCode      int
	givenIdentifier string
	givenPassword   string
	StartForCalls   int
	EndForCalls     int
}

// NewSessionMock creates a new mock for a SessionHandler
func NewSessionMock(returnedUser *user.User, startForError error,
	endForError error, getCurrentError error) *SessionHandlerMock {
	return &SessionHandlerMock{
		returnedUser,
		0,
		0,
		0,
		startForError,
		endForError,
		getCurrentError,
	}
}

// tom is an example user for test purposes.
var tom = user.New("T0m", "tom", "tom@example.com", "1234")

// gilbert is an example user for test purposes.
var gilbert = user.New("Gilbert", "g1lb3rt", "gil@bert.com", "13")

// TestLogin_ServeHTTP test the login handler.
func TestLogin_ServeHTTP(t *testing.T) {
	t.Parallel()
	var tests = []struct {
		name string
		cs   configSet
		ts   testSet
	}{
		{
			"Valid login.",
			configSet{
				NewSessionMock(nil, nil, nil, nil),
				NewAuthMock(&tom, nil, true),
			},
			testSet{
				tom.EMail,
				tom.Password,
				http.StatusOK,
				tom.EMail,
				tom.Password,
				1,
				0,
			},
		},
		{
			"Already logged in.",
			configSet{
				NewSessionMock(&tom, nil, nil, nil),
				NewAuthMock(nil, nil, true),
			},
			testSet{
				tom.EMail,
				tom.Password,
				http.StatusBadRequest,
				"",
				"",
				0,
				0,
			},
		},
		{
			"Can't start session.",
			configSet{
				NewSessionMock(nil, errors.New("ups"), nil, nil),
				NewAuthMock(nil, nil, true),
			},
			testSet{
				tom.EMail,
				tom.Password,
				http.StatusInternalServerError,
				tom.EMail,
				tom.Password,
				1,
				0,
			},
		},
		{
			"Can't get current session.",
			configSet{
				NewSessionMock(nil, nil, nil, errors.New("ups")),
				NewAuthMock(nil, nil, true),
			},
			testSet{
				tom.EMail,
				tom.Password,
				http.StatusInternalServerError,
				"",
				"",
				0,
				0,
			},
		},
		{
			"Error authenticating user.",
			configSet{
				NewSessionMock(nil, nil, nil, nil),
				NewAuthMock(nil, errors.New("ups"), false),
			},
			testSet{
				tom.EMail,
				tom.Password,
				http.StatusInternalServerError,
				tom.EMail,
				tom.Password,
				0,
				0,
			},
		},
		{
			"Invalid login credentials.",
			configSet{
				NewSessionMock(nil, nil, nil, nil),
				NewAuthMock(nil, nil, false),
			},
			testSet{
				tom.AccountName,
				tom.Password,
				http.StatusUnauthorized,
				tom.AccountName,
				tom.Password,
				0,
				0,
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			testLoginSingleServeHTTP(t, test.cs, test.ts)
		})
	}
	l := login{tests[0].cs.session, tests[0].cs.auth}
	w := httptest.NewRecorder()
	r, err := http.NewRequest(http.MethodGet, "/login", nil)
	if err != nil {
		t.Fatalf("Unable to create request: %s", err)
	}
	l.ServeHTTP(w, r)
	result := w.Result()
	if result.StatusCode != http.StatusMethodNotAllowed {
		t.Errorf("Wrong status code for use of GET-method"+
			". Expected %d but was %d.", http.StatusMethodNotAllowed, result.StatusCode)
	}
}

// testLoginSingleServeHTTP tests a single test case for the login handler.
func testLoginSingleServeHTTP(t *testing.T, cs configSet, ts testSet) {
	l := NewLoginHandler(cs.session, cs.auth)
	w := httptest.NewRecorder()
	r := generateRequest(t, ts.email, ts.password)
	l.ServeHTTP(w, r)
	result := w.Result()
	if result.StatusCode != ts.statusCode {
		t.Errorf("Wrong status code. Expected %d but was %d.", ts.statusCode, result.StatusCode)
		t.Errorf("Failed for: %+v", ts)
	}
	checkCredentials(t, ts.givenIdentifier, cs.auth.GivenIdentifier, ts)
	checkCredentials(t, ts.givenPassword, cs.auth.GivenPassword, ts)
	checkCalls(t, "SessionHandler.EndFor", ts.EndForCalls, cs.session.EndForCalls, ts)
	checkCalls(t, "SessionHandler.StartFor", ts.StartForCalls, cs.session.StartForCalls, ts)
}

// generateRequest generates a login request with given credentials.
func generateRequest(t *testing.T, e string, pw string) *http.Request {
	r, err := http.NewRequest(http.MethodPost, "/login", nil)
	if err != nil {
		t.Fatalf("Unable to create request: %s", err)
	}
	form := url.Values{}
	form.Add("inputIdentifier", e)
	form.Add("inputPassword", pw)
	r.Form = form
	return r
}

// TestLogout_ServeHTTP test the logout handlers serve function.
func TestLogout_ServeHTTP(t *testing.T) {
	t.Parallel()
	var tests = []struct {
		name          string
		s             *SessionHandlerMock
		startForCalls int
		endForCalls   int
		status        int
	}{
		{
			"Normal Logout.",
			NewSessionMock(&gilbert, nil, nil, nil),
			0,
			1,
			http.StatusOK,
		},
		{
			"Error getting session.",
			NewSessionMock(nil, nil, nil, errors.New("ups")),
			0,
			0,
			http.StatusInternalServerError,
		},
		{
			"No logged in user.",
			NewSessionMock(nil, nil, errors.New("ups"), nil),
			0,
			0,
			http.StatusBadRequest,
		},
		{
			"Error ending session.",
			NewSessionMock(&tom, nil, errors.New("ups"), nil),
			0,
			1,
			http.StatusInternalServerError,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			l := NewLogoutHandler(test.s)
			w := httptest.NewRecorder()
			r := httptest.NewRequest(http.MethodPost, "/logout", nil)
			l.ServeHTTP(w, r)
			result := w.Result()
			if result.StatusCode != test.status {
				t.Errorf("Wrong status code. Expected %d but was %d.", test.status, result.StatusCode)
				t.Errorf("Failed for: Test: %+v with session mock %+v", test, test.s)
			}
			checkCalls(t, "SessionHandler.EndFor", test.endForCalls, test.s.EndForCalls, test)
			checkCalls(t, "SessionHandler.StartFor", test.startForCalls, test.s.StartForCalls, test)
		})
	}
	l := logout{nil}
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/logout", nil)
	l.ServeHTTP(w, r)
	result := w.Result()
	if result.StatusCode != http.StatusMethodNotAllowed {
		t.Errorf("Wrong status code for GET method. Expected %d but was %d.",
			http.StatusMethodNotAllowed, result.StatusCode)
	}
}

// checkCredentials checks if a credential passed to UserAuth matches the expectation.
// If not an error is passed to testing.
func checkCredentials(t *testing.T, exp string, act string, ts ...interface{}) {
	if exp != act {
		t.Errorf("Credentials passed to UserAuth don't match expectations. "+
			"Expected %s but was %s.", exp, act)
		t.Errorf("Failed for test case: %+v", ts)
	}
}

// checkCalls checks if the amount of calls to a method are as expected and creates an error message
// if not.
func checkCalls(t *testing.T, method string, exp int, act int, ts ...interface{}) {
	if exp != act {
		t.Errorf("Wrong amount of Calls to %s. Expected %d but was %d.", method, exp, act)
		t.Errorf("Failed for test case: %+v", ts)
	}
}

type userServerMock struct {
	getUserNameIn   string
	getUserUserOut  *user.User
	getUserFoundOut bool
	addUserUserIn   *user.User
	addUserErrorOut error
}

func (usm *userServerMock) Get(name string) (*user.User, bool) {
	usm.getUserNameIn = name
	return usm.getUserUserOut, usm.getUserFoundOut
}
func (usm *userServerMock) Add(u *user.User) error {
	usm.addUserUserIn = u
	return usm.addUserErrorOut
}
func newUserServerMock(getUserUserOut *user.User, getUserFoundOut bool, addUserErrorOut error) *userServerMock {
	return &userServerMock{"", getUserUserOut, getUserFoundOut, nil, addUserErrorOut}
}

// TestRegister_ServeHTTP test the register handlers serve function.
func TestRegister_ServeHTTP(t *testing.T) {
	userServer := newUserServerMock(nil, false, nil)
	testHandler := NewRegisterHandler(userServer)
	accountName := "Robert1234"
	displayName := "KingRob"
	email := "rob@test.de"
	password := "secret"
	newUser := user.New(displayName, accountName, email, password)
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/register", nil)
	form := url.Values{}
	form.Add("inputAccountName", accountName)
	form.Add("inputDisplayName", displayName)
	form.Add("inputEmail", email)
	form.Add("inputPassword", password)
	req.Form = form

	testHandler.ServeHTTP(rec, req)

	if code := rec.Result().StatusCode; code != http.StatusCreated {
		t.Errorf("Wrong status code. Expected %d but was %d.", http.StatusCreated, code)
	}

	if rec.Header().Get("newAccount") != newUser.ID {
		t.Errorf("Returned wrong ID to client. Expected %q but was %q.", newUser.ID, rec.Header().Get("newAccount"))
	}

	if u := userServer.addUserUserIn; u.AccountName != accountName || u.DisplayName != displayName ||
		u.EMail != email || u.Password != password {
		t.Errorf("Userdata wasn't saved correctly. Expected %+v but %+v was saved", accountName, userServer.getUserNameIn)
	}
}

// TestLogout_ServeHTTP_Negative tests the register handlers serve function if the username is already assigned
func TestRegister_ServeHTTP_Negative(t *testing.T) {
	userServer := newUserServerMock(&user.User{AccountName: "Robert1234"}, true, nil)
	testHandler := NewRegisterHandler(userServer)
	accountName := "Robert1234"
	displayName := "KingRob"
	email := "rob@test.de"
	password := "secret"
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/register", nil)
	form := url.Values{}
	form.Add("inputAccountName", accountName)
	form.Add("inputDisplayName", displayName)
	form.Add("inputEmail", email)
	form.Add("inputPassword", password)
	req.Form = form

	testHandler.ServeHTTP(rec, req)

	if code := rec.Result().StatusCode; code != http.StatusConflict {
		t.Errorf("Wrong status code. Expected %d but was %d.", http.StatusConflict, code)
	}
}

// TestLogout_ServeHTTP_Negative2 tests the register handlers serve function if the user-server had an internal error
func TestRegister_ServeHTTP_Negative2(t *testing.T) {
	userServer := newUserServerMock(nil, false, errors.New("server down"))
	testHandler := NewRegisterHandler(userServer)
	accountName := "Robert1234"
	displayName := "KingRob"
	email := "rob@test.de"
	password := "secret"
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/register", nil)
	form := url.Values{}
	form.Add("inputAccountName", accountName)
	form.Add("inputDisplayName", displayName)
	form.Add("inputEmail", email)
	form.Add("inputPassword", password)
	req.Form = form

	testHandler.ServeHTTP(rec, req)

	if code := rec.Result().StatusCode; code != http.StatusInternalServerError {
		t.Errorf("Wrong status code. Expected %d but was %d.", http.StatusInternalServerError, code)
	}
}
