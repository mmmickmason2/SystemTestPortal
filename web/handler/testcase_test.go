/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"reflect"
	"strconv"
	"testing"
	"time"

	"bytes"
	"encoding/json"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/test"
	"gitlab.com/stp-team/systemtestportal/domain/visibility"
	projectStore "gitlab.com/stp-team/systemtestportal/store/project"
	testStore "gitlab.com/stp-team/systemtestportal/store/test"
	"gitlab.com/stp-team/systemtestportal/web/slugs"
)

var dummyTestCase = test.Case{
	ID:           "test-case-1",
	Name:         "Test Case 1",
	CreationDate: time.Now().Round(time.Second),
	TestCaseVersions: []test.CaseVersion{
		{
			Description: "First Test Case",
			Steps: []test.Step{
				{
					ID:             1,
					Action:         "Open the application",
					ExpectedResult: "The main screen should show",
				},
				{
					ID:             2,
					Action:         "Open the login page",
					ExpectedResult: "The login page should show",
				},
				{
					ID:             3,
					Action:         "Login with the test login",
					ExpectedResult: "The login should succeed",
				},
			},
		},
	},
}

// TestTestCasesHandlerBadRequest checks if bad requests are handled correctly
func TestTestCasesHandlerBadRequest(t *testing.T) {
	testBadRequest(t, "/user/project-1/testcases/")
}

func TestGetRequestTestCase(t *testing.T) {
	testStore.GetCaseStore().Add("user", dummyProject.ID, &dummyTestCase)
	testGetRequestTest(t, TestCases, "test-case-1")
}

// TestTestCaseSaving tests the saving of test cases and checks the response code
func TestTestCaseSaving(t *testing.T) {
	tsts := []struct {
		name       string
		statusWant int
		okWant     bool
		project    project.Project
	}{
		{"TEST CASE 9999", http.StatusCreated, true, *dummyProject},
		{"!!", http.StatusBadRequest, false, *dummyProject},
		{"TEST CASE 9998", http.StatusCreated, true, project.Project{
			ID:           "sutproject",
			Name:         "sutproject",
			Visibility:   visibility.Public,
			SUTVersions:  []string{},
			CreationDate: time.Now().Round(time.Second),
		}},
	}
	for _, tst := range tsts {
		projectStore.GetStore().Add("user", &tst.project)
		testTestCaseSaving(t, tst.project, tst.name, tst.statusWant, tst.okWant)
	}
}

func testTestCaseSaving(t *testing.T, p project.Project, name string, statusWant int, okWant bool) {
	type dataForm struct {
		InputTestCaseName,
		InputTestCaseDescription,
		InputTestCasePreconditions,
		InputTestCaseSUTVersionFrom,
		InputTestCaseSUTVersionTo string
		InputHours,
		InputMinutes int
		InputSteps []struct {
			ID               int
			Actual, Expected string
		}
	}
	inputSteps := []struct {
		ID               int
		Actual, Expected string
	}{{0, "", ""}}
	data := dataForm{
		name,
		"",
		"",
		test.MinSUT,
		test.MaxSUT,
		0,
		0,
		inputSteps,
	}

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(data)

	req, err := http.NewRequest(http.MethodPost, "/user/"+p.ID+"/testcases/save", b)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	hut := http.HandlerFunc(ProjectHandler)

	hut.ServeHTTP(rr, req)

	if status := rr.Code; status != statusWant {
		t.Errorf("Saving %s returned wrong status code: got %v want %v", name, status, statusWant)
	}

	if _, ok, _ := testStore.GetCaseStore().Get("user", p.ID, slugs.Slugify(name)); ok != okWant {
		if okWant {
			t.Errorf("test case %s was not created", name)
		} else {
			t.Errorf("test case %s was created", name)
		}
	}
}

// TestTestCaseDuplicating tests the duplicating of test cases and checks the response code
func TestTestCaseDuplicating(t *testing.T) {
	tsts := []struct {
		name       string
		version    int
		tcToDup    test.Case
		statusWant int
		okWant     bool
		project    *project.Project
	}{
		{"TEST CASE DUPLICATE", 1, dummyTestCase,
			http.StatusCreated, true, dummyProject},
		{"!!", 1, dummyTestCase,
			http.StatusBadRequest, false, dummyProject},
	}
	for _, tst := range tsts {
		projectStore.GetStore().Add("user", dummyProject)
		testStore.GetCaseStore().Add("user", tst.project.ID, &dummyTestCase)
		t.Run(tst.name, testTestCaseDuplicating(tst.project, tst.tcToDup, tst.name, tst.version, tst.statusWant, tst.okWant))
	}
}

func testTestCaseDuplicating(p *project.Project, tcToDup test.Case, name string, version,
	statusWant int, okWant bool) func(*testing.T) {
	return func(t *testing.T) {
		form := url.Values{}
		form.Add("inputTestCaseName", name)
		form.Add("version", strconv.Itoa(version))

		req, err := http.NewRequest(http.MethodPost, "/user/"+p.ID+"/testcases/"+tcToDup.ID+"/duplicate", nil)
		if err != nil {
			t.Fatal(err)
		}

		req.Form = form

		rr := httptest.NewRecorder()
		hut := http.HandlerFunc(ProjectHandler)

		hut.ServeHTTP(rr, req)

		if status := rr.Code; status != statusWant {
			t.Errorf("Duplicating %s returned wrong status code: got %v want %v", name, status, statusWant)
		}

		if _, ok, _ := testStore.GetCaseStore().Get("user", p.ID, slugs.Slugify(name)); ok != okWant {
			if okWant {
				t.Errorf("test case %s was not duplicated", name)
			} else {
				t.Errorf("test case %s was mistakenly duplicated", name)
			}
		}
	}
}

func TestTestCaseDeleting(t *testing.T) {
	tcs := []struct {
		name       string
		tcToDel    test.Case
		statusWant int
		project    *project.Project
	}{
		{"test-case-to-delete", test.NewTestCase("test-case-to-delete", "",
			"", "", "", 0), http.StatusOK, dummyProject},
		{"----NOT-A-TEST-CASE", test.Case{}, http.StatusNotFound, dummyProject},
	}
	for _, tc := range tcs {
		// Add test case to delete to project if the test case is not an empty struct
		if !reflect.DeepEqual(tc.tcToDel, test.Case{}) {
			testStore.GetCaseStore().Add("user", tc.project.ID, &tc.tcToDel)
		}
		t.Run(tc.name, testTestDeleteRequest(tc.project, TestCases, tc.name, tc.statusWant))
	}
}

// TestTestCaseUpdating tests the updating of a test case with various test cases
func TestTestCaseUpdating(t *testing.T) {
	tcs := []struct {
		tcToUpd    test.Case
		statusWant int
		isValid    bool
		isMinor    string
		project    project.Project
	}{
		{test.Case{
			Name: "TC Valid",
			ID:   "tc-valid",
			TestCaseVersions: []test.CaseVersion{
				{
					VersionNr:      1,
					Description:    "",
					Preconditions:  "",
					SUTVersionFrom: test.MinSUT,
					SUTVersionTo:   test.MaxSUT,
					DurationHours:  0,
					DurationMin:    0,
				},
			},
		}, http.StatusCreated, true, "false",
			*dummyProject}, {test.Case{
			Name: "TC Valid UPDATED",
			ID:   "tc-valid-updated",
			TestCaseVersions: []test.CaseVersion{
				{
					VersionNr:      1,
					Description:    "a",
					Preconditions:  "a",
					SUTVersionFrom: "Oldest Version",
					SUTVersionTo:   "Current Version",
					DurationHours:  1,
					DurationMin:    1,
					Steps: []test.Step{{
						ID:             0,
						Action:         "Update",
						ExpectedResult: "Updated",
					}, {
						ID:             1,
						Action:         "Again",
						ExpectedResult: "Again",
					}},
				},
			},
		}, http.StatusCreated, true, "false",
			*dummyProject}, {test.Case{
			Name: "",
			ID:   "tc-valid",
			TestCaseVersions: []test.CaseVersion{
				{
					VersionNr:      1,
					Description:    "",
					Preconditions:  "",
					SUTVersionFrom: test.MinSUT,
					SUTVersionTo:   test.MaxSUT,
					DurationHours:  0,
					DurationMin:    0,
				},
			},
		}, http.StatusBadRequest, false, "false",
			*dummyProject}, {test.Case{
			Name: "TC Valid",
			ID:   "tc-valid",
			TestCaseVersions: []test.CaseVersion{
				{
					VersionNr:      1,
					Description:    "",
					Preconditions:  "",
					SUTVersionFrom: test.MinSUT,
					SUTVersionTo:   test.MaxSUT,
					DurationHours:  -1,
					DurationMin:    0,
				},
			},
		}, http.StatusBadRequest, false, "false",
			*dummyProject}, {test.Case{
			Name: "TC Valid",
			ID:   "tc-valid",
			TestCaseVersions: []test.CaseVersion{
				{
					VersionNr:      1,
					Description:    "",
					Preconditions:  "",
					SUTVersionFrom: test.MinSUT,
					SUTVersionTo:   test.MaxSUT,
					DurationHours:  0,
					DurationMin:    -1,
				},
			},
		}, http.StatusBadRequest, false, "false",
			*dummyProject},
	}
	for _, tc := range tcs {
		testStore.GetCaseStore().Add("user", tc.project.ID, &tc.tcToUpd)
		testTestCaseUpdating(t, tc.project, tc.tcToUpd, tc.statusWant, tc.isValid)
	}
}

// testTestCaseUpdating tests the updating of a test case with a major, a minor update, a normal and a fragment request
func testTestCaseUpdating(t *testing.T, p project.Project, tc test.Case, statusWant int, isValid bool) {
	testTestCaseUpdate(t, p, tc, statusWant, isValid, "false", false)
	testTestCaseUpdate(t, p, tc, statusWant, isValid, "true", false)
	testTestCaseUpdate(t, p, tc, statusWant, isValid, "false", true)
	testTestCaseUpdate(t, p, tc, statusWant, isValid, "true", true)
}

func testTestCaseUpdate(t *testing.T, p project.Project, tc test.Case, statusWant int,
	isValid bool, isMinor string, isFrag bool) {

	type dataForm struct {
		Fragment,
		IsMinor bool
		InputTestCaseName,
		InputTestCaseDescription,
		InputTestCasePreconditions,
		InputTestCaseSUTVersionFrom,
		InputTestCaseSUTVersionTo string
		InputHours,
		InputMinutes int
		InputSteps []struct {
			ID               int
			Actual, Expected string
		}
	}
	var inputSteps []struct {
		ID               int
		Actual, Expected string
	}
	for i, s := range tc.TestCaseVersions[0].Steps {
		elem := struct {
			ID               int
			Actual, Expected string
		}{i,
			s.Action, s.ExpectedResult}
		inputSteps = append(inputSteps, elem)
	}
	minor, err := strconv.ParseBool(isMinor)
	if err != nil {
		t.Fatal(err)
	}

	data := dataForm{
		isFrag,
		minor,
		tc.Name,
		tc.TestCaseVersions[0].Description,
		tc.TestCaseVersions[0].Preconditions,
		tc.TestCaseVersions[0].SUTVersionFrom,
		tc.TestCaseVersions[0].SUTVersionTo,
		tc.TestCaseVersions[0].DurationHours,
		tc.TestCaseVersions[0].DurationMin,
		inputSteps,
	}

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(data)

	req, err := http.NewRequest(http.MethodPut, "/user/"+p.ID+"/testcases/"+tc.ID+"/update", b)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	hut := http.HandlerFunc(ProjectHandler)

	hut.ServeHTTP(rr, req)

	if status := rr.Code; status != statusWant {
		t.Errorf("creating %s returned wrong status code: got %v want %v", tc.Name, status, statusWant)
	}

	// Don't check for test case existence if test case is not valid and therefore not saved
	if !isValid {
		return
	}

	if _, ok, _ := testStore.GetCaseStore().Get("user", p.ID, slugs.Slugify(tc.Name)); !ok {
		t.Errorf("test case %s was not created", tc.Name)
	}
}
