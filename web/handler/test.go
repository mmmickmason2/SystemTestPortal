/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"html/template"
	"io"
	"log"
	"math"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/test"
	testStore "gitlab.com/stp-team/systemtestportal/store/test"
	"gitlab.com/stp-team/systemtestportal/web/action"
	"gitlab.com/stp-team/systemtestportal/web/context"
	"gitlab.com/stp-team/systemtestportal/web/errors"
)

// TestHandler routes a request pertaining to tests to the correct handler
func TestHandler(w http.ResponseWriter, r *http.Request, p project.Project, testType string) {
	path, con := handleTestPath(w, r, p, testType)
	if !con {
		return
	}

	handleTestRequest(w, r, path, p, testType)
}

func handleTestPath(w http.ResponseWriter, r *http.Request, p project.Project, testType string) (string, bool) {
	path := r.URL.EscapedPath()

	if !strings.HasPrefix(path, "/user/"+p.ID+"/"+testType) {
		log.Println("Invalid path", path)
		return path, false
	}

	path = strings.TrimPrefix(path, "/user/"+p.ID+"/"+testType)

	if path == action.Show {
		http.Redirect(w, r, "testcases/", http.StatusFound)
		return path, false
	}

	if strings.HasPrefix(path, "/") {
		path = strings.TrimPrefix(path, "/")
	}

	return path, true
}

func handleTestRequest(w http.ResponseWriter, r *http.Request, path string, p project.Project, testType string) {

	switch r.Method {
	case http.MethodGet:
		handleTestActionGet(w, r, p, testType, path)
	case http.MethodPost:
		handleTestActionPost(w, r, p, testType, path)
	case http.MethodPut:
		handleTestActionPut(w, r, p, testType, path)
	case http.MethodDelete:
		handleTestActionDelete(w, r, p, testType)
	default:
		errors.WrongRequestMethod(w, r)
		return
	}
}

func handleTestActionGet(w http.ResponseWriter, r *http.Request, p project.Project, testType, path string) {

	if path == action.Show || path == action.New {
		generalTestsActionHandler(w, r, p, testType, path)
		return
	}

	switch testType {
	case TestCases:
		handleTestCaseActionGet(w, r, p, path)
	case TestSequences:
		handleTestSequenceActionGet(w, r, p, path)
	default:
		errors.WrongPath(w, r)
	}

}

func handleTestActionPost(w http.ResponseWriter, r *http.Request, p project.Project, testType, path string) {
	switch path {
	case action.Save:
		if testType == TestCases {
			saveNewTestCaseHandler(w, r, p)
		} else if testType == TestSequences {
			saveNewTestSequenceHandler(w, r, p)
		}
	default:
		if testType == TestCases {
			handleTestCaseActionPost(w, r, p)
		} else if testType == TestSequences {
			handleTestSequenceActionPost(w, r, p)
		}

		stepPath, con, tc := computeTestStepPath(w, r, p, path)
		if !con {
			return
		}
		handleTestStepActionPost(w, r, stepPath, tc, p)
	}

}

func handleTestActionPut(w http.ResponseWriter, r *http.Request, p project.Project, testType, path string) {
	pathSeg := strings.Split(path, "/")
	ac := pathSeg[1]
	switch ac {
	case action.Update:
		actionPutDelete(w, r, p, testType, action.Update)
	default:
		stepPath, con, tc := computeTestStepPath(w, r, p, path)
		if !con {
			return
		}
		handleTestStepActionPut(w, r, stepPath, tc, p)
	}
}

func handleTestActionDelete(w http.ResponseWriter, r *http.Request, p project.Project, testType string) {
	actionPutDelete(w, r, p, testType, action.Delete)
}

// actionPutDelete gets the test to update or delete from the url and performs the given action (act parameter)
// on this test
func actionPutDelete(w http.ResponseWriter, r *http.Request, p project.Project, testType, act string) {
	switch testType {
	case TestCases:
		tc, _, err := getTestObjectFromPath(w, r, p)
		if err != nil {
			return
		}
		segments := strings.Split(r.URL.EscapedPath(), "/")
		size := len(segments)
		if segments[size-2] == TestSteps {
			handleTestStepActionDelete(w, r, tc, p)
			return
		}

		switch act {
		case action.Delete:
			deleteTestCaseHandler(w, r, p, tc)
		case action.Update:
			updateTestCaseHandler(w, r, p, tc)
		}
	case TestSequences:
		_, ts, err := getTestObjectFromPath(w, r, p)
		if err != nil {
			return
		}

		if act == action.Delete {
			deleteTestSequenceHandler(w, r, p, ts)
		} else if act == action.Update {
			updateTestSequenceHandler(w, r, p, ts)
		}
	}
}

// generalTestsActionHandler handles the test listing of all tests or showing the creation screen for a test
func generalTestsActionHandler(w io.Writer, r *http.Request, p project.Project, testType, act string) {
	var tmpl *template.Template
	switch testType {
	case TestCases:
		switch act {
		case action.Show:
			tmpl = getTestCaseListFragment(r)
		case action.New:
			tmpl = getTestCaseNewFragment(r)
		}
	case TestSequences:
		switch act {
		case action.Show:
			tmpl = getTestSequenceListFragment(r)
		case action.New:
			tmpl = getTestSequenceNewFragment(r)
		}
	default:
		log.Panicf("Unkown test type %q was passed!", testType)
	}
	execute(tmpl, r, w, p)
}

// getTestVersionToShow gets the test case or test sequence to show. If the version parameter in the request
// is empty, the newest test version will be shown.
// Returns -1 if the version is not valid.
func getTestVersionToShow(w http.ResponseWriter, r *http.Request, t ValidationItemBasic) int {
	tv, length, isCase := getTestFromMap(r, t)

	if tv < 0 || (isCase && tv > length) || (!isCase && tv > length) {
		errors.BadRequest(w)
		return -1
	}

	return tv
}

// getTestVersionToEdit gets the correct test version to edit. If the version parameter is not the newest
// version or invalid the function returns -1 because only the newest version can be edited
func getTestVersionToEdit(w http.ResponseWriter, r *http.Request, t ValidationItemBasic) int {
	tv, length, isCase := getTestFromMap(r, t)

	if (isCase && tv != length) || (!isCase && tv != length) {
		errors.BadRequest(w)
		return -1
	}

	return tv
}

// getTestFromMap gets either a test case from the map of test cases or a test sequence from the test sequences,
// depending on the type of the parameter t
func getTestFromMap(r *http.Request, t ValidationItemBasic) (version, length int, isCase bool) {
	version = getFormValueVersion(r)

	tc, isTc := t.(test.Case)
	if isTc {
		if version <= 0 {
			version = len(tc.TestCaseVersions)
		}
		length = len(tc.TestCaseVersions)
		isCase = true
	}
	ts, isTs := t.(test.Sequence)
	if isTs {
		if version <= 0 {
			version = len(ts.SequenceVersions)

		}
		length = len(ts.SequenceVersions)
	}

	return version, length, isCase
}

// getFormValueVersion gets the version from the request.
// Returns 0 if the version is empty.
// Returns -1 if the version is not an integer.
func getFormValueVersion(r *http.Request) int {
	ver := r.FormValue("version")
	if ver == "" {
		return 0
	}
	return stringToInt(ver)
}

// stringToInt gets a string and converts it to int.
// Returns -1 if the string cannot be converted to an int.
func stringToInt(s string) int {
	ver, err := strconv.ParseInt(s, 10, 32)
	if err != nil {
		log.Println(err)
		return -1
	}
	return int(ver)
}

// getIndex returns the index of a string in an slice of strings. Returns -1 if string does not exist.
func getIndex(val string, slice []string) int {
	for i, v := range slice {
		if v == val {
			return i
		}
	}
	return -1
}

// stringToDuration gets a string and converts it to a duration
// Returns -1 if the string is invalid
func stringToDuration(s string) time.Duration {
	sInt, err := strconv.ParseInt(s, 10, 32)
	if err != nil {
		log.Println(err)
		return -1
	}
	return time.Duration(sInt)
}

func getTestObjectFromPath(w http.ResponseWriter, r *http.Request, p project.Project) (
	test.Case, test.Sequence, error) {

	path := r.URL.EscapedPath()
	tID := strings.Split(path, "/")[4]

	tc, tcOK, _ := testStore.GetCaseStore().Get("user", p.ID, tID)
	if !tcOK {
		ts, tsOK, _ := testStore.GetSequenceStore().Get("user", p.ID, tID)
		if !tsOK {
			log.Printf("No test with the id %s", tID)
			if tID != "info" {
				w.WriteHeader(http.StatusNotFound)
			}
			return test.Case{}, test.Sequence{}, InvalidID()
		}
		return test.Case{}, *ts, nil
	}
	return *tc, test.Sequence{}, nil
}

// formatTimeSince gets a duration and returns the time passed since then formatted as string.
// If the duration is smaller than one minute, return "1 Minute".
// The biggest time format are years
func formatTimeSince(d time.Duration) string {
	if d < time.Hour {
		return formatMinute(d)
	}

	if d < time.Hour*24 {
		return formatHour(d)
	}

	if d < time.Hour*24*7 {
		return formatDay(d)
	}

	if d < time.Hour*24*30 {
		return formatWeek(d)
	}

	if d < time.Hour*24*365 {
		return formatMonth(d)
	}

	return formatYear(d)
}

func formatMinute(d time.Duration) string {

	f := int(math.Floor(d.Minutes()))
	if f < 2 {
		return "1 Minute ago"
	}
	return strconv.Itoa(f) + " Minutes ago"

}

func formatHour(d time.Duration) string {
	f := int(math.Floor(d.Hours()))
	if f < 2 {
		return "1 Hour ago"
	}
	return strconv.Itoa(f) + " Hours ago"
}

func formatDay(d time.Duration) string {
	f := int(d / (time.Hour * 24))
	if f < 2 {
		return "1 Day ago"
	}
	return strconv.Itoa(f) + " Days ago"
}

func formatWeek(d time.Duration) string {
	f := int(d / (time.Hour * 24 * 7))
	if f < 2 {
		return "1 Week ago"
	}
	return strconv.Itoa(f) + " Weeks ago"
}

func formatMonth(d time.Duration) string {
	f := int(d / (time.Hour * 24 * 30))
	if f < 2 {
		return "1 Month ago"
	}
	return strconv.Itoa(f) + " Months ago"
}

func formatYear(d time.Duration) string {
	f := int(d / (time.Hour * 24 * 365))
	if f < 2 {
		return "1 Year ago"
	}
	return strconv.Itoa(f) + " Years ago"
}

// execute executes the given templates and passes given project as context
// an error report is put into the log if something goes wrong.
func execute(tmpl *template.Template, r *http.Request, w io.Writer, p project.Project) {
	tcs := testStore.GetCaseStore().List("user", p.ID)
	tss := testStore.GetSequenceStore().List("user", p.ID)

	if err := tmpl.Execute(w, context.New().WithUserInformation(r).With("Project", p).With("TestCases", tcs).
		With("TestSequences", tss)); err != nil {
		log.Println(err)
	}
}

// isFragmentRequest checks if the request contains a parameter "fragment". If the parameter is "true", the function
// returns true, else it returns false
func isFragmentRequest(r *http.Request) bool {
	if frag := r.FormValue("fragment"); frag != "" {
		isFrag, err := strconv.ParseBool(frag)
		if err != nil {
			log.Println(err)
		}
		return isFrag
	}
	return false
}

// isMinorUpdate checks if the request contains a parameter "isMinor". If the parameter is "true", the function
// returns true, else it returns false
func isMinorUpdate(minor string) bool {
	if minor != "" {
		isMinor, err := strconv.ParseBool(minor)
		if err != nil {
			log.Println(err)
		}
		return isMinor
	}
	return false
}
