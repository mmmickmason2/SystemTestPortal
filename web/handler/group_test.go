/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal/domain/group"
	"gitlab.com/stp-team/systemtestportal/domain/visibility"
)

func TestShowGroupHandler(t *testing.T) {
	g := group.NewGroup("Group 1", "An example group", visibility.Public, nil)

	urls := []struct {
		url        string
		statusWant int
	}{
		{"/" + g.ID, http.StatusSeeOther},
		{"/" + g.ID + "/activity", http.StatusOK},
	}
	for _, grpURL := range urls {
		getRequest(t, grpURL.url, grpURL.statusWant, false, RootHandler)
		getRequest(t, grpURL.url, grpURL.statusWant, true, RootHandler)
	}
}
