/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"reflect"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/test"
	projectStore "gitlab.com/stp-team/systemtestportal/store/project"
	testStore "gitlab.com/stp-team/systemtestportal/store/test"
	"gitlab.com/stp-team/systemtestportal/web/slugs"
)

var dummyTestSequence = &test.Sequence{
	ID:   "test-sequence-1",
	Name: "Test Sequence 1",
	SequenceVersions: []test.SequenceVersion{
		{
			Description: "First test sequence",
		},
	},
}

// TestTestSequencesHandlerBadRequest tests the creation of sequences with a TRACE request
func TestTestSequencesHandlerBadRequest(t *testing.T) {
	testBadRequest(t, "/user/project-1/testsequences/")
}

func TestGetRequestTestSequence(t *testing.T) {
	testStore.GetSequenceStore().Add("user", dummyProject.ID, dummyTestSequence)
	testGetRequestTest(t, TestSequences, "test-sequence-1")
}

// TestTestSequenceSaving tests the saving of test sequences and checks the response code
func TestTestSequenceSaving(t *testing.T) {
	tsts := []struct {
		name       string
		statusWant int
		okWant     bool
		project    *project.Project
	}{
		{"TEST SEQUENCE 9999", http.StatusCreated, true, dummyProject},
		{"!!", http.StatusBadRequest, false, dummyProject},
	}
	for _, tst := range tsts {
		projectStore.GetStore().Add("test", tst.project)
		testTestSequenceSaving(t, *tst.project, tst.name, tst.statusWant, tst.okWant)
	}
}

func testTestSequenceSaving(t *testing.T, p project.Project, name string, statusWant int, okWant bool) {

	form := url.Values{}
	form.Add("inputTestSequenceName", name)
	form.Add("inputTestSequenceDescription", "")
	form.Add("inputTestSequencePreconditions", "")

	req, err := http.NewRequest(http.MethodPost, "/user/"+dummyProject.ID+"/testsequences/save", nil)
	if err != nil {
		t.Fatal(err)
	}

	req.Form = form

	rr := httptest.NewRecorder()
	hut := http.HandlerFunc(ProjectHandler)

	hut.ServeHTTP(rr, req)

	if status := rr.Code; status != statusWant {
		t.Errorf("hut returned wrong status code: got %v want %v",
			status, statusWant)
	}

	if _, ok, _ := testStore.GetSequenceStore().Get("user", p.ID, slugs.Slugify(name)); ok != okWant {
		if okWant {
			t.Errorf("test sequence %s was not created", name)
		} else {
			t.Errorf("test sequence %s was created", name)
		}
	}
}

func TestTestSequenceDeleting(t *testing.T) {
	nts, _ := test.NewTestSequence("test-sequence-to-delete", "",
		"", []test.Case{})
	tss := []struct {
		name       string
		tsToDel    test.Sequence
		statusWant int
		project    *project.Project
	}{
		{"test-sequence-to-delete", nts, http.StatusOK, dummyProject},
		{"----NOT-A-TEST-SEQ", test.Sequence{}, http.StatusNotFound, dummyProject},
	}
	for _, ts := range tss {
		// Add test sequence to delete to project if the test sequence is not an empty struct
		if !reflect.DeepEqual(ts.tsToDel, test.Sequence{}) {
			testStore.GetSequenceStore().Add("user", ts.project.ID, &ts.tsToDel)
		}
		t.Run(ts.name, testTestDeleteRequest(ts.project, TestSequences, ts.name, ts.statusWant))
	}
}

// TestTestSequencesUpdateValid tests the updating of a valid test sequence
func TestTestSequencesUpdateValid(t *testing.T) {
	nts, _ := test.NewTestSequence("TS Valid", "", "", nil)
	tss := []struct {
		tsToUpd    test.Sequence
		statusWant int
		project    project.Project
	}{
		{nts, http.StatusCreated, *dummyProject},
	}
	for _, ts := range tss {
		testStore.GetSequenceStore().Add("user", ts.project.ID, &ts.tsToUpd)
		testTestSequenceUpdating(t, ts.project, ts.tsToUpd, ts.statusWant)
	}
}

// testTestSequenceUpdating tests the updating of a test sequence without and with a fragment request
func testTestSequenceUpdating(t *testing.T, p project.Project, tsToUpd test.Sequence, statusWant int) {
	testTestSequenceUpdate(t, p, tsToUpd, statusWant, false)
	testTestSequenceUpdate(t, p, tsToUpd, statusWant, true)
}

func testTestSequenceUpdate(t *testing.T, p project.Project, ts test.Sequence, statusWant int, isFrag bool) {

	form := url.Values{}
	form.Add("handling", "update")
	form.Add("version", "1")
	form.Add("inputTestSequenceName", ts.Name)
	form.Add("inputTestSequenceDescription", "")
	form.Add("inputTestSequencePreconditions", "")
	if isFrag {
		form.Add("fragment", "true")
	}

	req, err := http.NewRequest(http.MethodPut, "/user/"+p.ID+"/testsequences/"+ts.ID+"/update", nil)
	if err != nil {
		t.Fatal(err)
	}

	req.Form = form

	rr := httptest.NewRecorder()
	hut := http.HandlerFunc(ProjectHandler)

	hut.ServeHTTP(rr, req)

	if status := rr.Code; status != statusWant {
		t.Errorf("hut returned wrong status code: got %v want %v",
			status, statusWant)
	}

	if _, ok, _ := testStore.GetSequenceStore().Get("user", p.ID, slugs.Slugify(ts.Name)); !ok {
		t.Errorf("test sequence %s was not updated", ts.Name)
	}
}

func TestRespondTestsequenceInfo(t *testing.T) {
	tss := struct {
		caseID     string
		statusWant int
		projectID  string
	}{
		"test-case-1", http.StatusOK, "project-1",
	}

	cs := tss.caseID + "~"
	testTestRespondTestsequenceInfo(t, tss.projectID, cs, tss.statusWant)
}

func testTestRespondTestsequenceInfo(t *testing.T, pID, c string, statusWant int) {
	form := url.Values{}
	form.Add("newTestcases", c)
	req, err := http.NewRequest(http.MethodGet, "/user/"+pID+"/testsequences/info", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Form = form
	rr := httptest.NewRecorder()
	hut := http.HandlerFunc(ProjectHandler)

	hut.ServeHTTP(rr, req)

	if status := rr.Code; status != statusWant {
		t.Errorf("hut returned wrong status code: got %v want %v",
			status, statusWant)
	}
}
