/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"encoding/json"
	"html/template"
	"io"
	"net/http"
	"strings"

	"fmt"
	"log"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/test"
	testStore "gitlab.com/stp-team/systemtestportal/store/test"
	"gitlab.com/stp-team/systemtestportal/web/action"
	"gitlab.com/stp-team/systemtestportal/web/context"
	"gitlab.com/stp-team/systemtestportal/web/errors"
	"gitlab.com/stp-team/systemtestportal/web/slugs"
)

// handleTestSequenceActionGet handles the actions of the GET request
func handleTestSequenceActionGet(w http.ResponseWriter, r *http.Request, p project.Project, path string) {
	url := r.URL.EscapedPath()
	id := strings.Split(url, "/")[4]
	if id == "json" {
		getJSONTestSequences(w, r, p)
	}

	_, ts, err := getTestObjectFromPath(w, r, p)
	if err != nil {
		path := r.URL.EscapedPath()
		tID := strings.Split(path, "/")[4]
		if tID == "info" {
			HandleError(respondTestsequenceInfo(w, r, p), w)
		} else {
			HandleError(err, w)
		}
		return
	}

	pathSegments := strings.Split(path, "/")
	if len(pathSegments) < 1 {
		return
	}

	if len(pathSegments) == 1 {
		specificTestSequenceAction(w, r, p, ts, action.Show)
		return
	}

	switch pathSegments[1] {
	case action.Execute:
		handleTestSequenceExecutionGet(w, r, p, ts)
	case action.Edit:
		specificTestSequenceAction(w, r, p, ts, action.Edit)
	case action.History:
		historyTestSequenceHandler(w, r, p, ts)
	case "json":
		getJSONTestSequence(w, r, p, ts)
	default:
		errors.WrongPath(w, r)
	}
}

func respondTestsequenceInfo(w http.ResponseWriter, r *http.Request, p project.Project) error {
	testcaseIds := strings.Split(r.FormValue("newTestcases"), "~")
	testcaseIds = testcaseIds[0 : len(testcaseIds)-1]
	tsi, err := getSequenceInfo(testcaseIds, p)
	if err != nil {
		return err
	}
	b, er := json.Marshal(tsi)
	if er != nil {
		return MarshalerError()
	}
	fmt.Fprintf(w, string(b))
	return nil

}

// MarshalerError returns an ErrorMessage describing that an Error occured while encoding a message
func MarshalerError() errors.HandlerError {
	return errors.Construct(http.StatusBadRequest).
		WithLog("The client send an invalid string of testcase ids.").
		WithBodyModal("Invalid testcase ids",
			"The sequence information cannot be calculated").
		WithStackTrace(1).
		Finish()
}

//getSequenceInfo calculates the SUTVersion and Duration from the testcaseIds
func getSequenceInfo(ids []string, p project.Project) (test.SequenceInfo, error) {
	tc, err := getTestcases(ids, p)
	if err != nil {
		return test.SequenceInfo{}, err
	}
	sutf, err := test.CalculateSUTVersion(tc, test.MaxSUT, test.MinSUT, "From")
	if err != nil {
		return test.SequenceInfo{}, err
	}
	sutt, err := test.CalculateSUTVersion(tc, test.MinSUT, test.MaxSUT, "To")
	if err != nil {
		return test.SequenceInfo{}, err
	}
	dh, dm := test.CalculateDuration(tc)

	si := test.SequenceInfo{
		SUTVersionFrom: sutf,
		SUTVersionTo:   sutt,
		DurationHours:  dh,
		DurationMin:    dm,
	}
	return si, nil
}

//getTestcases returns the testcase objects of the given ids
func getTestcases(ids []string, p project.Project) ([]test.Case, error) {
	var result []test.Case

	for _, caseID := range ids {
		tc, ok, err := testStore.GetCaseStore().Get("user", p.ID, caseID)
		if ok {
			result = append(result, *tc)
		} else if err != nil {
			return result, err
		}
	}
	return result, nil
}

func handleTestSequenceActionPost(w http.ResponseWriter, r *http.Request, p project.Project) {
	url := r.URL.EscapedPath()
	urlSeg := strings.Split(url, "/")
	_, ts, err := getTestObjectFromPath(w, r, p)
	if err != nil {
		return
	}

	/*if len(urlSeg) > 5 && urlSeg[5] == action.Duplicate {
		saveDuplicateTestCaseHandler(w, r, p)
	} else */
	if len(urlSeg) > 5 && urlSeg[5] == action.Execute {
		handleTestSequenceExecutionPost(w, r, p, ts)
	}
}

// specificTestSequenceAction handles the showing or editing of a test sequence
func specificTestSequenceAction(w http.ResponseWriter, r *http.Request, p project.Project, ts test.Sequence,
	act string) {

	tmpl := getTabTestSequenceShowEdit(r, act)
	tsv := getTestSequenceVersionToShowOrEdit(w, r, ts, act)
	tcs := testStore.GetCaseStore().List("user", p.ID)

	if tsv <= 0 {
		return
	}
	ctx := context.New().
		WithUserInformation(r).
		With("Project", p).
		With("TestCases", tcs).
		With("TestSequence", ts).
		With("TestSequenceVersion", ts.SequenceVersions[len(ts.SequenceVersions)-tsv])
	if err := tmpl.Execute(w, ctx); err != nil {
		log.Printf("Failed to show test case error on template parsing: %s", err)
	}
}

// historyTestSequenceHandler is a handler for showing the history of a test sequence
func historyTestSequenceHandler(w io.Writer, r *http.Request, p project.Project, ts test.Sequence) {
	tmpl := getTabTestSequencesHistory(r)
	ctx := context.New().
		WithUserInformation(r).
		With("Project", p).
		With("TestSequence", ts)
	if err := tmpl.Execute(w, ctx); err != nil {
		log.Printf("Failed to show test case error on template parsing: %s", err)
	}
}

// saveNewTestSequenceHandler is used to save a new test sequence to the system
func saveNewTestSequenceHandler(w http.ResponseWriter, r *http.Request, p project.Project) {
	tsn, _, tsd, tsp, tsc := getTestSequenceInput(r, p)

	ts, err := test.NewTestSequence(tsn, tsd, tsp, tsc)
	if err != nil {
		HandleError(err, w)
	}
	if vErr := validateItem(p, ts); vErr != nil {
		HandleError(vErr, w)
		return
	}

	err = testStore.GetSequenceStore().Add("user", p.ID, &ts)

	if err != nil {
		HandleError(err, w)
		return
	}

	w.Header().Set("newName", slugs.Slugify(ts.Name))
	w.WriteHeader(http.StatusCreated)
	r.Form.Set("fragment", "true")

	generalTestsActionHandler(w, r, p, TestSequences, action.Show)
}

// deleteTestSequenceHandler is a handler for deleting a test sequence from the system
func deleteTestSequenceHandler(w http.ResponseWriter, r *http.Request, p project.Project, ts test.Sequence) {
	err := testStore.GetSequenceStore().Delete("user", p.ID, ts.ID)

	if err != nil {
		HandleError(err, w)
		return
	}

	generalTestsActionHandler(w, r, p, TestSequences, action.Show)
}

// updateTestSequenceHandler is a handler for updating a test sequence
func updateTestSequenceHandler(w http.ResponseWriter, r *http.Request, p project.Project, ts test.Sequence) {
	tsn, cm, tsd, tsp, tsc := getTestSequenceInput(r, p)

	ts, err := handleTestSequenceVersion(r, ts, cm, tsd, tsp, tsc)
	if err != nil {
		return
	}

	p, ts, nerr := updateTestSequence(p, ts, tsn)
	if nerr != nil {
		HandleError(nerr, w)
		return
	}

	w.Header().Set("newName", ts.ID)
	w.WriteHeader(http.StatusCreated)
	r.Form.Set("version", "")

	specificTestSequenceAction(w, r, p, ts, action.Show)
}

// getTestSequenceVersionToShowOrEdit returns the version of the test sequence to show or edit
func getTestSequenceVersionToShowOrEdit(w http.ResponseWriter, r *http.Request, ts test.Sequence, actn string) int {
	if actn == action.Edit {
		return getTestVersionToEdit(w, r, ts)
	}
	return getTestVersionToShow(w, r, ts)
}

// getTestSequenceInput gets the test sequence input from the request
func getTestSequenceInput(r *http.Request, p project.Project) (string, string, string, string, []test.Case) {
	tsn := r.FormValue("inputTestSequenceName")
	tsd := r.FormValue("inputTestSequenceDescription")
	tsp := r.FormValue("inputTestSequencePreconditions")
	tsc := r.FormValue("inputTestSequenceTestCase")
	cm := r.FormValue("inputCommitMessage")

	tc := strings.Split(tsc, "~")
	var tc2 []test.Case

	for _, caseID := range tc {
		for _, tc := range testStore.GetCaseStore().List("user", p.ID) {
			if tc.ID == caseID {
				tc2 = append(tc2, *tc)
			}
		}
	}

	return tsn, cm, tsd, tsp, tc2
}

// handleTestSequenceVersion creates a new test sequence version and either marks it as minor edit or not, depending
// on the "isMinor" parameter in the request.
// Returns false if the version is not valid.
func handleTestSequenceVersion(r *http.Request, ts test.Sequence, cm, tsd, tsp string,
	tsc []test.Case) (test.Sequence, error) {

	var tsv test.SequenceVersion
	var err error
	curVer := len(ts.SequenceVersions)
	if curVer <= 0 {
		return ts, InvalidTSVersion()
	}
	if isMinorUpdate(r.FormValue("isMinor")) {
		tsv, err = test.NewTestSequenceVersion(curVer+1, true, cm, tsd, tsp, tsc)
	} else {
		tsv, err = test.NewTestSequenceVersion(curVer+1, false, cm, tsd, tsp, tsc)
	}

	// Insert at beginning of slice
	ts.SequenceVersions = append(ts.SequenceVersions, test.SequenceVersion{})
	copy(ts.SequenceVersions[1:], ts.SequenceVersions[:])
	ts.SequenceVersions[0] = tsv

	return ts, err
}

// InvalidTSVersion returns an ErrorMessage describing that the test seqeuence version is not valid
func InvalidTSVersion() errors.HandlerError {
	return errors.Construct(http.StatusBadRequest).
		WithLog("The client send an invalid test sequence version.").
		WithBodyModal("Invalid test sequence version",
			"The test sequence version is invalid").
		WithStackTrace(1).
		Finish()
}

// updateTestSequence updates a test sequence. If the test sequence is not valid, it shows an error and returns false.
// If the test sequence is successfully updated updateTestSequence returns true
func updateTestSequence(p project.Project, ts test.Sequence, tsn string) (
	project.Project, test.Sequence, error) {
	backupTs, _, err := testStore.GetSequenceStore().Get("user", p.ID, ts.ID)

	if err != nil {
		return p, ts, err
	}

	err = testStore.GetSequenceStore().Delete("user", p.ID, ts.ID)

	if err != nil {
		return p, ts, err
	}

	ts.Name = tsn
	ts.ID = slugs.Slugify(tsn)
	if vErr := validateItem(p, ts); vErr != nil {
		err = testStore.GetSequenceStore().Add("user", p.ID, backupTs)
		if err != nil {
			return p, ts, err
		}
		return p, ts, vErr
	}

	err = testStore.GetSequenceStore().Add("user", p.ID, &ts)
	return p, ts, err
}

// getJSONTestSequences gets all test sequences of the given project as json and writes them in the response
func getJSONTestSequences(w io.Writer, r *http.Request, p project.Project) {
	ts, _ := json.Marshal(testStore.GetSequenceStore().List("user", p.ID))
	w.Write(ts)
}

// getJSONTestSequence gets a test sequence of a project and writes it in the response
func getJSONTestSequence(w io.Writer, r *http.Request, p project.Project, ts test.Sequence) {
	tsjson, _ := json.Marshal(ts)
	w.Write(tsjson)
}

func getTestSequenceListFragment(r *http.Request) *template.Template {
	if isFragmentRequest(r) {
		return GetTabTestSequencesListFragment()
	}
	return GetTabTestSequencesListTree()
}

func getTestSequenceNewFragment(r *http.Request) *template.Template {
	if isFragmentRequest(r) {
		return GetTabTestSequencesNewFragment()
	}
	return GetTabTestSequencesNewTree()

}

// getTabTestSequenceShowEdit returns either the show or edit fragments for a test sequence
func getTabTestSequenceShowEdit(r *http.Request, actn string) *template.Template {
	switch actn {
	case action.Show:
		return getTabTestSequenceShow(r)
	case action.Edit:
		return getTabTestSequenceEdit(r)
	}
	return nil
}

// getTabTestSequenceShow returns either the test sequence show fragment only or the show fragment with all its parent
// fragments, depending on the isFrag parameter
func getTabTestSequenceShow(r *http.Request) *template.Template {
	if isFragmentRequest(r) {
		return GetTabTestSequencesShowFragment()
	}
	return GetTabTestSequencesShowTree()
}

// getTabTestSequenceEdit returns either the test sequence edit fragment only or the edit fragment with all its parent
// fragments, depending on the isFrag parameter
func getTabTestSequenceEdit(r *http.Request) *template.Template {
	if isFragmentRequest(r) {
		return GetTabTestSequencesEditFragment()
	}
	return GetTabTestSequencesEditTree()
}

// getTabTestSequencesHistory returns either the test sequence history fragment only or the edit fragment
// with all its parent fragments, depending on the isFrag parameter
func getTabTestSequencesHistory(r *http.Request) *template.Template {
	if isFragmentRequest(r) {
		return GetTabTestSequencesHistoryFragment()
	}
	return GetTabTestSequencesHistoryTree()
}

// GetTabTestSequencesListTree returns the test sequence list tab template with all parent templates
func GetTabTestSequencesListTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab test sequence list tree
		Append("tab-testsequences-list").
		Get().Lookup("header")
}

// GetTabTestSequencesNewTree returns the new test sequence tab template with all parent templates
func GetTabTestSequencesNewTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab test sequence new tree
		Append("tab-testsequences-new", "modal-testcase-selection").
		Get().Lookup("header")
}

// GetTabTestSequencesShowTree returns the test sequence show tab template with all parent templates
func GetTabTestSequencesShowTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab test sequence show tree
		Append("tab-testsequences-show").
		Get().Lookup("header")
}

// GetTabTestSequencesEditTree returns the test sequence edit tab template with all parent templates
func GetTabTestSequencesEditTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab test sequence edit tree
		Append("tab-testsequences-edit", "modal-testcase-selection").
		Get().Lookup("header")
}

// GetTabTestSequencesHistoryTree returns the test sequence history tab template with all parent templates
func GetTabTestSequencesHistoryTree() *template.Template {
	return getNoSideBarTree().
		// Project tabs tree
		Append("content-project-tabs").
		// Tab test sequence history tree
		Append("tab-testsequences-history").
		Get().Lookup("header")
}

// GetTabTestSequencesListFragment returns only the test sequence list tab template
func GetTabTestSequencesListFragment() *template.Template {
	return getBaseTree().Append("tab-testsequences-list").Get().Lookup("tab-content")
}

// GetTabTestSequencesNewFragment returns only the new test sequence tab template
func GetTabTestSequencesNewFragment() *template.Template {
	return getBaseTree().
		Append("tab-testsequences-new", "modal-testcase-selection").
		Get().Lookup("tab-content")
}

// GetTabTestSequencesShowFragment returns only the test sequence show template
func GetTabTestSequencesShowFragment() *template.Template {
	return getBaseTree().Append("tab-testsequences-show").Get().Lookup("tab-content")
}

// GetTabTestSequencesEditFragment returns only the test sequence edit template
func GetTabTestSequencesEditFragment() *template.Template {
	return getBaseTree().
		Append("tab-testsequences-edit", "modal-testcase-selection").
		Get().Lookup("tab-content")
}

// GetTabTestSequencesHistoryFragment returns only the test sequence history template
func GetTabTestSequencesHistoryFragment() *template.Template {
	return getBaseTree().Append("tab-testsequences-history").Get().Lookup("tab-content")
}
