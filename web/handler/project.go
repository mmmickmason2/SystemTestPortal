/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strings"

	"html/template"

	"gitlab.com/stp-team/systemtestportal/domain/project"
	"gitlab.com/stp-team/systemtestportal/domain/visibility"
	projectStore "gitlab.com/stp-team/systemtestportal/store/project"
	"gitlab.com/stp-team/systemtestportal/web/action"
	"gitlab.com/stp-team/systemtestportal/web/context"
	"gitlab.com/stp-team/systemtestportal/web/errors"
	"gitlab.com/stp-team/systemtestportal/web/slugs"
)

// The const contains the tabs that can be selected in a project
const (
	Dashboard     = "dashboard"
	TestCases     = "testcases"
	TestSequences = "testsequences"
	TestSteps     = "teststeps"
	Protocols     = "protocols"
	Records       = "records"
)

// ProjectHandler is a handler for showing the proper project
func ProjectHandler(w http.ResponseWriter, r *http.Request) {
	path := r.URL.EscapedPath()
	if !strings.HasPrefix(path, "/user/") {
		log.Println("Invalid path", path)
		return
	}

	path = strings.TrimPrefix(path, "/user/")
	pathSegments := strings.Split(path, "/")

	handlePath(w, r, pathSegments)
}

// handlePath redirects to the correct handler depending on the given path
func handlePath(w http.ResponseWriter, r *http.Request, pathSegments []string) {
	if ok := handleFirstPathSegment(w, r, pathSegments); !ok {
		return
	}

	pID := pathSegments[0]
	p, ok, err := projectStore.GetStore().Get("user", pID)
	if err != nil {
		HandleError(err, w)
		return
	}

	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if len(pathSegments) == 1 {
		http.Redirect(w, r, p.ID+"/", http.StatusFound)
		return
	}

	handleSecPathSegment(w, r, pathSegments, *p)
}

// handleFirstPathSegment redirects to the correct handler depending on the given url
func handleFirstPathSegment(w http.ResponseWriter, r *http.Request, pathSegments []string) bool {
	if pathSegments[0] == "projects" {
		switch r.Method {
		case http.MethodGet:
			switch pathSegments[1] {
			case action.Show:
				errors.WrongPath(w, r)
				return false
			case action.New:
				NewProjectHandler(w, r)
				return false
			case "json":
				getJSONProjects(w, r)
			default:
				errors.WrongPath(w, r)
				return false
			}

		case http.MethodPost:
			switch pathSegments[1] {
			case action.Save:
				SaveNewProjectHandler(w, r)
				return false
			default:
				errors.WrongPath(w, r)
				return false
			}
		default:
			errors.WrongRequestMethod(w, r)
			return false
		}
	}
	return true
}

// handleSecPathSegment redirects to the correct handler depending on the given url
func handleSecPathSegment(w http.ResponseWriter, r *http.Request, pathSegments []string, p project.Project) {
	switch pathSegments[1] {
	case action.Show:
		http.Redirect(w, r, "testcases/", http.StatusSeeOther)
	case TestCases, TestSequences:
		TestHandler(w, r, p, pathSegments[1])
	case "json":
		getJSONProject(w, r, p)
	case Protocols, Records:
		ProtocolHandler(w, r, p, pathSegments)
	default:
		errors.WrongPath(w, r)
	}
}

// NewProjectHandler is a handler for showing the screen for creating a new project
func NewProjectHandler(w http.ResponseWriter, r *http.Request) {
	tmpl := GetNewProjectTree()
	if err := tmpl.Execute(w, context.New().WithUserInformation(r)); err != nil {
		log.Println(err)
	}
}

// SaveNewProjectHandler is used to save a new project in the system
func SaveNewProjectHandler(w http.ResponseWriter, r *http.Request) {
	pn := r.FormValue("inputProjectName")
	pd := r.FormValue("inputProjectDesc")
	pvs := r.FormValue("optionsProjectVisibility")
	pv, err := visibility.StringToVis(pvs)
	if err != nil {
		HandleError(InvalidVisibility(), w)
		return
	}

	p := project.NewProject(pn, pd, pv)
	if vErr := validateContainer(p); vErr != nil {
		HandleError(vErr, w)
		return
	}

	err = projectStore.GetStore().Add("user", &p)
	if err != nil {
		HandleError(err, w)
		return
	}

	w.Header().Set("newName", slugs.Slugify(p.Name))
	w.WriteHeader(http.StatusOK)
}

// getJSONProjects returns all projects in the system as json
func getJSONProjects(w io.Writer, r *http.Request) {
	p, _ := json.Marshal(projectStore.GetStore().List("user"))
	w.Write(p)
}

// getJSONProject returns the project as json
func getJSONProject(w io.Writer, r *http.Request, p project.Project) {
	pjson, _ := json.Marshal(p)
	w.Write(pjson)
}

// GetNewProjectTree returns the new project template with all parent templates
func GetNewProjectTree() *template.Template {
	return getNoSideBarTree().
		// New project tree
		Append("new-project").
		Get().Lookup("header")
}
