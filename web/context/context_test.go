/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package context

import (
	"net/http"
	"testing"

	"errors"

	"net/http/httptest"

	"fmt"

	"gitlab.com/stp-team/systemtestportal/domain/user"
)

// SessionStub is used to stub a session handler for the context package
type SessionStub struct {
	rUser *user.User
	rErr  error
}

// GetCurrent is a stub method that just calls a predefined function.
func (s SessionStub) GetCurrent(r *http.Request) (*user.User, error) {
	return s.rUser, s.rErr
}

// TestNew test the new function for the context package.
func TestNew(t *testing.T) {
	Init(SessionStub{nil, nil})
	c := New()

	k := "key"
	v := "test"
	c[k] = v

	if c[k] != v {
		t.Errorf("Context doesn't work like a map: "+
			"Expected %s but got %s for key %s.", v, c[k], k)
	}
}

// TestContext_With tests the With function for a context object returned
// by context.New()
func TestContext_With(t *testing.T) {
	Init(SessionStub{nil, nil})
	k := "key"
	v := "test"
	c := New().With(k, v)

	if c[k] != v {
		t.Errorf("context.Context.With() value wasn't assigned correctly:"+
			" Expected %s but got %s for key %s.", v, c[k], k)
	}
}

// TestContext_WithUserInformation tests the WithUserInformation function for
// a context object returned by context.New()
func TestContext_WithUserInformation(t *testing.T) {
	u := user.New("ImaDummy",
		"ImanoDummy",
		"ima@example.com",
		"password")
	r := httptest.NewRequest(http.MethodPost, "/", nil)
	var tests = []struct {
		rUser     *user.User
		rErr      error
		eUser     *user.User
		eLoggedIn bool
	}{
		{
			nil,
			nil,
			nil,
			false,
		},
		{
			&u,
			nil,
			&u,
			true,
		},
		{
			&u,
			errors.New("hoppala"),
			nil,
			false,
		},
		{
			nil,
			errors.New("hoppala"),
			nil,
			false,
		},
	}
	for _, tc := range tests {
		Init(SessionStub{tc.rUser, tc.rErr})
		c := New().WithUserInformation(r)

		if c[LoggedInKey] != tc.eLoggedIn {
			t.Errorf("context.Context.WithUserInformation() doesn't set "+
				"values correctly: Expected %t but was %t for key %s", tc.eLoggedIn,
				c[LoggedInKey], LoggedInKey)
			t.Logf("Failed for test-case %+v", tc)
		}
		u, ok := c[UserKey]
		if !eq(u, ok, tc.eUser) {
			t.Errorf("context.Context.WithUserInformation() doesn't set "+
				"values correctly: Expected %+v but was %+v for key %s", tc.eUser,
				u, UserKey)
			t.Logf("Failed for test-case %+v", tc)
		}
	}
}

// ExampleContext_With is an example for the usage of the context.Context.With() method.
func ExampleContext_With() {
	c := New().With("key", "value").With("int", 5)
	c.With("struct", struct {
		a int
		b string
	}{9, "bla"})
	fmt.Println(c["key"], c["int"], c["struct"])
	//Output:value 5 {9 bla}
}

// eq checks if a map entry is equal to a given other value.
// Equality is defined the following way:
// Nil is equal to no entry in the map (ok is false).
// Otherwise the values are equal if they are equal by the '=='-operator.
func eq(u interface{}, ok bool, eu *user.User) bool {
	return (ok && u == eu) || (!ok && eu == nil)
}
