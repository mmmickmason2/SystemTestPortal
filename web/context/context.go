/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Package context is used to create a context that can be passed to the execution
of templates. And helps with conveying information like the logged in user to
the user interface.

A new context can be created using the New() function.

		c := context.New()

To add information to a context use the With() function. It
can be easily chained with the New() method.

		c := context.New().With("key", "value").With("number", 9)

The package also provides a method with which user information is added to the
context automatically. To be able to use this the packages need to get a session
handler via its Init() function.

		context.Init(mySessionHandler)
		c := context.New().WithUserInformation(myRequest)

*/
package context

import (
	"net/http"

	"log"
	"net/http/httputil"

	"gitlab.com/stp-team/systemtestportal/config"
	"gitlab.com/stp-team/systemtestportal/domain/user"
)

// session is used to access information about the currently logged in user,
var session SessionHandler

// Reserved keys for user information
const (
	LoggedInKey = "LoggedIn"
	UserKey     = "User"
)

// Init initializes this packages to use the given session handler.
func Init(handler SessionHandler) {
	session = handler
}

// New creates a new *empty* context.
func New() Context {
	return make(Context).With("version", config.Version())
}

// SessionHandler is used to get the user that corresponds to a request.
type SessionHandler interface {
	// GetCurrent gets the user that hold the session. If there is no
	// user session the returned user will be nil.
	GetCurrent(r *http.Request) (*user.User, error)
}

// Context contains information useful for the execution of templates
type Context map[string]interface{}

// WithUserInformation fills the context with information about the currently logged in user.
// This call can be chained.
// Be careful not to set values with the keys used by this method for the user information.
// (See the LoggedInKey and UserKey constants of this package)
// If With() was used before to add values with said key this method will panic.
func (c Context) WithUserInformation(r *http.Request) Context {
	if session == nil {
		log.Print("Context: Couldn't get user information for request because " +
			"session handler hasn't been initialised with the Init() fucntion " +
			"of this package. Assueming user is not logged in.")
		return c.With(LoggedInKey, false)
	}
	u, err := session.GetCurrent(r)
	if err != nil {
		if d, _ := httputil.DumpRequest(r, true); err != nil {
			log.Printf("Context: Couldn't get user information for request: %+v because %s. "+
				"Assueming user is not logged in.", r, err)
		} else {
			log.Printf("Context: Couldn't get user information for request: %s because %s. "+
				"Assueming user is not logged in.", d, err)
		}
		return c.With(LoggedInKey, false)
	}
	return c.With(LoggedInKey, u != nil).With(UserKey, u)
}

// With is a convince method used to chain-add content to the context.
// Values can only be added once. If a value with given key already
// exists this function will panic.
// If 'nil' is passed as data it will not be stored. This means this
// method can't be used to delete values.
func (c Context) With(key string, data interface{}) Context {
	v, existsAlready := c[key]
	if existsAlready {
		log.Panicf("Context error: Tried adding a value with key '%s' that already "+
			"existed: Value %+v would be overriden by value %+v!",
			key, v, data)
	}
	if data != nil {
		c[key] = data
	}
	return c
}
