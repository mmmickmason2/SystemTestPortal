/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

// Package action contains the action const for creating, saving, deleting, editing and updating
package action

// Show string (listing/showing information), New, Save, Delete, Edit and Update
// are the possible actions for projects, groups, test cases and sequences
const (
	Show      = ""
	New       = "new"
	Save      = "save"
	Duplicate = "duplicate"
	Edit      = "edit"
	Update    = "update"
	Delete    = "delete"
	History   = "history"
	Execute   = "execute"
)
