/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package slugs

import (
	"testing"
)

// TestSlugify contains test cases for the Slugify function
func TestSlugify(t *testing.T) {
	tcs := []struct {
		in   string
		want string
	}{
		{"Test", "test"},
		{"Tést", "test"},
		{"<Test>", "test"},
		{"Test Case", "test-case"},
		{"Test Case !", "test-case"},
		{"Test Case 1", "test-case-1"},
	}

	for _, tc := range tcs {
		got := Slugify(tc.in)

		if got != tc.want {
			t.Errorf("Slugify(%s) = %s, want %s", tc.in, got, tc.want)
		}
	}
}
