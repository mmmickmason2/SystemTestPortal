#!/bin/bash
#
# This file is part of SystemTestPortal.
# Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
#
# SystemTestPortal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SystemTestPortal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
#
REF=$1
if [ -z ${REF} ]; then
    REF="master"
fi

BINARY_DIR="/opt/stp/bin"
STATIC_FILES_DIR="/opt/stp"

# Select architecture
JOB=""
case $(uname -m) in
  armv6*)
    JOB="linux-armel"
    ;;
  armv7*)
    JOB="linux-armhf"
    ;;
  i386)
    JOB="linux-386"
    ;;
  x86_64)
    JOB="linux-amd64"
    ;;
  *)
    echo "Your machine is not supported by this script yet!"
    exit 1
esac

function report_sig_error {
    echo "Couldn't verify gpg signature for file $1."
    echo "Have you already imported the public key?"
    echo "If you have, the downloaded file might be malicious."
}

# Construct download urls
URL_BASE="https://gitlab.com/stp-team/systemtestportal"
URL="$URL_BASE/builds/artifacts/$REF/download?job=$JOB"
SYSTEMD_URL="$URL_BASE/raw/$REF/_scripts/ci/service_tmpl.sh"
INSTALL_URL="$URL_BASE/raw/$REF/_scripts/install_stp.sh"

working_dir=${PWD}

mkdir -p ${working_dir}/stp
cd ${working_dir}/stp
# Download
echo "Downloading required files..."
curl -L -o ./stp.tar.gz.zip "$URL"
curl -L -o ./service.sh "$SYSTEMD_URL"
curl -L -o ./install.sh "$INSTALL_URL"
# Extract
echo "Extracting..."
unzip -u -d ./ ./stp.tar.gz.zip
file="$(find ./ -maxdepth 1 -iname '*.tar.gz')"
gpg --verify ${file}.sig || { report_sig_error ${file}; rm -r ${working_dir}/stp; exit -1; }
mkdir ./binary
tar -xvzf ${file} -C ./binary
# Install
echo "Installing..."
# Make scripts executable
chmod +x ./service.sh
chmod +x ./install.sh
./service.sh ${BINARY_DIR} ${STATIC_FILES_DIR}  > ./binary/stp.service
./install.sh ./binary ${BINARY_DIR} ${STATIC_FILES_DIR}
# Clean
echo "Cleaning..."
rm -r ${working_dir}/stp
