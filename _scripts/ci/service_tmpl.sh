#!/usr/bin/env bash
#
# This file is part of SystemTestPortal.
# Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
#
# SystemTestPortal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SystemTestPortal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
#
BINDIR=$1
WORKDIR=$2

cat  << EOF
[Unit]
Description=STP the best web app for manual testing.
After=syslog.target
After=network.target
#After=mysqld.service
#After=postgresql.service
#After=memcached.service
#After=redis.service

[Service]
Type=simple
WorkingDirectory=${WORKDIR}
ExecStart=${BINDIR}/stp --basepath=${WORKDIR} --host="" --port="80"
Restart=always

[Install]
WantedBy=multi-user.target
EOF