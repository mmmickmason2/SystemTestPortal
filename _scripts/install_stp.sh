#!/usr/bin/env bash
#
# This file is part of SystemTestPortal.
# Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
#
# SystemTestPortal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SystemTestPortal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
#
# Parameters
SRC_DIR=$1
BINARY_DIR=$2
STATIC_FILES_DIR=$3
SYSTEMD_DIR=${4-/etc/systemd/system/}

# Prepare folders
mkdir -p ${BINARY_DIR} ${STATIC_FILES_DIR} ${SYSTEMD_DIR}

# Copy files over
cp ${SRC_DIR}/stp.service ${SYSTEMD_DIR}
cp ${SRC_DIR}/stp ${BINARY_DIR}
cp -r ${SRC_DIR}/templates ${STATIC_FILES_DIR}
cp -r ${SRC_DIR}/static ${STATIC_FILES_DIR}