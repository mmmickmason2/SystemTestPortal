[![Build-Badge][build]][commits-master]
[![Coverage-Badge][coverage]][coverage-report]


[coverage]: https://gitlab.com/stp-team/systemtestportal/badges/master/coverage.svg?job=test-project "Master test coverage"
[build]: https://gitlab.com/stp-team/systemtestportal/badges/master/build.svg "Master pipeline status"
[coverage-report]: https://gitlab.com/stp-team/systemtestportal/builds/artifacts/master/browse?job=test-project
[commits-master]: https://gitlab.com/stp-team/systemtestportal/commits/master

# SystemTestPortal

SystemTestPortal is a web application that allows to create, run and analyze
manual system tests. It aims to be useful for developers, testers and end-users.

The SystemTestPortal is currently in version 0.6.0. See the [changelog](https://gitlab.com/STP-Team/SystemTestPortal/blob/master/CHANGELOG.md) for more information.

## Website

http://www.systemtestportal.org

## Webapplication

A link to a public system will be added.

## Features

The features are described [here](http://www.systemtestportal.org/discover/).

## Installation guide

The installation guide can be found [here](http://www.systemtestportal.org/get/).

## About the developers

The SystemTestPortal is developed by students of the [Institute of Software Technology at the Universität Stuttgart](http://www.iste.uni-stuttgart.de/se) in a student project.

## You want to contribute?

Check out our workflow on [the wiki page](https://gitlab.com/STP-Team/SystemTestPortal/wikis/git-workflow).